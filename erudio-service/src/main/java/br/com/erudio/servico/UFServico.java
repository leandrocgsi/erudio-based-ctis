package br.com.erudio.servico;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.erudio.negocio.leitura.UnidadeFederativaNegocioLeitura;

@Path("uf")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class UFServico {

    @EJB
    private UnidadeFederativaNegocioLeitura ufLeitura;
    
    @GET
    public Response buscarTodosUF() {
        return Response.ok(ufLeitura.buscarTodos()).build();
    }
    
}