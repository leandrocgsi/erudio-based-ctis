package br.com.erudio.servico;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.erudio.negocio.leitura.GrauInstrucaoNegocioLeitura;

@Path("grauInstrucao")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class GrauInstrucaoServico {

    @EJB
    private GrauInstrucaoNegocioLeitura grauInstrucaoLeitura;
    
    @GET
    public Response buscarTodosGrauInstrucao() {
        return Response.ok(grauInstrucaoLeitura.buscarTodos()).build();
    }
    
}