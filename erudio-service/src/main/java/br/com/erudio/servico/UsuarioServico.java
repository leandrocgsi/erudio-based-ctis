package br.com.erudio.servico;

import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.picketlink.authorization.annotations.RolesAllowed;

import br.com.erudio.dto.PaginacaoDTO;
import br.com.erudio.negocio.escrita.UsuarioNegocioEscrita;
import br.com.erudio.negocio.leitura.UsuarioNegocioLeitura;
import br.com.erudio.negocio.vo.UsuarioExternoVO;
import br.com.erudio.negocio.vo.UsuarioInternoVO;
import br.com.erudio.seguranca.util.ConstantesPermissoes;

@Path("usuario")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class UsuarioServico {

    @EJB
    private UsuarioNegocioLeitura usuarioLeitura;

    @EJB
    private UsuarioNegocioEscrita usuarioEscrita;

    @GET
    @Path("detalharUsuario/{id}")
    public Response detalharUsuario(@PathParam("id") Integer id) {
        return Response.ok(usuarioLeitura.detalharUsuario(id)).build();
    }

    @GET
    @Path("buscarUsuarioExternoEmail/{email}")
    public Response buscarUsuarioExternoEmail(@PathParam("email") String email) {
        return Response.ok(usuarioLeitura.buscarUsuarioExternoVOPorEmail(email)).build();
    }

    @GET
    @Path("/buscarUsuarioInternoVOTitularUnidade/{idUnidade}")
    @RolesAllowed({ConstantesPermissoes.GerenciarUnidade.VISUALIZAR_UNIDADE})
    public Response buscarUsuarioInternoVOTitularUnidade(@PathParam("idUnidade") Integer idUnidade) {
        return Response.ok(usuarioLeitura.buscarUsuarioInternoVOTitularUnidade(idUnidade)).build();
    }

    @GET
    @Path("usuarioLogado")
    public Response recuperarUsuarioLogado() {
        return Response.ok(usuarioLeitura.detalharUsuario(1)).build();
    }

    @POST
    @Path("/buscaPaginadaUsuarioInterno")
    @RolesAllowed({ConstantesPermissoes.AssociarUsuarioPerfil.ADICIONAR_ASSOCIACAO})
    public Response buscaPaginadaUsuarioInterno(PaginacaoDTO<UsuarioInternoVO> usuarioInternoDTO) {
        usuarioLeitura.buscaPaginadaUsuarioInterno(usuarioInternoDTO);
        return Response.ok(usuarioInternoDTO).build();
    }

    @POST
    @Path("/buscaPaginadaUsuarioExterno")
    @RolesAllowed({ConstantesPermissoes.GerenciarPerfil.VISUALIZAR_PERFIL})
    public Response buscaPaginadaUsuarioExterno(PaginacaoDTO<UsuarioExternoVO> usuarioExternoDTO) {
        usuarioLeitura.buscaPaginadaUsuarioExterno(usuarioExternoDTO);
        return Response.ok(usuarioExternoDTO).build();
    }

    @POST
    @Path("/atualizarUsuarioExterno")
    public Response cadastrarUsuarioExterno(UsuarioExternoVO usuarioExternoVO) {
        usuarioEscrita.atualizarUsuarioExterno(usuarioExternoVO);
        return Response.ok().build();
    }

    @PUT
    @Path("/associarUsuariosPerfil")
    @RolesAllowed({ConstantesPermissoes.AssociarUsuarioPerfil.ADICIONAR_ASSOCIACAO})   
    public Response associarUsuariosPerfil(Map<String, Object> usuariosPerfil) {
        List<Integer> idUsuarios = (List<Integer>) usuariosPerfil.get("idUsuarios");
        Integer idPefil = (Integer) usuariosPerfil.get("idPerfil");
        usuarioEscrita.associarUsuariosPerfil(idUsuarios, idPefil);
        return Response.ok().build();
    }

    @PUT
    @Path("/excluirAssociarUsuarioPerfil")
    @Consumes(MediaType.APPLICATION_JSON)
    @RolesAllowed({ConstantesPermissoes.AssociarUsuarioPerfil.EXCLUIR_ASSOCIACAO})
    public Response excluirAssociarUsuarioPerfil(UsuarioInternoVO usuariosInternoVO) {
        usuarioEscrita.excluirAssociarUsuariosPerfil(usuariosInternoVO);
        return Response.ok().build();
    }

}