package br.com.erudio.servico;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.picketlink.authorization.annotations.RolesAllowed;

import br.com.erudio.dto.PaginacaoDTO;
import br.com.erudio.negocio.escrita.PerfilAcessoNegocioEscrita;
import br.com.erudio.negocio.leitura.PerfilAcessoNegocioLeitura;
import br.com.erudio.negocio.vo.PerfilAcessoPermissaoVO;
import br.com.erudio.negocio.vo.PerfilAcessoVO;
import br.com.erudio.seguranca.util.ConstantesPermissoes;

@Path("perfilAcesso")
@Produces(MediaType.APPLICATION_JSON)
public class PerfilAcessoServico {

    @EJB
    private PerfilAcessoNegocioEscrita perfilAcessoEscrita;

    @EJB
    private PerfilAcessoNegocioLeitura perfilAcessoLeitura;

    @GET
    @RolesAllowed(ConstantesPermissoes.GerenciarPerfil.LISTAR_PERFIL)
    public Response buscarTodosPerfis() {
        return Response.ok(perfilAcessoLeitura.buscarTodos()).build();
    }

    @GET
    @Path("/buscarTodosPerfisInternos")
    @RolesAllowed({
        ConstantesPermissoes.GerenciarPerfil.LISTAR_PERFIL,
        ConstantesPermissoes.AssociarUsuarioPerfil.ADICIONAR_ASSOCIACAO, 
        ConstantesPermissoes.AssociarUsuarioPerfil.EXCLUIR_ASSOCIACAO})
    public Response buscarTodosPerfisInternos() {
        return Response.ok(perfilAcessoLeitura.buscarTodosPerfisInternos()).build();
    }

    @POST
    @Path("/buscaPaginada")
    @Consumes(MediaType.APPLICATION_JSON)
    @RolesAllowed({
        ConstantesPermissoes.GerenciarPerfil.LISTAR_PERFIL})
    public Response buscaPaginada(PaginacaoDTO<PerfilAcessoVO> perfilAcessoDTO) {
        perfilAcessoLeitura.buscaPaginada(perfilAcessoDTO);
        return Response.ok(perfilAcessoDTO).build();
    }

    @GET
    @Path("/{id}")
    @RolesAllowed(ConstantesPermissoes.GerenciarPerfil.VISUALIZAR_PERFIL)
    public Response buscarPerfilPorId(@PathParam("id") Integer id) {
        return Response.ok(perfilAcessoLeitura.buscarPerfilAcessoVOPorId(id)).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @RolesAllowed(ConstantesPermissoes.GerenciarPerfil.INCLUIR_PERFIL)
    public Response incluirPerfil(PerfilAcessoVO perfil) {
        return gravar(perfil);
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @RolesAllowed(ConstantesPermissoes.GerenciarPerfil.ALTERAR_PERFIL)
    public Response atualizarPerfil(PerfilAcessoVO perfil) {
        return gravar(perfil);
    }

    @DELETE
    @Path("{id}")
    @RolesAllowed(ConstantesPermissoes.GerenciarPerfil.EXCLUIR_PERFIL)
    public Response removerPerfilAcesso(@PathParam("id") Integer id) {
        perfilAcessoEscrita.excluir(id);
        return Response.ok().build();
    }

    private Response gravar(PerfilAcessoVO perfil) {
        PerfilAcessoVO novoPerfil = perfilAcessoEscrita.gravar(perfil);
        return Response.ok(novoPerfil).build();
    }
    
    @GET
    @Path("/detalhar/{id}")
    public Response buscarPerfilAtivoComFuncionalidade(@PathParam("id") Integer id){
    	return Response.ok(perfilAcessoLeitura.buscarPerfilAcessoVODetalhadoPorId(id)).build();
    }
    
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("permissoes")
    public Response gravarPermissao(PerfilAcessoPermissaoVO perfil){
    	perfilAcessoEscrita.atualizarPermissoesPerfil(perfil);
    	return Response.ok().build();
    }
}