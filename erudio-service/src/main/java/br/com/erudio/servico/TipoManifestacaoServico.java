package br.com.erudio.servico;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.picketlink.authorization.annotations.RolesAllowed;

import br.com.erudio.dto.PaginacaoDTO;
import br.com.erudio.negocio.escrita.TipoManifestacaoNegocioEscrita;
import br.com.erudio.negocio.leitura.TipoManifestacaoNegocioLeitura;
import br.com.erudio.negocio.vo.TipoManifestacaoVO;
import br.com.erudio.seguranca.util.ConstantesPermissoes;

@Path("tipoManifestacao")
@Produces(MediaType.APPLICATION_JSON)
public class TipoManifestacaoServico {

    @EJB
    private TipoManifestacaoNegocioEscrita tipoManifestacaoEscrita;

    @EJB
    private TipoManifestacaoNegocioLeitura tipoManifestacaoLeitura;

    @GET
    public Response buscarTodos() {
        return Response.ok(tipoManifestacaoLeitura.buscarTodos()).build();
    }

    @GET
    @Path("{id}/buscarTipoManifestacaoPorIdCategoria")
    @RolesAllowed({ConstantesPermissoes.ManterTipoManifestacao.CONSULTAR_TIPOS, ConstantesPermissoes.ManterTipoManifestacao.VISUALIZAR_TIPO})
    public Response buscarTipoManifestacaoVOPorIdCategoria(@PathParam("id") Integer idCategoria) {
        return Response.ok(tipoManifestacaoLeitura.buscarTipoManifestacaoVOPorIdCategoria(idCategoria)).build();
    }

    @PUT
    @Path("/incluir")
    @Consumes(MediaType.APPLICATION_JSON)
    @RolesAllowed({ConstantesPermissoes.ManterTipoManifestacao.INCLUIR_TIPO})
    public Response incluirTipoManifestacao(TipoManifestacaoVO tipoManifestacao) {
        return Response.ok(tipoManifestacaoEscrita.incluirTipoManifestacao(tipoManifestacao)).build();
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @RolesAllowed({ConstantesPermissoes.ManterTipoManifestacao.ALTERAR_TIPO})
    public Response alterarTipoManifestacao(TipoManifestacaoVO tipoManifestacao) {
        return Response.ok(tipoManifestacaoEscrita.alterarTipoManifestacao(tipoManifestacao)).build();
    }

    @DELETE
    @Path("/excluir/{id}")
    @RolesAllowed({ConstantesPermissoes.ManterTipoManifestacao.EXCLUIR_TIPO})
    public Response excluir(@PathParam("id") Integer idTipoManifestacao) {
        tipoManifestacaoEscrita.excluir(idTipoManifestacao);
        return Response.ok().build();
    }

    @POST
    @Path("/buscarTipoManifestacao")
    @Consumes(MediaType.APPLICATION_JSON)
    @RolesAllowed({ConstantesPermissoes.ManterTipoManifestacao.CONSULTAR_TIPOS, ConstantesPermissoes.ManterTipoManifestacao.VISUALIZAR_TIPO})
    public Response buscarTipoManifestacao(PaginacaoDTO<TipoManifestacaoVO> tipoManifestacaoVO) {
        tipoManifestacaoLeitura.buscarTipoManifestacao(tipoManifestacaoVO);
        return Response.ok(tipoManifestacaoVO).build();
    }

}