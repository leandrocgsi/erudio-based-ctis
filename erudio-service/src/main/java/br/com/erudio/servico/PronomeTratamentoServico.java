package br.com.erudio.servico;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.erudio.negocio.leitura.PronomeTratamentoNegocioLeitura;

@Path("pronomeTratamento")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class PronomeTratamentoServico {

    @EJB
    private PronomeTratamentoNegocioLeitura pronomeTratamentoLeitura;
    
    
    @GET
    public Response buscarTodosPronomeTratamento() {
        return Response.ok(pronomeTratamentoLeitura.buscarTodos()).build();
    }
    
}