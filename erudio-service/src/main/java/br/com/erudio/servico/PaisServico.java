package br.com.erudio.servico;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.erudio.negocio.leitura.PaisNegocioLeitura;

@Path("pais")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class PaisServico {

    @EJB
    private PaisNegocioLeitura paisLeitura;
    
    @GET
    public Response buscarTodosPaisesAtivos() {
        return Response.ok(paisLeitura.buscarTodosAtivos()).build();
    }
    
    @GET
    @Path("{nome}")
    public Response buscarTodosPaisesAtivos(@PathParam("nome") String nome) {
        return Response.ok(paisLeitura.buscarPorNome(nome)).build();
    }
    
}