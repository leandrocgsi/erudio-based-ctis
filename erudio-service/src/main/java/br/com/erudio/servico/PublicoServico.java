package br.com.erudio.servico;

import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.erudio.negocio.escrita.UsuarioNegocioEscrita;
import br.com.erudio.negocio.leitura.GrauInstrucaoNegocioLeitura;
import br.com.erudio.negocio.leitura.PaisNegocioLeitura;
import br.com.erudio.negocio.leitura.PronomeTratamentoNegocioLeitura;
import br.com.erudio.negocio.leitura.UnidadeFederativaNegocioLeitura;
import br.com.erudio.negocio.leitura.UsuarioNegocioLeitura;
import br.com.erudio.negocio.vo.UsuarioExternoVO;

@Path("public")
@Produces(MediaType.APPLICATION_JSON)
public class PublicoServico {

    @EJB
    private PronomeTratamentoNegocioLeitura pronomeTratamentoLeitura;

    @EJB
    private PaisNegocioLeitura paisLeitura;

    @EJB
    private UnidadeFederativaNegocioLeitura ufLeitura;

    @EJB
    private GrauInstrucaoNegocioLeitura grauInstrucaoLeitura;

    @EJB
    private UsuarioNegocioEscrita usuarioEscrita;

    @EJB
    private UsuarioNegocioLeitura usuarioLeitura;

    @GET
    @Path("/pronomeTratamento")
    public Response buscarTodosPronomeTratamento() {
        return Response.ok(pronomeTratamentoLeitura.buscarTodos()).build();
    }

    @GET
    @Path("/paisesAtivos")
    public Response buscarTodosPaisesAtivos() {
        return Response.ok(paisLeitura.buscarTodosAtivos()).build();
    }

    @GET
    @Path("/pais/{nome}")
    public Response buscarTodosPaisesAtivos(@PathParam("nome") String nome) {
        return Response.ok(paisLeitura.buscarPorNome(nome)).build();
    }

    @GET
    @Path("/uf")
    public Response buscarTodosUF() {
        return Response.ok(ufLeitura.buscarTodos()).build();
    }

    @GET
    @Path("/grauInstrucao")
    public Response buscarTodosGrauInstrucao() {
        return Response.ok(grauInstrucaoLeitura.buscarTodos()).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/cadastrarUsuarioExterno")
    public Response cadastrarUsuarioExterno(UsuarioExternoVO usuarioExternoVO) {
        usuarioEscrita.cadastrarUsuarioExterno(usuarioExternoVO);
        return Response.ok().build();
    }

    @GET
    @Path("/categorias")
    public Response recuperarCategorias() {
        return Response.ok(grauInstrucaoLeitura.buscarTodos()).build();
    }

    @GET
    @Path("/ativarCadastro/{codigo}")
    public Response ativarCadastro(@PathParam("codigo") String codigo) {
        Map<String, String> retorno = new HashMap<>();
        retorno.put("mensagem", usuarioEscrita.ativarCadastro(codigo));
        return Response.ok(retorno).build();
    }

    @GET
    @Path("/solicitarRedefinicaoSenha/{email}")
    public Response solicitarRedefinicaoSenha(@PathParam("email") String email) {
        usuarioEscrita.solicitarRedefinicaoSenha(email);
        return Response.ok().build();
    }

    @GET
    @Path("/buscarUsuarioExternoParaRedefinicaoSenha/{codigo}")
    public Response buscarUsuarioExternoParaRedefinicaoSenha(@PathParam("codigo") String codigo) {
        UsuarioExternoVO usuarioExternoVO = usuarioLeitura.buscarUsuarioExternoParaRedefinicaoSenha(codigo);
        return Response.ok(usuarioExternoVO).build();
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/redefinicaoSenha")
    public Response redefinicaoSenha(UsuarioExternoVO usuarioExternoVO) {
        usuarioEscrita.redefinicaoSenha(usuarioExternoVO);
        return Response.ok().build();
    }

}