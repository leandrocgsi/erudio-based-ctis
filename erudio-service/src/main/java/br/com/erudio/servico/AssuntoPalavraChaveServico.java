package br.com.erudio.servico;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.picketlink.authorization.annotations.RolesAllowed;

import br.com.erudio.dto.PaginacaoDTO;
import br.com.erudio.negocio.escrita.AssuntoPalavraChaveNegocioEscrita;
import br.com.erudio.negocio.leitura.AssuntoPalavraChaveNegocioLeitura;
import br.com.erudio.negocio.vo.AssuntoPalavraChaveVO;
import br.com.erudio.negocio.vo.PalavraChaveVO;
import br.com.erudio.negocio.vo.TipoAssuntoVO;
import br.com.erudio.seguranca.util.ConstantesPermissoes.ManterAssunto;
import br.com.erudio.seguranca.util.ConstantesPermissoes.ManterPalavraChave;

@Path("assuntoPalavraChave")
@Produces(MediaType.APPLICATION_JSON)
public class AssuntoPalavraChaveServico {
    @EJB
    private AssuntoPalavraChaveNegocioLeitura assuntoPalavraChaveLeitura;
    @EJB
    private AssuntoPalavraChaveNegocioEscrita assuntoPalavraChaveEscrita;

    @GET
    @Path("/assuntos")
    @RolesAllowed(ManterAssunto.CONSULTAR_ASSUNTO)
    public Response buscarTodosAssuntos() {
        return Response.ok(assuntoPalavraChaveLeitura.buscarTodosAssuntos()).build();
    }

    @GET
    @RolesAllowed({ManterPalavraChave.CONSULTAR_PALAVRA_CHAVE,ManterAssunto.INCLUIR_ASSUNTO,ManterAssunto.ALTERAR_ASSUNTO})
    @Path("/palavraChaves/{nome}")
    public Response buscarTodasPalavrasChavePorNome(@PathParam("nome") String nome) {
        return Response.ok(assuntoPalavraChaveLeitura.buscarPalavraChavesPorNome(nome)).build();
    }

    @GET
    @RolesAllowed(ManterPalavraChave.CONSULTAR_PALAVRA_CHAVE)
    @Path("/palavraChaves")
    public Response buscarTodasPalavrasChave() {
        return Response.ok(assuntoPalavraChaveLeitura.buscarPalavraChavesPorNome("")).build();

    }

    @GET
    @Path("/buscarAssunto/{id}")
    public Response buscarAssuntorId(@PathParam("id") Integer id) {
        return Response.ok(assuntoPalavraChaveLeitura.buscaAssuntoPorId(Long.valueOf(id))).build();
    }

    @GET
    @Path("/buscaPalavraChave/{id}")
    public Response buscarPalavraChaveId(@PathParam("id") Integer id) {
        return Response.ok(assuntoPalavraChaveLeitura.buscaPalavraChavePorId(Long.valueOf(id))).build();
    }

    @POST
    @RolesAllowed(ManterAssunto.INCLUIR_ASSUNTO)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/incluirAssunto")
    public Response incluirAssunto(TipoAssuntoVO assunto) {
        TipoAssuntoVO novoAssunto = assuntoPalavraChaveEscrita.inserirAssunto(assunto);
        return Response.ok(novoAssunto).build();
    }

    @PUT
    @RolesAllowed(ManterAssunto.ALTERAR_ASSUNTO)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/alterarAssunto")
    public Response alterarAssunto(TipoAssuntoVO assunto) {
        TipoAssuntoVO novoAssunto = assuntoPalavraChaveEscrita.alterarAssunto(assunto);
        return Response.ok(novoAssunto).build();
    }

    @POST
    @RolesAllowed(ManterPalavraChave.INCLUIR_PALAVRA_CHAVE)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/incluirPalavraChave")
    public Response incluirPalavraChave(PalavraChaveVO palavraChave) {
        PalavraChaveVO novaPalavraChave = assuntoPalavraChaveEscrita.gravarPalavraChave(palavraChave);
        return Response.ok(novaPalavraChave).build();

    }

    @PUT
    @RolesAllowed(ManterPalavraChave.ALTERAR_PALAVRA_CHAVE)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/alterarPalavraChave")
    public Response alterarPalavraChave(PalavraChaveVO palavraChave) {
        PalavraChaveVO novaPalavraChave = assuntoPalavraChaveEscrita.gravarPalavraChave(palavraChave);
        return Response.ok(novaPalavraChave).build();

    }

    @DELETE
    @RolesAllowed(ManterPalavraChave.EXCLUIR_PALAVRA_CHAVE)
    @Path("/excluirPalavraChave/{id}")
    public Response removerPalavraChave(@PathParam("id") Integer id) {
        assuntoPalavraChaveEscrita.excluirPalavraChave(id);
        return Response.ok().build();
    }

    @DELETE
    @RolesAllowed({ManterAssunto.EXCLUIR_ASSUNTO,ManterAssunto.CONSULTAR_ASSUNTO})
    @Path("/excluirTipoAssunto/{id}")
    public Response removerTipoAssunto(@PathParam("id") Integer id) {
        assuntoPalavraChaveEscrita.excluirTipoAssunto(id);
        return Response.ok().build();
    }

    @GET
    @Path("/verificarPalavraChaveVinculadaManifestacaoPorIdPalavraChave/{id}")
    public Response verificarPalavraChaveVinculadaManifestacao(@PathParam("id") Integer idPalavraChave) {
        Map<String, Boolean> retorno = new HashMap<>();
        retorno.put("palavraChaveVinculadaManifestacao",
                assuntoPalavraChaveLeitura.verificarPalavraChaveVinculadaManifestacaoPorIdPalavraChave(idPalavraChave));
        return Response.ok(retorno).build();
    }

    @GET
    @Path("/verificarTipoAssuntoVinculadoManifestacaoPorIdTipoAssunto/{id}")
    public Response verificarTipoAssuntoVinculadoManifestacao(@PathParam("id") Integer idTipoAssunto) {
        Map<String, Boolean> retorno = new HashMap<>();
        retorno.put("tipoAssuntoVinculadoManifestacao",
                assuntoPalavraChaveLeitura.verificarPalavraChaveVinculadaManifestacaoPorIdTipoAssunto(idTipoAssunto));
        return Response.ok(retorno).build();
    }

    @GET
    @Path("/verificarPalavraChaveVinculadaTipoAssuntoPorIdPalavraChave/{id}")
    public Response verificarPalavraChaveVinculadaTipoAssunto(@PathParam("id") Integer idPalavraChave) {
        Map<String, Boolean> retorno = new HashMap<>();
        retorno.put("palavraChaveVinculadaTipoAssunto",
                assuntoPalavraChaveLeitura.verificarPalavraChaveVinculadaTipoAssuntoPorIdPalavraChave(idPalavraChave));
        return Response.ok(retorno).build();
    }

    @GET
    @RolesAllowed({ManterAssunto.CONSULTAR_ASSUNTO,ManterAssunto.EXCLUIR_ASSUNTO})
    @Path("/verificarPalavraChaveVinculadaTipoAssuntoPorIdTipoAssunto/{id}")
    public Response verifcarPalavraChaveVinculadaTipoAssuntoPorIdTipoAssunto(@PathParam("id") Integer idTipoAssunto) {
        Map<String, Boolean> retorno = new HashMap<>();
        retorno.put("palavraChaveVinculadaTipoAssunto",
                assuntoPalavraChaveLeitura.verificarPalavraChaveVinculadaTipoAssuntoPorIdTipoAssunto(idTipoAssunto));
        return Response.ok(retorno).build();

    }

    @GET
    @RolesAllowed(ManterPalavraChave.VISUALIZAR_PALAVRA_CHAVE)
    @Path("/buscarAssuntoPalavraChaveVOPorIdPalavraChave/{id}")
    public Response buscarAssuntoPalavraChaveVOPorIdPalavraChave(@PathParam("id") Integer idPalavraChave) {
        List<AssuntoPalavraChaveVO> retorno = assuntoPalavraChaveLeitura
                .buscarAssuntoPalavraChaveVOPorIdPalavraChave(idPalavraChave);
        return Response.ok(retorno).build();
    }

    @GET
    @RolesAllowed({ManterAssunto.VISUALIZAR_ASSUNTO,ManterAssunto.ALTERAR_ASSUNTO})
    @Path("/buscarAssuntoPalavraChaveVOPorIdTipoAssunto/{id}")
    public Response buscarAssuntoPalavraChaveVOPorIdTipoAssunto(@PathParam("id") Integer idAssunto) {
        List<AssuntoPalavraChaveVO> retorno = assuntoPalavraChaveLeitura
                .buscarAssuntoPalavraChaveVoPorIdTipoAssunto(idAssunto);
        return Response.ok(retorno).build();
    }

    @POST
    @RolesAllowed({ManterPalavraChave.CONSULTAR_PALAVRA_CHAVE,ManterAssunto.INCLUIR_ASSUNTO})
    @Path("/buscarPalavraChave")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response buscarTodasPalavrasChaveVOPorNome(PaginacaoDTO<PalavraChaveVO> palavraChaveDTO) {
        assuntoPalavraChaveLeitura.buscarPalavraChave(palavraChaveDTO);
        return Response.ok(palavraChaveDTO).build();
    }

    @POST
    @Path("/buscarAssunto")
    @RolesAllowed(ManterAssunto.CONSULTAR_ASSUNTO)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response buscarTodosAssuntosVOPorNome(PaginacaoDTO<TipoAssuntoVO> assuntoDTO) {
        assuntoPalavraChaveLeitura.buscarAssunto(assuntoDTO);
        return Response.ok(assuntoDTO).build();

    }
}
