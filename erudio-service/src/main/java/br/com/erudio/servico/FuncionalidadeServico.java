package br.com.erudio.servico;

import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.erudio.negocio.leitura.FuncionalidadeAcessoNegocioLeitura;

@Path("perfil")

@Produces(MediaType.APPLICATION_JSON)
public class FuncionalidadeServico {
    
    @EJB
    private FuncionalidadeAcessoNegocioLeitura funcionalidadeLeitura;

    @GET
    @Path("/permissao/{id}")
    public Response buscarPermissaoPorID(@PathParam("id")Integer idFuncionalidade ){
        return Response.ok(funcionalidadeLeitura.recuperarTipoAcessosPorIdFuncionalidade(idFuncionalidade)).build();
//        return Response.ok().build();
    }
    
    @GET
    @Path("/funcionalidade")
    public Response buscarTodasFuncionalidades(){
    	return Response.ok(funcionalidadeLeitura.recuperarTodasFuncionalidadeVOsAtivos()).build();
    }
    
}
