package br.com.erudio.servico;

import java.util.List;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.picketlink.authorization.annotations.RolesAllowed;

import br.com.erudio.negocio.escrita.UnidadeNegocioEscrita;
import br.com.erudio.negocio.leitura.UnidadeNegocioLeitura;
import br.com.erudio.negocio.vo.UnidadeVO;
import br.com.erudio.seguranca.util.ConstantesPermissoes;

@Path("unidade")
@Produces(MediaType.APPLICATION_JSON)
public class UnidadeServico {

    @EJB
    private UnidadeNegocioEscrita unidadeEscrita;

    @EJB
    private UnidadeNegocioLeitura unidadeLeitura;

    @GET
    @RolesAllowed({ ConstantesPermissoes.GerenciarUnidade.VISUALIZAR_UNIDADE })
    public Response buscarTodas() {
        return Response.ok(unidadeLeitura.buscarTodas()).build();
    }

    @GET
    @Path("/buscarEstruturaOganizacional")
    @RolesAllowed({ ConstantesPermissoes.GerenciarUnidade.VISUALIZAR_UNIDADE })
    public Response buscarEstruturaOrganizacional() {
        return Response.ok(unidadeLeitura.buscarEstruturaOrganizacional()).build();
    }

    @PUT
    @Path("/atualizarStatus")
    @Consumes(MediaType.APPLICATION_JSON)
    @RolesAllowed({ ConstantesPermissoes.GerenciarUnidade.ALTERAR_UNIDADE })
    public Response atualizarStatus(List<UnidadeVO> unidades) {
        unidadeEscrita.atualizaStatus(unidades);
        return buscarTodas();
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @RolesAllowed({ ConstantesPermissoes.GerenciarUnidade.ALTERAR_UNIDADE })
    public Response alterarUnidade(UnidadeVO unidadeAlteracao) {
        UnidadeVO unidadeAtualizada = unidadeEscrita.alterar(unidadeAlteracao);
        return Response.ok(unidadeAtualizada).build();
    }

    @GET
    @Path("{id}/emails")
    @RolesAllowed({ConstantesPermissoes.GerenciarUnidade.VISUALIZAR_UNIDADE})
    public Response buscarTodosEmailUnidadePorIdUnidade(@PathParam("id") Integer idUnidade) {
        return Response.ok(unidadeLeitura.buscarTodosEmailUnidadeVOPorIdUnidade(idUnidade)).build();
    }

}
