package br.com.erudio.servico;

import java.util.List;

import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.picketlink.authorization.annotations.RolesAllowed;

import br.com.erudio.dto.PaginacaoDTO;
import br.com.erudio.builder.CategoriasManifestacaoBuilder;
import br.com.erudio.negocio.escrita.CategoriaManifestacaoNegocioEscrita;
import br.com.erudio.negocio.leitura.CategoriaManifestacaoNegocioLeitura;
import br.com.erudio.negocio.vo.CategoriaManifestacaoVO;
import br.com.erudio.seguranca.util.ConstantesPermissoes;

@Path("categoriaManifestacao")
@Produces(MediaType.APPLICATION_JSON)
public class CategoriaManifestacaoServico {

    @EJB
    private CategoriaManifestacaoNegocioLeitura categoriaManifestacaoLeitura;

    @EJB
    private CategoriaManifestacaoNegocioEscrita categoriaManifestacaoEscrita;

    @GET
    @RolesAllowed({ConstantesPermissoes.ManterCategoriaManifestacao.CONSULTAR_CATEGORIAS,ConstantesPermissoes.ManterCategoriaManifestacao.VISUALIZAR_CATEGORIA,ConstantesPermissoes.ManterTipoManifestacao.CONSULTAR_TIPOS})
    public Response buscarTodas() {
        return Response.ok(categoriaManifestacaoLeitura.buscarTodas()).build();
    }

    @GET
    @Path("/buscarCategoriasManifestacaoAtiva")
    @RolesAllowed({ ConstantesPermissoes.ManterCategoriaManifestacao.CONSULTAR_CATEGORIAS,
            ConstantesPermissoes.ManterCategoriaManifestacao.VISUALIZAR_CATEGORIA,
            ConstantesPermissoes.ManterTipoManifestacao.VISUALIZAR_TIPO,
            ConstantesPermissoes.ManterTipoManifestacao.CONSULTAR_TIPOS,
            ConstantesPermissoes.ManterTipoManifestacao.ALTERAR_TIPO,
            ConstantesPermissoes.ManterTipoManifestacao.INCLUIR_TIPO })
    public Response buscarCategoriaManifestacaoAtiva(@Context HttpServletRequest request) {
        List<CategoriaManifestacaoVO> categorias = 
                new CategoriasManifestacaoBuilder(request.getLocale())
                .buildDescricaoCombo(categoriaManifestacaoLeitura.buscarTodasAtiva())
                .build();
        return Response.ok(categorias).build();
    }

    @PUT
    @Path("/incluir")
    @Consumes(MediaType.APPLICATION_JSON)
    @RolesAllowed({ConstantesPermissoes.ManterCategoriaManifestacao.INCLUIR_CATEGORIA})
    public Response incluir(CategoriaManifestacaoVO categoriaManifestacao) {
        return Response.ok(categoriaManifestacaoEscrita.incluir(categoriaManifestacao)).build();
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @RolesAllowed({ConstantesPermissoes.ManterCategoriaManifestacao.ALTERAR_CATEGORIA})
    public Response alterar(CategoriaManifestacaoVO categoriaManifestacao) {
        return Response.ok(categoriaManifestacaoEscrita.alterar(categoriaManifestacao)).build();
    }

    @POST
    @Path("/buscarCategoriaManifestacao")
    @Consumes(MediaType.APPLICATION_JSON)
    @RolesAllowed({ConstantesPermissoes.ManterCategoriaManifestacao.CONSULTAR_CATEGORIAS})
    public Response buscarTipoManifestacao(PaginacaoDTO<CategoriaManifestacaoVO> categoriaManifestacaoVO) {
        categoriaManifestacaoLeitura.buscarCategoriaManifestacao(categoriaManifestacaoVO);
        return Response.ok(categoriaManifestacaoVO).build();
    }

    @DELETE
    @Path("/excluir/{id}")
    @RolesAllowed({ConstantesPermissoes.ManterCategoriaManifestacao.EXCLUIR_CATEGORIA})
    public Response excluir(@PathParam("id") Integer idCategoriaManifestacao) {
        categoriaManifestacaoEscrita.excluir(idCategoriaManifestacao);
        return Response.ok().build();
    }

}
