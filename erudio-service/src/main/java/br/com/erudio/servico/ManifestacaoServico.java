package br.com.erudio.servico;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.erudio.negocio.escrita.ManifestacaoNegocioEscrita;
import br.com.erudio.negocio.leitura.ManifestacaoNegocioLeitura;
import br.com.erudio.negocio.vo.ManifestacaoVO;

@Path("manifestacao")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ManifestacaoServico {

    @EJB
    private ManifestacaoNegocioLeitura manifestacaoLeitura;
    
    @EJB
    private ManifestacaoNegocioEscrita manifestacao;
    
    
    
    @POST
    public Response cadastrar(ManifestacaoVO manifestacaoVO){
        manifestacao.cadastrarManifestacao(manifestacaoVO);
        return Response.ok(manifestacaoVO).build();
    }

    @GET
    @Path("recuperarTiposDeRelacionamentos")
    public Response recuperarTiposRelacionamentos() {
        return Response.ok(manifestacaoLeitura.recuperarTiposRelacionamentos()).build();
    }

    @GET
    @Path("recuperarTiposRespostas")
    public Response recuperarTiposRespostas() {
        return Response.ok(manifestacaoLeitura.recuperarTiposRespostas()).build();
    }
    

}
