package br.com.erudio.servico;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.HEAD;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.picketlink.Identity;
import org.picketlink.authorization.annotations.LoggedIn;
import org.picketlink.credential.DefaultLoginCredentials;
import org.picketlink.idm.credential.Token;

import br.com.erudio.negocio.vo.PerfilAcessoVO;
import br.com.erudio.seguranca.AutenticacaoModelManager;
import br.com.erudio.seguranca.model.AutenticacaoToken;
import br.com.erudio.seguranca.model.UsuarioAutenticacao;
import br.com.erudio.seguranca.util.ConstantesPermissoes;
import br.com.erudio.seguranca.vo.PerfilAutenticacaoVO;
import br.com.erudio.seguranca.vo.UsuarioLogadoVO;

@Path("/autenticacao")
public class LoginServico {

    @Inject
    private Identity identity;
    
    @Context
    private HttpServletRequest request;
    
    @Inject
    private AutenticacaoModelManager autenticacaoModelManager;
    
    @Inject
    private Token.Provider<AutenticacaoToken> tokenProvider;
    
    @GET
    @LoggedIn
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/usuarioLogado")
    public Response recuperarInformacoesUsuario(DefaultLoginCredentials credentials){
        UsuarioAutenticacao account = (UsuarioAutenticacao)this.identity.getAccount();
        UsuarioLogadoVO usuarioLogado = criarUsuarioLogadoVO(account);
        return Response.ok(usuarioLogado).build();
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/todasPermissoes")
    public Response recuperarRodasPermissoes(){
        return Response.ok(ConstantesPermissoes.TODAS_PERMISSOES).build();
    }    
    
    @HEAD
    @LoggedIn
    @Path("/usuarioLogado")
    public Response isUsuarioLogado(){
        return Response.ok().build();
    }
    
    @GET
    @LoggedIn
    @Path("logout")
    public void efetuarLogout(){
        this.tokenProvider.invalidate(identity.getAccount());
        this.identity.logout();
    }
    
    private UsuarioLogadoVO criarUsuarioLogadoVO(UsuarioAutenticacao ua){
        UsuarioLogadoVO usuarioLogado = new UsuarioLogadoVO();
        PerfilAcessoVO perfil = ua.getUsuario().getPerfil();
        
        usuarioLogado.setNome(ua.getUsuario().getPessoa().getNome());
        usuarioLogado.setNomeUsuario(ua.getNomeUsuario());
        usuarioLogado.setTipo(ua.getTipoUsuario().toString());
        usuarioLogado.setPerfil(new PerfilAutenticacaoVO(perfil.getId(), perfil.getNome()));
        usuarioLogado.setPermissoes(autenticacaoModelManager.recuperarPermissoesUsuario(ua));
        return usuarioLogado;
    }

}
