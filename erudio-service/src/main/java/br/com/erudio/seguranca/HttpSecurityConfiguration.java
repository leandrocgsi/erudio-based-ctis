package br.com.erudio.seguranca;

import javax.enterprise.event.Observes;

import org.picketlink.config.SecurityConfigurationBuilder;
import org.picketlink.event.SecurityConfigurationEvent;

public class HttpSecurityConfiguration {

    public void onInit(@Observes SecurityConfigurationEvent event){
        
        SecurityConfigurationBuilder builder = event.getBuilder();
        
//        builder.identity().stateless()
//            .http()
//                .forPath("/rest/*")
//                    .unprotected()
//                    .cors()
//                        .allowMethods("GET", "PUT", "POST", "DELETE", "OPTIONS")
//                        .allowAll();
        
        builder
            .identity()
                .stateless()
            .http()
                .forPath("/rest/*")
                    .authenticateWith()
                        .token()
                    .cors()
                        .allowMethods("GET", "PUT", "POST", "DELETE", "OPTIONS", "HEAD")
                        .allowAll()
                .forPath("/rest/public/*")
                    .unprotected()
                    .cors()
                    .allowMethods("GET", "PUT", "POST", "DELETE", "OPTIONS", "HEAD")
                    .allowAll();
        
    }
    
}
