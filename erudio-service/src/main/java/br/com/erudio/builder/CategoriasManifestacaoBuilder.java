package br.com.erudio.builder;

import java.util.List;
import java.util.Locale;

import org.apache.commons.lang.StringUtils;

import br.com.erudio.mensagem.Mensagem;
import br.com.erudio.negocio.vo.CategoriaManifestacaoVO;

public class CategoriasManifestacaoBuilder {

    private Locale locale;

    private List<CategoriaManifestacaoVO> categorias;

    public CategoriasManifestacaoBuilder(Locale locale) {
        this.locale = locale;
    }

    public CategoriasManifestacaoBuilder buildDescricaoCombo(List<CategoriaManifestacaoVO> categorias) {
        this.categorias = categorias;
        for (CategoriaManifestacaoVO categoriaManifestacao : categorias) {
            String descricaoCombo = Mensagem.getMensagem(locale, categoriaManifestacao.getNome());
            if (StringUtils.isNotBlank(descricaoCombo)) {
                categoriaManifestacao.setDescricaoCombo(descricaoCombo);
            } else {
                categoriaManifestacao.setDescricaoCombo(categoriaManifestacao.getNome());
            }
        }
        return this;
    }

    public List<CategoriaManifestacaoVO> build() {
        return categorias;
    }

}
