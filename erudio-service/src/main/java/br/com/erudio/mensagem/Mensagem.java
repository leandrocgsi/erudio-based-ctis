package br.com.erudio.mensagem;

import java.util.Locale;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Mensagem {

    private static final Logger LOGGER = Logger.getLogger(Mensagem.class.getName());
    
    private Mensagem () {
        
    }
    
    public static String  getMensagem(Locale locale,String chave){
        try {
            return ResourceBundle.getBundle("messages", locale, Thread.currentThread().getContextClassLoader()).getString(chave);
        } catch (java.util.MissingResourceException e) {
            LOGGER.log(Level.FINE, e.getMessage(), e);
            return null;
        }
    }

}
