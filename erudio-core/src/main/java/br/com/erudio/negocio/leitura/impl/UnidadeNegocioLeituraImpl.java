package br.com.erudio.negocio.leitura.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import br.com.erudio.modelo.EmailUnidade;
import br.com.erudio.modelo.Unidade;
import br.com.erudio.negocio.NegocioBaseErudio;
import br.com.erudio.negocio.leitura.UnidadeNegocioLeitura;
import br.com.erudio.negocio.vo.EmailUnidadeVO;
import br.com.erudio.negocio.vo.UnidadeVO;
import br.com.erudio.persistencia.PersistenciaErudio;

@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class UnidadeNegocioLeituraImpl extends NegocioBaseErudio implements UnidadeNegocioLeitura {

    @EJB
    private PersistenciaErudio<Integer, Unidade> persistencia;

    @EJB
    private PersistenciaErudio<Integer, EmailUnidade> persistenciaEmailUnidade;

    @Override
    public List<UnidadeVO> buscarTodas() {
        List<Unidade> unidades = persistencia.obterTodos(Unidade.class);
        return converter(unidades, UnidadeVO.class);
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public List<EmailUnidadeVO> buscarTodosEmailUnidadeVOPorIdUnidade(Integer idUnidade) {
        List<EmailUnidadeVO> emailUnidadeVOs = new ArrayList<>();
        if (idUnidade != null) {
            emailUnidadeVOs = converter(buscarTodosEmailUnidadePorIdUnidade(idUnidade), EmailUnidadeVO.class);
        }
        return emailUnidadeVOs;
    }

    /**
     * Este método retorna todos os emails de uma unidade, entretanto estes não
     * estão com o atributo "unidade" setado.
     */
    @Override
    public List<EmailUnidade> buscarTodosEmailUnidadePorIdUnidade(Integer idUnidade) {
        List<EmailUnidade> emailUnidades = new ArrayList<>();
        if (idUnidade != null) {
            CriteriaBuilder cb = persistenciaEmailUnidade.getEntityManager().getCriteriaBuilder();
            CriteriaQuery<EmailUnidade> criteriaQuery = cb.createQuery(EmailUnidade.class);
            Root<EmailUnidade> fromEmailUnidade = criteriaQuery.from(EmailUnidade.class);

            criteriaQuery.multiselect(fromEmailUnidade.get("id"), fromEmailUnidade.get("email"))
                    .where(cb.equal(fromEmailUnidade.get("unidade").get("id"), idUnidade));

            emailUnidades = persistenciaEmailUnidade.getEntityManager().createQuery(criteriaQuery).getResultList();

            return emailUnidades;
        }
        return emailUnidades;

    }

    @Override
    public List<UnidadeVO> buscarEstruturaOrganizacional() {
        List<Unidade> unidadesPai = buscarUnidadesSemPai();
        List<UnidadeVO> estruturaOrganizacional = converter(unidadesPai, UnidadeVO.class);
        for (UnidadeVO unidadeVO : estruturaOrganizacional) {
            popularUnidadesFilhas(unidadeVO);
        }
        return estruturaOrganizacional;
    }

    private void popularUnidadesFilhas(UnidadeVO unidadeVO) {
        List<Unidade> unidadesFilhas = buscarUnidadesFilhas(unidadeVO.getId());
        List<UnidadeVO> unidadeFilhaVOs = converter(unidadesFilhas, UnidadeVO.class);
        for (UnidadeVO unidadeFilhaVO : unidadeFilhaVOs) {
            popularUnidadesFilhas(unidadeFilhaVO);
        }
        unidadeVO.setUnidadeFilhaVOs(unidadeFilhaVOs);
    }

    private List<Unidade> buscarUnidadesFilhas(Integer idUnidadePai) {
        CriteriaBuilder cb = persistenciaEmailUnidade.getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Unidade> criteriaQuery = cb.createQuery(Unidade.class);
        Root<Unidade> fromUnidade = criteriaQuery.from(Unidade.class);

        criteriaQuery.select(fromUnidade).where(cb.equal(fromUnidade.get("unidadePai").get("id"), idUnidadePai),
                cb.and(cb.equal(fromUnidade.get("ativo"), Boolean.TRUE)));

        return persistenciaEmailUnidade.getEntityManager().createQuery(criteriaQuery).getResultList();
    }

    private List<Unidade> buscarUnidadesSemPai() {
        CriteriaBuilder cb = persistenciaEmailUnidade.getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Unidade> criteriaQuery = cb.createQuery(Unidade.class);
        Root<Unidade> fromUnidade = criteriaQuery.from(Unidade.class);

        criteriaQuery.select(fromUnidade).where(cb.isNull(fromUnidade.get("unidadePai")),
                cb.and(cb.equal(fromUnidade.get("ativo"), Boolean.TRUE)));

        return persistenciaEmailUnidade.getEntityManager().createQuery(criteriaQuery).getResultList();
    }

}