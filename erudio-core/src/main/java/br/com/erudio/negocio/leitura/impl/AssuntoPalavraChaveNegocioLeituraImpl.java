package br.com.erudio.negocio.leitura.impl;

import java.util.List;

import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.apache.commons.lang3.StringUtils;

import br.com.erudio.dto.PaginacaoDTO;
import br.com.erudio.exception.NegocioException;
import br.com.erudio.modelo.AssuntoPalavraChave;
import br.com.erudio.modelo.PalavraChave;
import br.com.erudio.modelo.TipoAssunto;
import br.com.erudio.modelo.chavecomposta.AssuntoPalavraChaveId;
import br.com.erudio.negocio.NegocioBaseErudio;
import br.com.erudio.negocio.leitura.AssuntoPalavraChaveNegocioLeitura;
import br.com.erudio.negocio.vo.AssuntoPalavraChaveVO;
import br.com.erudio.negocio.vo.PalavraChaveVO;
import br.com.erudio.negocio.vo.TipoAssuntoVO;
import br.com.erudio.persistencia.PersistenciaErudio;
import br.com.erudio.util.Constante;

@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class AssuntoPalavraChaveNegocioLeituraImpl extends NegocioBaseErudio
        implements AssuntoPalavraChaveNegocioLeitura {

    @EJB
    private PersistenciaErudio<Integer, TipoAssunto> persistenciaTipoAssunto;

    @EJB
    private PersistenciaErudio<Integer, PalavraChave> persistenciaPalavraChave;

    @EJB
    private PersistenciaErudio<AssuntoPalavraChaveId, AssuntoPalavraChave> persistenciaAssuntoPalavraChave;

    @Override
    public TipoAssuntoVO buscaAssuntoPorId(Long id) {
        Integer idValor = Integer.parseInt(id.toString());
        TipoAssunto tipoAssunto = persistenciaTipoAssunto.obter(TipoAssunto.class, idValor);
        if (tipoAssunto == null) {
            NegocioException.throwExceptionErroIfNull(tipoAssunto, "Registro não encontrado.");
        }
        return converter(tipoAssunto, TipoAssuntoVO.class);
    }

    @Override
    public PalavraChaveVO buscaPalavraChavePorId(Long id) {
        Integer idValor = Integer.parseInt(id.toString());
        PalavraChave palavraChave = persistenciaPalavraChave.obter(PalavraChave.class, idValor);
        if (palavraChave == null) {
            NegocioException.throwExceptionErroIfNull(palavraChave, "Registro não encontrado.");
        }
        return converter(palavraChave, PalavraChaveVO.class);
    }

    @Override
    public List<PalavraChaveVO> buscarPalavraChavesPorNome(String nome) {
        String jpql = "select pc from PalavraChave pc where pc.nome like :nome and pc.ativo = 'S'";
        Query query = persistenciaAssuntoPalavraChave.getEntityManager().createQuery(jpql);
        query.setParameter("nome", nome + "%");
        return converter(query.getResultList(), PalavraChaveVO.class);
    }

    @Override
    public List<TipoAssuntoVO> buscarTodosAssuntos() {
        List<TipoAssunto> tipoAssunto = persistenciaTipoAssunto.obterTodos(TipoAssunto.class);
        return converter(tipoAssunto, TipoAssuntoVO.class);
    }

    @Override
    public Boolean verificarPalavraChaveVinculadaManifestacaoPorIdPalavraChave(Integer idPalavraChave) {

        return false;
    }

    /**
     * PARA_FAZER: Deve ser implementado no pacote PC3
     */
    @Override
    public Boolean verificarPalavraChaveVinculadaManifestacaoPorIdTipoAssunto(Integer idTipoAssunto) {
        return Boolean.FALSE;
    }

    @Override
    public Boolean verificarPalavraChaveVinculadaTipoAssuntoPorIdPalavraChave(Integer idPalavraChave) {
        return !buscarAssuntoPalavraChavePorIdPalavraChave(idPalavraChave).isEmpty();
    }

    @Override
    public Boolean verificarPalavraChaveVinculadaTipoAssuntoPorIdTipoAssunto(Integer idTipoAssunto) {
        return !buscarAssuntoPalavraChavePorIdTipoAssunto(idTipoAssunto).isEmpty();
    }

    @Override
    public List<AssuntoPalavraChave> buscarAssuntoPalavraChavePorIdPalavraChave(Integer idPalavraChave) {
        String jpql = "select apc from AssuntoPalavraChave apc where apc.palavraChave.id = :idPalavraChave";
        Query query = persistenciaAssuntoPalavraChave.getEntityManager().createQuery(jpql);
        query.setParameter("idPalavraChave", idPalavraChave);
        return query.getResultList();
    }

    @Override
    public List<AssuntoPalavraChave> buscarAssuntoPalavraChavePorIdTipoAssunto(Integer idTipoAssunto) {
        String jpql = "select apc from AssuntoPalavraChave apc where apc.tipoAssunto.id = :idTipoAssunto";
        Query query = persistenciaAssuntoPalavraChave.getEntityManager().createQuery(jpql);
        query.setParameter("idTipoAssunto", idTipoAssunto);
        return query.getResultList();
    }

    @Override
    public void buscarPalavraChave(PaginacaoDTO<PalavraChaveVO> palavraChaveDTO) {
        Long total = getQueryTotalBuscaPalavraChave(palavraChaveDTO).getSingleResult();
        List<PalavraChave> palavraChaves = getQueryResultadoBuscaPalavraChave(palavraChaveDTO).getResultList();

        List<PalavraChaveVO> palavraChaveVOs = converter(palavraChaves, PalavraChaveVO.class);
        palavraChaveDTO.setList(palavraChaveVOs);
        palavraChaveDTO.setTotalResults(total.intValue());
    }

    @Override
    public void buscarAssunto(PaginacaoDTO<TipoAssuntoVO> assuntoDTO) {
        Long total = getQueryTotalBuscaAssunto(assuntoDTO).getSingleResult();
        List<TipoAssunto> assuntos = getQueryResultadoBuscaAssunto(assuntoDTO).getResultList();

        List<TipoAssuntoVO> assuntoVOs = converter(assuntos, TipoAssuntoVO.class);
        assuntoDTO.setList(assuntoVOs);
        assuntoDTO.setTotalResults(total.intValue());
    }

    private TypedQuery<Long> getQueryTotalBuscaAssunto(PaginacaoDTO<TipoAssuntoVO> assuntoDTO) {
        String jpql = getJpqlBuscaAssunto(assuntoDTO, "count(ta)");
        TypedQuery<Long> query = persistenciaAssuntoPalavraChave.getEntityManager().createQuery(jpql, Long.class);
        setFiltrosBuscaAssunto(query, assuntoDTO);
        return query;
    }

    private TypedQuery<Long> getQueryTotalBuscaPalavraChave(PaginacaoDTO<PalavraChaveVO> palavraChaveDTO) {
        String jpql = getJpqlBuscaPalavraChave(palavraChaveDTO, "count(pc)");
        TypedQuery<Long> query = persistenciaAssuntoPalavraChave.getEntityManager().createQuery(jpql, Long.class);
        setFiltosBuscaPalavraChave(query, palavraChaveDTO);
        return query;
    }

    private Query getQueryResultadoBuscaPalavraChave(PaginacaoDTO<PalavraChaveVO> palavraChaveDTO) {
        String jpql = getJpqlBuscaPalavraChave(palavraChaveDTO, "pc");
        jpql = configurarOrdenacao(jpql, "pc", palavraChaveDTO);
        Query query = persistenciaAssuntoPalavraChave.getEntityManager().createQuery(jpql);
        setFiltosBuscaPalavraChave(query, palavraChaveDTO);
        configurarPaginacao(query, palavraChaveDTO);
        return query;
    }

    private Query getQueryResultadoBuscaAssunto(PaginacaoDTO<TipoAssuntoVO> assuntoDTO) {
        String jpql = getJpqlBuscaAssunto(assuntoDTO, "ta");
        jpql = configurarOrdenacao(jpql, "ta", assuntoDTO);
        Query query = persistenciaAssuntoPalavraChave.getEntityManager().createQuery(jpql);
        setFiltrosBuscaAssunto(query, assuntoDTO);
        configurarPaginacao(query, assuntoDTO);
        return query;
    }

    private String getJpqlBuscaPalavraChave(PaginacaoDTO<PalavraChaveVO> palavraChaveDTO, String retorno) {
        String filtroNome = palavraChaveDTO.getFiltros().get("nome").toString();

        Boolean filtroAtivo = (Boolean) palavraChaveDTO.getFiltros().get(Constante.ATIVO);
        
        List<String> palavrasChavesExistentes = (List<String>) palavraChaveDTO.getFiltros().get("palavrasChavesExistentes");

        StringBuilder jpql = new StringBuilder();

        jpql.append("select ")

                .append(retorno)

                .append(" from PalavraChave pc");

        String filtro = " where ";
        if (!StringUtils.isEmpty(filtroNome)) {
            jpql.append(filtro).append("pc.nome = :nome");
            filtro = Constante.AND;
        }
        if (filtroAtivo != null) {
            jpql.append(filtro).append("pc.ativo = :ativo");
            filtro = Constante.AND;
        }
        if (palavrasChavesExistentes != null && !palavrasChavesExistentes.isEmpty()) {
            jpql.append(filtro).append("pc.nome not in (:palavrasChavesExistentes)");
        }
        return jpql.toString();
    }

    private String getJpqlBuscaAssunto(PaginacaoDTO<TipoAssuntoVO> assuntoDTO, String retorno) {
        String filtroNome = assuntoDTO.getFiltros().get("nome").toString();

        Boolean filtroAtivo = (Boolean) assuntoDTO.getFiltros().get(Constante.ATIVO);

        StringBuilder jpql = new StringBuilder();

        jpql.append("select ").append(retorno).append(" from TipoAssunto ta");
        String filtro = " where ";
        if (!StringUtils.isEmpty(filtroNome)) {
            jpql.append(filtro).append("ta.nome = :nome");
            filtro = Constante.AND;
        }
        if (filtroAtivo != null) {
            jpql.append(filtro).append("ta.ativo = :ativo");
            filtro = Constante.AND;
        }
        return jpql.toString();
    }

    private void setFiltosBuscaPalavraChave(Query query, PaginacaoDTO<PalavraChaveVO> palavraChaveDTO) {
        String filtroNome = palavraChaveDTO.getFiltros().get("nome").toString();
        Boolean filtroAtivo = (Boolean) palavraChaveDTO.getFiltros().get(Constante.ATIVO);
        List<String> palavrasChavesExistentes = (List<String>) palavraChaveDTO.getFiltros().get("palavrasChavesExistentes");

        if (!StringUtils.isEmpty(filtroNome)) {
            query.setParameter("nome", filtroNome);
        }
        if (filtroAtivo != null) {
            query.setParameter(Constante.ATIVO, filtroAtivo);
        }
        if (palavrasChavesExistentes != null && !palavrasChavesExistentes.isEmpty()) {
            query.setParameter("palavrasChavesExistentes", palavrasChavesExistentes);
        }
    }

    private void setFiltrosBuscaAssunto(Query query, PaginacaoDTO<TipoAssuntoVO> assuntoDTO) {
        String filtroNome = assuntoDTO.getFiltros().get("nome").toString();
        Boolean filtroAtivo = (Boolean) assuntoDTO.getFiltros().get(Constante.ATIVO);
        if (!StringUtils.isEmpty(filtroNome)) {
            query.setParameter("nome", filtroNome);
        }
        if (filtroAtivo != null) {
            query.setParameter(Constante.ATIVO, filtroAtivo);
        }
    }

    @Override
    public PalavraChave buscarPalavraChavePorNome(String nome) {
        String jpql = "select pc from PalavraChave pc where pc.nome = :nome";
        Query query = persistenciaAssuntoPalavraChave.getEntityManager().createQuery(jpql);
        query.setParameter("nome", nome);
        return (PalavraChave) query.getSingleResult();
    }
    
    @Override
    public TipoAssunto buscarTipoAssuntoPorNome(String nome) {
        String jpql = "select ta from TipoAssunto ta where ta.nome = :nome";
        Query query = persistenciaAssuntoPalavraChave.getEntityManager().createQuery(jpql);
        query.setParameter("nome", nome);
        return (TipoAssunto)query.getSingleResult();
    }

    @Override
    public List<AssuntoPalavraChaveVO> buscarAssuntoPalavraChaveVOPorIdPalavraChave(Integer idPalavraChave) {
        List<AssuntoPalavraChave> assuntoPalavraChaves = buscarAssuntoPalavraChavePorIdPalavraChave(idPalavraChave);
        return converter(assuntoPalavraChaves, AssuntoPalavraChaveVO.class);
    }

    @Override
    public List<AssuntoPalavraChaveVO> buscarAssuntoPalavraChaveVoPorIdTipoAssunto(Integer idTipoAssunto) {
        List<AssuntoPalavraChave> assuntoPalavraChaves = buscarAssuntoPalavraChavePorIdTipoAssunto(idTipoAssunto);
        return converter(assuntoPalavraChaves, AssuntoPalavraChaveVO.class);
    }

}
