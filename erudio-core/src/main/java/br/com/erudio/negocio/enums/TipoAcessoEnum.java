package br.com.erudio.negocio.enums;

import br.com.erudio.util.Constante;

public enum TipoAcessoEnum {
    
    INCLUIR(Constante.UM, "INCLUIR"),
    EXCLUIR(Constante.QUATRO, "EXCLUIR"),
    ALTERAR(Constante.DOIS, "ALTERAR"),
    CONSULTAR(Constante.TRES, "CONSULTAR"),
    IMPRIMIR(Constante.CINCO, "IMPRIMIR");

    private Long codigo;
    private String descricao;

    TipoAcessoEnum(Long codigo, String descricao) {
        this.codigo = codigo;
        this.descricao = descricao;
    }

    public Long getCodigo() {
        return codigo;
    }

    public void setCodigo(Long codigo) {
        this.codigo = codigo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getIncluir() {
        return TipoAcessoEnum.INCLUIR.descricao;
    }

    public String getExcluir() {
        return TipoAcessoEnum.EXCLUIR.descricao;
    }

    public String getAlterar() {
        return TipoAcessoEnum.ALTERAR.descricao;
    }

    public String getConsultar() {
        return TipoAcessoEnum.CONSULTAR.descricao;
    }

    public String getImprimir() {
        return TipoAcessoEnum.IMPRIMIR.descricao;
    }

}
