package br.com.erudio.negocio.enums;

public enum PerfilAcessoEnum {
    USUARIO_EXTERNO("USUARIO_EXTERNO", "Usuário Externo"),
    USUARIO_INTERNO("USUARIO_INTERNO", "Usuário Interno");
    
    private String nome;
    private String descricao;
    
    private PerfilAcessoEnum(String nome, String descricao) {
        this.nome = nome;
        this.descricao = descricao;
    }
    
    public String getNome(){
        return nome;
    }

    public String getDescricao() {
        return descricao;
    }
    
}
