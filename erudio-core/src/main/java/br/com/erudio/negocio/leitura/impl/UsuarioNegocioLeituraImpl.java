package br.com.erudio.negocio.leitura.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;

import br.com.erudio.dto.PaginacaoDTO;
import br.com.erudio.exception.NegocioException;
import br.com.erudio.modelo.PerfilAcesso;
import br.com.erudio.modelo.SenhaUsuarioExterno;
import br.com.erudio.modelo.SolicitacaoRedefinicaoSenha;
import br.com.erudio.modelo.TipoCargo;
import br.com.erudio.modelo.Usuario;
import br.com.erudio.modelo.UsuarioExterno;
import br.com.erudio.modelo.UsuarioInterno;
import br.com.erudio.modelo.view.CargoUsuarioInternoView;
import br.com.erudio.negocio.NegocioBaseErudio;
import br.com.erudio.negocio.leitura.UsuarioNegocioLeitura;
import br.com.erudio.negocio.vo.CargoVO;
import br.com.erudio.negocio.vo.UsuarioExternoVO;
import br.com.erudio.negocio.vo.UsuarioInternoVO;
import br.com.erudio.negocio.vo.UsuarioVO;
import br.com.erudio.persistencia.PersistenciaErudio;
import br.com.erudio.util.Constante;
import br.com.erudio.util.Util;

@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class UsuarioNegocioLeituraImpl extends NegocioBaseErudio implements UsuarioNegocioLeitura {
    
    private static final int MAX_RESULTADO = 3;

    @EJB
    private PersistenciaErudio<Integer, PerfilAcesso> persistencia;

    @EJB
    private PersistenciaErudio<Integer, UsuarioExterno> persistenciaUsuarioExterno;
    
    @EJB
    private PersistenciaErudio<Integer, UsuarioInterno> persistenciaUsuarioInterno;
      
    @Override
    public UsuarioVO detalharUsuario(Integer id) {
       
       String select = "from Usuario u where u.id = " + id; 
       Usuario usuario = persistencia.getEntityManager().createQuery(select,
       Usuario.class).getSingleResult(); 
       
       return converter(usuario,UsuarioVO.class);
       
    }
    
    @Override
    public UsuarioExterno buscarUsuarioExternoPorEmail(String email) {
        CriteriaBuilder cb = persistenciaUsuarioExterno.getEntityManager().getCriteriaBuilder();
        CriteriaQuery<UsuarioExterno> criteriaQuery = cb.createQuery(UsuarioExterno.class);
        Root<UsuarioExterno> fromUsuarioExterno = criteriaQuery.from(UsuarioExterno.class);
        criteriaQuery.select(fromUsuarioExterno)
                .where(cb.equal(fromUsuarioExterno.get("usuario").get("pessoa").get("email"), email));

        return persistenciaUsuarioExterno.getEntityManager().createQuery(criteriaQuery).getSingleResult();
    }

    @Override

    public UsuarioInterno buscarUsuarioInternoPorLoginRede(String loginRede){
        CriteriaBuilder cb = persistenciaUsuarioInterno.getEntityManager().getCriteriaBuilder();
        CriteriaQuery<UsuarioInterno> criteriaQuery = cb.createQuery(UsuarioInterno.class);
        Root<UsuarioInterno> fromUsuarioInterno = criteriaQuery.from(UsuarioInterno.class);
        criteriaQuery.select(fromUsuarioInterno)
            .where(cb.equal(fromUsuarioInterno.get("loginRede"), loginRede));
        
        return persistenciaUsuarioInterno.getEntityManager().createQuery(criteriaQuery).getSingleResult();
    }

    @Override
    public UsuarioInternoVO buscarUsuarioInternoVOTitularUnidade(Integer idUnidade) {

        Integer idUsuarioInterno = recuperarIdUsuarioInternoTitularUnidade(idUnidade);

        UsuarioInterno usuarioInternos = persistenciaUsuarioInterno.obter(UsuarioInterno.class, idUsuarioInterno);

        return converter(usuarioInternos, UsuarioInternoVO.class);

    }

    private Integer recuperarIdUsuarioInternoTitularUnidade(Integer idUnidade) {

        if (idUnidade != null) {

            StringBuilder querySB = new StringBuilder();
            querySB.append("select ui.sq_pessoa from usuario_interno ui ")
                    .append("join cargo_usuario_interno  cui on ui.sq_pessoa = cui.sq_pessoa ")
                    .append("join cargo c on c.sq_cargo = cui.sq_cargo ")
                    .append("join cargo_comissao cc on cc.sq_cargo = c.sq_cargo ")
                    .append("join unidade u on u.sq_unidade = cc.sq_unidade ")
                    .append("where cc.st_cargo_comissao_direcao = 'S' ").append("and u.sq_unidade = :idUnidade ");
            Query query = persistenciaUsuarioInterno.getEntityManager().createNativeQuery(querySB.toString());
            query.setParameter("idUnidade", idUnidade);

            return ((Integer) query.getSingleResult()).intValue();

        }

        return null;
    }

    @Override
    public UsuarioExternoVO buscarUsuarioExternoVOPorEmail(String email) {
        return converter(buscarUsuarioExternoPorEmail(email), UsuarioExternoVO.class);
    }

    @Override
    public void buscaPaginadaUsuarioInterno(PaginacaoDTO<UsuarioInternoVO> usuarioInternoDTO) {
        Long total = getQueryTotalBuscaUsuarioInterno(usuarioInternoDTO).getSingleResult();
        @SuppressWarnings("unchecked")
		List<CargoUsuarioInternoView> cargoUsuarioInternoViews = getQueryResultadoBuscaUsuarioInterno(usuarioInternoDTO).getResultList();
        List<UsuarioInternoVO> retorno = new ArrayList<>();
        for (CargoUsuarioInternoView cargoUsuarioInternoView : cargoUsuarioInternoViews) {
            UsuarioInternoVO usuarioInternoVO = converter(cargoUsuarioInternoView.getUsuarioInterno(), UsuarioInternoVO.class);
            usuarioInternoVO.setCargoVO(converter(cargoUsuarioInternoView.getCargo(), CargoVO.class));
            retorno.add(usuarioInternoVO);
        }
        usuarioInternoDTO.setList(retorno);
        usuarioInternoDTO.setTotalResults(total.intValue());
    }

    private Query getQueryResultadoBuscaUsuarioInterno(PaginacaoDTO<UsuarioInternoVO> usuarioInternoDTO) {
        String jpql = getJpqlBuscaUsuarioInterno(usuarioInternoDTO, "cuiw");
        jpql = configurarOrdenacao(jpql, usuarioInternoDTO);
        Query query = persistencia.getEntityManager().createQuery(jpql);
        setFiltosBuscaUsuarioInterno(query, usuarioInternoDTO);
        configurarPaginacao(query, usuarioInternoDTO);
        return query;
    }
    
    private TypedQuery<Long> getQueryTotalBuscaUsuarioInterno(PaginacaoDTO<UsuarioInternoVO> usuarioInternoDTO) {
        String jpql = getJpqlBuscaUsuarioInterno(usuarioInternoDTO, "count(cuiw)");
        TypedQuery<Long> query = persistencia.getEntityManager().createQuery(jpql, Long.class);
        setFiltosBuscaUsuarioInterno(query, usuarioInternoDTO);
        return query;
    }
    
    private String getJpqlBuscaUsuarioInterno(PaginacaoDTO<UsuarioInternoVO> usuarioInternoDTO, String retorno) {
        String idPerfil = Util.toStringNullToNull(getValorNoMapa(usuarioInternoDTO.getFiltros(), Constante.PERFIL_ACESSO_ID));
        Boolean estagiario = (Boolean) getValorNoMapa(usuarioInternoDTO.getFiltros(), "estagiario");
        String nomePessoa = Util.toStringNullToNull(getValorNoMapa(usuarioInternoDTO.getFiltros(), Constante.NOME_PESSOA));
        Boolean filtroAtivo = (Boolean) usuarioInternoDTO.getFiltros().get(Constante.ATIVO);

        StringBuilder jpql = new StringBuilder();

        jpql.append("select ").append(retorno).append(" from CargoUsuarioInternoView cuiw ");

        String filtro = " where ";
        if (!StringUtils.isEmpty(idPerfil)) {
            jpql.append(filtro).append("cuiw.usuarioInterno.usuario.perfil.id = :idPerfil");
            filtro = Constante.AND;
        }
        if (filtroAtivo != null) {
            jpql.append(filtro).append("cuiw.usuarioInterno.usuario.ativo = :ativo");
            filtro = Constante.AND;
        }
        if (!StringUtils.isEmpty(nomePessoa)) {
            jpql.append(filtro).append("upper(cuiw.usuarioInterno.usuario.pessoa.nome) like upper(:nomePessoa)");
            filtro = Constante.AND;
        }
        if (estagiario != null) {
            if (estagiario) {
                jpql.append(filtro).append("upper(cuiw.cargo.tipoCargo.nome) = :nomeTipoCargo");
            } else {
                jpql.append(filtro).append("upper(cuiw.cargo.tipoCargo.nome) != :nomeTipoCargo");
            }
            filtro = Constante.AND;
        }
        return jpql.toString();
    }
    
    private void setFiltosBuscaUsuarioInterno(Query query, PaginacaoDTO<UsuarioInternoVO> usuarioInternoDTO) {
        String idPerfil = Util.toStringNullToNull(getValorNoMapa(usuarioInternoDTO.getFiltros(), Constante.PERFIL_ACESSO_ID));
        Boolean estagiario = (Boolean) getValorNoMapa(usuarioInternoDTO.getFiltros(), "estagiario");
        String nomePessoa = Util.toStringNullToNull(getValorNoMapa(usuarioInternoDTO.getFiltros(), Constante.NOME_PESSOA));
        Boolean filtroAtivo = (Boolean) usuarioInternoDTO.getFiltros().get(Constante.ATIVO);

        if (!StringUtils.isEmpty(idPerfil)) {
            query.setParameter("idPerfil", Integer.parseInt(idPerfil));
        }
        if (filtroAtivo != null) {
            query.setParameter(Constante.ATIVO, filtroAtivo);
        }
        if (!StringUtils.isEmpty(nomePessoa)) {
            query.setParameter(Constante.NOME_PESSOA, "%"+nomePessoa+"%");
        }
        if (estagiario != null) {
            query.setParameter("nomeTipoCargo", TipoCargo.TIPO_CARGO_ESTAGIARIO);
        }
    }

    @Override
    public void buscaPaginadaUsuarioExterno(PaginacaoDTO<UsuarioExternoVO> usuarioExternoDTO) {
        Long total = getQueryTotalBuscaUsuarioExterno(usuarioExternoDTO).getSingleResult();
        @SuppressWarnings("unchecked")
        List<UsuarioExterno> usuarioExternos = getQueryResultadoBuscaUsuarioExterno(usuarioExternoDTO).getResultList();
        List<UsuarioExternoVO> usuarioExternoVOs = converter(usuarioExternos, UsuarioExternoVO.class);
        usuarioExternoDTO.setList(usuarioExternoVOs);
        usuarioExternoDTO.setTotalResults(total.intValue());
    }
    
    private Query getQueryResultadoBuscaUsuarioExterno(PaginacaoDTO<UsuarioExternoVO> usuarioExternoDTO) {
        String jpql = getJpqlBuscaUsuarioExterno(usuarioExternoDTO, "ue");
        jpql = configurarOrdenacao(jpql, usuarioExternoDTO);
        Query query = persistencia.getEntityManager().createQuery(jpql);
        setFiltosBuscaUsuarioExterno(query, usuarioExternoDTO);
        configurarPaginacao(query, usuarioExternoDTO);
        return query;
    }
    
    private TypedQuery<Long> getQueryTotalBuscaUsuarioExterno(PaginacaoDTO<UsuarioExternoVO> usuarioExternoDTO) {
        String jpql = getJpqlBuscaUsuarioExterno(usuarioExternoDTO, "count(ue)");
        TypedQuery<Long> query = persistencia.getEntityManager().createQuery(jpql, Long.class);
        setFiltosBuscaUsuarioExterno(query, usuarioExternoDTO);
        return query;
    }
    
    private String getJpqlBuscaUsuarioExterno(PaginacaoDTO<UsuarioExternoVO> usuarioExternoDTO, String retorno) {
        String idPerfil = getValorNoMapa(usuarioExternoDTO.getFiltros(), Constante.PERFIL_ACESSO_ID).toString();
//        Boolean filtroAtivo = (Boolean) usuarioExternoDTO.getFiltros().get(Constante.ATIVO);
        String nomePessoa = (String) usuarioExternoDTO.getFiltros().get(Constante.NOME_PESSOA);
        String emailPessoa = (String) usuarioExternoDTO.getFiltros().get(Constante.EMAIL_PESSOA);
        
        
        StringBuilder jpql = new StringBuilder();
        
        jpql.append("select ").append(retorno).append(" from UsuarioExterno ue");
        
        String filtro = " where ";
        if (!StringUtils.isEmpty(idPerfil)) {
            jpql.append(filtro).append("ue.usuario.perfil.id = :idPerfil");
            filtro = Constante.AND;
        }
        
        /* Comentado para atender a RN27 que permite trazer usuários ativos e inativos
        if (filtroAtivo != null) {
            jpql.append(filtro).append("ue.usuario.ativo = :ativo");
            filtro = Constante.AND;
        }*/
        if (!StringUtils.isEmpty(nomePessoa)) {
        	//Comentado por que sei que essa regra bullshit não vai durar muito
            //jpql.append(filtro).append("ue.usuario.pessoa.nome like :nomePessoa");
            jpql.append(filtro).append("ue.usuario.pessoa.nome = :nomePessoa");
            filtro = Constante.AND;
        }
        if (!StringUtils.isEmpty(emailPessoa)) {
        	//Comentado por que sei que essa regra bullshit não vai durar muito
            //jpql.append(filtro).append("ue.usuario.pessoa.email like :emailPessoa");
            jpql.append(filtro).append("ue.usuario.pessoa.email = :emailPessoa");
            filtro = Constante.AND;
        }
        return jpql.toString();
    }
    
    private void setFiltosBuscaUsuarioExterno(Query query, PaginacaoDTO<UsuarioExternoVO> usuarioExternoDTO) {
        String idPerfil = getValorNoMapa(usuarioExternoDTO.getFiltros(), Constante.PERFIL_ACESSO_ID).toString();
//        Boolean filtroAtivo = (Boolean) usuarioExternoDTO.getFiltros().get(Constante.ATIVO);
        String nomePessoa = (String) usuarioExternoDTO.getFiltros().get(Constante.NOME_PESSOA);
        String emailPessoa = (String) usuarioExternoDTO.getFiltros().get(Constante.EMAIL_PESSOA);

        if (!StringUtils.isEmpty(idPerfil)) {
            query.setParameter("idPerfil", Integer.parseInt(idPerfil));
        }
        /* Comentado para atender a RN27 que permite trazer usuários ativos e inativos
        if (filtroAtivo != null) {
            query.setParameter(Constante.ATIVO, filtroAtivo);
        }*/
        if (!StringUtils.isEmpty(nomePessoa)) {
            //query.setParameter(Constante.NOME_PESSOA, "%" + nomePessoa + "%");
            query.setParameter(Constante.NOME_PESSOA, nomePessoa);
        }
        if (!StringUtils.isEmpty(emailPessoa)) {
            //query.setParameter(Constante.EMAIL_PESSOA, "%" + emailPessoa + "%");
            query.setParameter(Constante.EMAIL_PESSOA, emailPessoa);
        }
    }
    
    @Override
    public Usuario buscarUsuarioPorId(Integer id){
        String jpql = "select usu from Usuario usu where usu.id = :id";
        Query query = persistencia.getEntityManager().createQuery(jpql);
        query.setParameter("id", id);
        return (Usuario) query.getSingleResult();
    }

    @Override
    public SolicitacaoRedefinicaoSenha buscarUltimaSolicitacaoRedefinicaoSenha(UsuarioExterno usuarioExterno) {
        String jpql = "select srs from SolicitacaoRedefinicaoSenha srs where srs.usuarioExterno.id = :idUsuarioExterno order by srs.dataSolicitacao desc";
        TypedQuery<SolicitacaoRedefinicaoSenha> query = persistencia.getEntityManager().createQuery(jpql, SolicitacaoRedefinicaoSenha.class);
        query.setParameter(Constante.ID_USUARIO_EXTERNO, usuarioExterno.getId());
        List<SolicitacaoRedefinicaoSenha> resultado = query.getResultList();
        if (!resultado.isEmpty()) {
            return resultado.get(0);
        }
        return null;
    }
    
    @Override
    public UsuarioExternoVO buscarUsuarioExternoParaRedefinicaoSenha(String codigo) {
        String email = codigo;
        String mensagem = "MSG113";
        UsuarioExterno usuarioExterno = buscarUsuarioExternoPorEmail(email);
        if (usuarioExterno == null) {
            NegocioException.throwExceptionErro(mensagem);
        }
        SolicitacaoRedefinicaoSenha solicitacaoRedefinicaoSenha = buscarUltimaSolicitacaoRedefinicaoSenha(usuarioExterno);
        if (solicitacaoRedefinicaoSenha.getUtilizada()) {
            NegocioException.throwExceptionErro(mensagem);
        }
        validarDataParaRedefinicaoExpirado(solicitacaoRedefinicaoSenha);
        
        return converter(usuarioExterno, UsuarioExternoVO.class);
    }
    
    private void validarDataParaRedefinicaoExpirado(SolicitacaoRedefinicaoSenha solicitacaoRedefinicaoSenha) {
        DateTime dataSolicitacao = new DateTime(solicitacaoRedefinicaoSenha.getDataSolicitacao());
        DateTime dataLimite  = dataSolicitacao.plusDays(1);
        if (dataLimite.isBeforeNow()) {
            NegocioException.throwExceptionErro("MSG113");
        }
    }

    @Override
    public List<SenhaUsuarioExterno> buscarUltimasTresSenhasDoUsuarioExterno(UsuarioExterno usuarioExterno) {
        String jpql = "select sue from SenhaUsuarioExterno sue where sue.usuarioExterno.id = :idUsuarioExterno order by sue.dataCriacao desc";
        TypedQuery<SenhaUsuarioExterno> query = persistencia.getEntityManager().createQuery(jpql, SenhaUsuarioExterno.class);
        query.setParameter(Constante.ID_USUARIO_EXTERNO, usuarioExterno.getId());
        query.setFirstResult(0);
        query.setMaxResults(MAX_RESULTADO);
        return query.getResultList();
    }

    @Override
    public SenhaUsuarioExterno buscarUltimaSenhaUsuarioExterno(Usuario usuario) {
        String jpql = "select sue from SenhaUsuarioExterno sue where sue.usuarioExterno.id = :idUsuarioExterno order by sue.dataCriacao desc";
        TypedQuery<SenhaUsuarioExterno> query = persistencia.getEntityManager().createQuery(jpql, SenhaUsuarioExterno.class);
        query.setParameter(Constante.ID_USUARIO_EXTERNO, usuario.getId());
        query.setFirstResult(0);
        query.setMaxResults(1);
        return query.getSingleResult();
    }
}