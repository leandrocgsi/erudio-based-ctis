package br.com.erudio.negocio.leitura;

import java.util.List;

import javax.ejb.Local;

import br.com.erudio.negocio.vo.TipoRelacionamentoVO;
import br.com.erudio.negocio.vo.TipoRespostaManifestacaoVO;


@Local
public interface ManifestacaoNegocioLeitura{

    List<TipoRelacionamentoVO> recuperarTiposRelacionamentos();

    List<TipoRespostaManifestacaoVO> recuperarTiposRespostas();

}
