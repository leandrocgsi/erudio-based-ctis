package br.com.erudio.negocio.escrita;

import java.util.List;

import br.com.erudio.negocio.vo.UnidadeVO;

public interface UnidadeNegocioEscrita {

    void atualizaStatus(List<UnidadeVO> unidades);

    UnidadeVO alterar(UnidadeVO unidadeAlteracao);
}
