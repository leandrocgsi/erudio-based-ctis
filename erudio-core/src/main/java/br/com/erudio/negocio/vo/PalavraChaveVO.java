package br.com.erudio.negocio.vo;

import br.com.erudio.negocio.vo.base.BaseVO;

public class PalavraChaveVO extends BaseVO {
    private static final long serialVersionUID = 1L;
    private Integer id;
    private String nome;
    private Boolean ativo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }

}
