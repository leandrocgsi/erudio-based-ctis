package br.com.erudio.negocio.enums;

public enum FuncionalidadeEnum {
    
    EFETUAR_LOGIN("Efetuar Login"),
    GERENCIAR_ORGAO_UNIDADE("Gerenciar Órgão/Unidade"),
    GERENCIAR_PERFIL("Gerenciar Perfil"),
    ASSOCIAR_FUNCIONALIDADE_PERFIL("Associar Funcionalidade ao Perfil"),
    ASSOCIAR_USUARIO_PERFIL("Associar Usuário ao Perfil");
    
    private String descricao;
    
    FuncionalidadeEnum(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }
    
}
