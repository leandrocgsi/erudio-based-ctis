package br.com.erudio.negocio.leitura;

import java.util.List;

import javax.ejb.Local;

import br.com.erudio.negocio.vo.PronomeTratamentoVO;

@Local
public interface PronomeTratamentoNegocioLeitura {

    List<PronomeTratamentoVO> buscarTodos();
    
}