package br.com.erudio.negocio.leitura.impl;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import br.com.erudio.modelo.Funcionalidade;
import br.com.erudio.negocio.NegocioBaseErudio;
import br.com.erudio.negocio.leitura.FuncionalidadeNegocioLeitura;
import br.com.erudio.persistencia.PersistenciaErudio;

@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class FuncionalidadeNegocioLeituraImpl extends NegocioBaseErudio implements FuncionalidadeNegocioLeitura {

    @EJB
    private PersistenciaErudio<Integer, Funcionalidade> persistenciaFuncionalidade;
    
    @SuppressWarnings("unchecked")
    @Override
    public List<Funcionalidade> listarFuncionalidadesAtivas() {
        StringBuilder sb = new StringBuilder("select distinct funcionalidade");
        sb.append(" from Funcionalidade as funcionalidade")
            .append(" join fetch funcionalidade.tiposAcesso")
            .append(" where funcionalidade.ativa = 'S'");
        
        return (List<Funcionalidade>) persistenciaFuncionalidade.getEntityManager()
                .createQuery(sb.toString())
                .getResultList();
    }

}
