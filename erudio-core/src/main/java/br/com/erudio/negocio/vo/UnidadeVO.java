package br.com.erudio.negocio.vo;

import java.util.List;

import br.com.erudio.negocio.vo.base.BaseVO;

public class UnidadeVO extends BaseVO {

    private static final long serialVersionUID = 1L;

    private Integer id;

    private UnidadeVO unidadePai;

    private String sigla;

    private String nome;

    private String observacao;

    private Boolean trataManifestacao;

    private Boolean ativo;
    
    private List<EmailUnidadeVO> emailUnidadeVOs;
    
    private List<UnidadeVO> unidadeFilhaVOs;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public UnidadeVO getUnidadePai() {
        return unidadePai;
    }

    public void setUnidadePai(UnidadeVO unidadePai) {
        this.unidadePai = unidadePai;
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public Boolean getTrataManifestacao() {
        return trataManifestacao;
    }

    public void setTrataManifestacao(Boolean trataManifestacao) {
        this.trataManifestacao = trataManifestacao;
    }

    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }

    public List<EmailUnidadeVO> getEmailUnidadeVOs() {
        return emailUnidadeVOs;
    }
    
    public void setEmailUnidadeVOs(List<EmailUnidadeVO> emailUnidadeVOs) {
        this.emailUnidadeVOs = emailUnidadeVOs;
    }

    public List<UnidadeVO> getUnidadeFilhaVOs() {
        return unidadeFilhaVOs;
    }

    public void setUnidadeFilhaVOs(List<UnidadeVO> unidadeFilhaVOs) {
        this.unidadeFilhaVOs = unidadeFilhaVOs;
    }
    
    

}
