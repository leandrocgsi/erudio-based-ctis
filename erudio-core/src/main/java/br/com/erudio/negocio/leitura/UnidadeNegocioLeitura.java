package br.com.erudio.negocio.leitura;

import java.util.List;

import br.com.erudio.modelo.EmailUnidade;
import br.com.erudio.negocio.vo.EmailUnidadeVO;
import br.com.erudio.negocio.vo.UnidadeVO;

public interface UnidadeNegocioLeitura {

    List<UnidadeVO> buscarTodas();

    List<EmailUnidadeVO> buscarTodosEmailUnidadeVOPorIdUnidade(Integer idUnidade);

    List<EmailUnidade> buscarTodosEmailUnidadePorIdUnidade(Integer idUnidade);

    List<UnidadeVO> buscarEstruturaOrganizacional();
}
