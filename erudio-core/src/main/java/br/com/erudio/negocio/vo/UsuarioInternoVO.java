package br.com.erudio.negocio.vo;

import br.com.erudio.negocio.vo.base.BaseVO;

public class UsuarioInternoVO extends BaseVO {

    private static final long serialVersionUID = 1L;

    private Long id;

    private UsuarioVO usuario;

    private UnidadeVO unidade;
    
    private CargoVO cargoVO;

    private String loginRede;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UsuarioVO getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioVO usuario) {
        this.usuario = usuario;
    }

    public UnidadeVO getUnidade() {
        return unidade;
    }

    public void setUnidade(UnidadeVO unidade) {
        this.unidade = unidade;
    }

    public String getLoginRede() {
        return loginRede;
    }
 
    public void setLoginRede(String loginRede) {
        this.loginRede = loginRede;
    }

    public CargoVO getCargoVO() {
        return cargoVO;
    }

    public void setCargoVO(CargoVO cargoVO) {
        this.cargoVO = cargoVO;
    }
    
}
