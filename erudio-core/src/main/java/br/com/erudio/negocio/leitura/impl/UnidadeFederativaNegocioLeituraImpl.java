package br.com.erudio.negocio.leitura.impl;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import br.com.erudio.modelo.UnidadeFederativa;
import br.com.erudio.negocio.NegocioBaseErudio;
import br.com.erudio.negocio.leitura.UnidadeFederativaNegocioLeitura;
import br.com.erudio.negocio.vo.UnidadeFederativaVO;
import br.com.erudio.persistencia.PersistenciaErudio;

@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class UnidadeFederativaNegocioLeituraImpl extends NegocioBaseErudio implements UnidadeFederativaNegocioLeitura {

    @EJB
    private PersistenciaErudio<String, UnidadeFederativa> persistencia;

    @Override
    public List<UnidadeFederativaVO> buscarTodos() {
        List<UnidadeFederativa> ufs = persistencia.obterTodos(UnidadeFederativa.class);
        return converter(ufs, UnidadeFederativaVO.class);
    }

}
