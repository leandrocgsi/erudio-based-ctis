package br.com.erudio.negocio.escrita;

import br.com.erudio.negocio.vo.CategoriaManifestacaoVO;

public interface CategoriaManifestacaoNegocioEscrita {
    CategoriaManifestacaoVO incluir(CategoriaManifestacaoVO categoriaManifestacao);

    CategoriaManifestacaoVO alterar(CategoriaManifestacaoVO categoriaManifestacao);

    void excluir(Integer idCategoriaManifestacao);
    

}
