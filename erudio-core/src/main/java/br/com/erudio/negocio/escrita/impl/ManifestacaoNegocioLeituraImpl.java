package br.com.erudio.negocio.escrita.impl;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import br.com.erudio.modelo.TipoRelacionamento;
import br.com.erudio.modelo.TipoRespostaManifestacao;
import br.com.erudio.negocio.NegocioBaseErudio;
import br.com.erudio.negocio.leitura.ManifestacaoNegocioLeitura;
import br.com.erudio.negocio.vo.TipoRelacionamentoVO;
import br.com.erudio.negocio.vo.TipoRespostaManifestacaoVO;
import br.com.erudio.persistencia.PersistenciaErudio;

@Stateless
public class ManifestacaoNegocioLeituraImpl extends NegocioBaseErudio implements ManifestacaoNegocioLeitura {

    @EJB
    private PersistenciaErudio<Integer, TipoRelacionamento> relacionentoPersistencia;
    
    @EJB
    private PersistenciaErudio<Integer, TipoRespostaManifestacao> respostaPersistencia;

    @Override
    public List<TipoRelacionamentoVO> recuperarTiposRelacionamentos() {
        List<TipoRelacionamento> lista = relacionentoPersistencia.obterTodos(TipoRelacionamento.class);
        return converter(lista, TipoRelacionamentoVO.class);
    }

    @Override
    public List<TipoRespostaManifestacaoVO> recuperarTiposRespostas() {
        List<TipoRespostaManifestacao> lista = respostaPersistencia.obterTodos(TipoRespostaManifestacao.class);
        return converter(lista, TipoRespostaManifestacaoVO.class);
    }

}
