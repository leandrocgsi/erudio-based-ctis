package br.com.erudio.negocio.escrita.impl;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import br.com.erudio.exception.NegocioException;
import br.com.erudio.modelo.EmailUnidade;
import br.com.erudio.modelo.Unidade;
import br.com.erudio.negocio.NegocioBaseErudio;
import br.com.erudio.negocio.escrita.UnidadeNegocioEscrita;
import br.com.erudio.negocio.leitura.UnidadeNegocioLeitura;
import br.com.erudio.negocio.vo.UnidadeVO;
import br.com.erudio.persistencia.PersistenciaErudio;

@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class UnidadeNegocioEscritaImpl extends NegocioBaseErudio implements UnidadeNegocioEscrita {

    @EJB
    private PersistenciaErudio<Integer, Unidade> persistenciaUnidade;

    @EJB
    private PersistenciaErudio<Integer, EmailUnidade> persistenciaEmail;

    @EJB
    private UnidadeNegocioLeitura unidadeLeitura;

    @Override
    public void atualizaStatus(List<UnidadeVO> unidades) {

        if (!unidades.isEmpty() && unidades != null) {
            for (UnidadeVO unidadeVO : unidades) {
                Unidade unidade = converter(unidadeVO, Unidade.class);
                persistenciaUnidade.gravar(unidade);
            }
        }
    }

    @Override
    public UnidadeVO alterar(UnidadeVO unidadeVO) {
        if (unidadeVO.getId() == null) {
            NegocioException.throwExceptionErro("Unidade sem ID.");
        }

        Unidade unidade = converter(unidadeVO, Unidade.class);
        unidade = persistenciaUnidade.gravar(unidade);
        excluirEmailsUnidadePorIdUnidade(unidade.getId());
        if (!CollectionUtils.isEmpty(unidadeVO.getEmailUnidadeVOs())) {
            List<EmailUnidade> emailUnidades = converter(unidadeVO.getEmailUnidadeVOs(), EmailUnidade.class);
            incluirEmailsUnidade(emailUnidades, unidade);
        }

        UnidadeVO retorno = converter(unidade, UnidadeVO.class);
        retorno.setEmailUnidadeVOs(unidadeLeitura.buscarTodosEmailUnidadeVOPorIdUnidade(unidade.getId()));
        return retorno;
    }

    private void incluirEmailsUnidade(List<EmailUnidade> emailUnidades, Unidade unidade) {

        for (EmailUnidade emailUnidade : emailUnidades) {
            emailUnidade.setUnidade(unidade);
            persistenciaEmail.gravar(emailUnidade);
        }

    }

    private void excluirEmailsUnidadePorIdUnidade(Integer idUnidade) {
        List<EmailUnidade> unidade = unidadeLeitura.buscarTodosEmailUnidadePorIdUnidade(idUnidade);

        if (!unidade.isEmpty()) {

            List<EmailUnidade> emailUnidades = converter(unidade, EmailUnidade.class);

            for (EmailUnidade emailUnidade : emailUnidades) {

                emailUnidade = persistenciaEmail.getEntityManager().merge(emailUnidade);
                persistenciaEmail.getEntityManager().remove(emailUnidade);

            }
        }
    }

}
