package br.com.erudio.negocio.leitura;

import java.util.List;

import javax.ejb.Local;

import br.com.erudio.dto.PaginacaoDTO;
import br.com.erudio.modelo.SenhaUsuarioExterno;
import br.com.erudio.modelo.SolicitacaoRedefinicaoSenha;
import br.com.erudio.modelo.Usuario;
import br.com.erudio.modelo.UsuarioExterno;
import br.com.erudio.modelo.UsuarioInterno;
import br.com.erudio.negocio.vo.UsuarioExternoVO;
import br.com.erudio.negocio.vo.UsuarioInternoVO;
import br.com.erudio.negocio.vo.UsuarioVO;

@Local
public interface UsuarioNegocioLeitura {

    UsuarioVO detalharUsuario(Integer id);

    Usuario buscarUsuarioPorId(Integer idUsuario);

    UsuarioInternoVO buscarUsuarioInternoVOTitularUnidade(Integer idUnidade);

    UsuarioExterno buscarUsuarioExternoPorEmail(String email);

    UsuarioExternoVO buscarUsuarioExternoVOPorEmail(String email);

    UsuarioInterno buscarUsuarioInternoPorLoginRede(String loginRede);

    void buscaPaginadaUsuarioInterno(PaginacaoDTO<UsuarioInternoVO> usuarioInternoDTO);

    void buscaPaginadaUsuarioExterno(PaginacaoDTO<UsuarioExternoVO> usuarioExternoDTO);

    SolicitacaoRedefinicaoSenha buscarUltimaSolicitacaoRedefinicaoSenha(UsuarioExterno usuarioExterno);

    UsuarioExternoVO buscarUsuarioExternoParaRedefinicaoSenha(String codigo);

    List<SenhaUsuarioExterno> buscarUltimasTresSenhasDoUsuarioExterno(UsuarioExterno usuarioExterno);

    SenhaUsuarioExterno buscarUltimaSenhaUsuarioExterno(Usuario usuario);
}