package br.com.erudio.negocio.leitura.impl;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import br.com.erudio.modelo.GrauInstrucao;
import br.com.erudio.negocio.NegocioBaseErudio;
import br.com.erudio.negocio.leitura.GrauInstrucaoNegocioLeitura;
import br.com.erudio.negocio.vo.GrauInstrucaoVO;
import br.com.erudio.persistencia.PersistenciaErudio;

@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class GrauInstrucaoNegocioLeituraImpl extends NegocioBaseErudio implements GrauInstrucaoNegocioLeitura {

    @EJB
    private PersistenciaErudio<Integer, GrauInstrucao> persistencia;

    @Override
    public List<GrauInstrucaoVO> buscarTodos() {
        List<GrauInstrucao> grauInstrucaos = persistencia.obterTodos(GrauInstrucao.class);
        return converter(grauInstrucaos, GrauInstrucaoVO.class);
    }

}
