package br.com.erudio.negocio.leitura.impl;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import br.com.erudio.modelo.TipoUsuario;
import br.com.erudio.negocio.NegocioBaseErudio;
import br.com.erudio.negocio.leitura.TipoUsuarioNegocioLeitura;
import br.com.erudio.persistencia.PersistenciaErudio;

@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class TipoUsuarioNegocioLeituraImpl extends NegocioBaseErudio implements TipoUsuarioNegocioLeitura {

    @EJB
    private PersistenciaErudio<Integer, TipoUsuario> persistencia;
 

    
    @Override
    public TipoUsuario buscarTipoUsuarioPorNome(String nome) {
        CriteriaBuilder cb = persistencia.getEntityManager().getCriteriaBuilder();
        CriteriaQuery<TipoUsuario> criteriaQuery = cb.createQuery(TipoUsuario.class);
        Root<TipoUsuario> from = criteriaQuery.from(TipoUsuario.class);
        criteriaQuery.select(from).where(cb.equal(from.get("nome"), nome));
        return persistencia.getEntityManager().createQuery(criteriaQuery).getSingleResult();
    }
}