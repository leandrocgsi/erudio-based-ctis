package br.com.erudio.negocio.vo;

import br.com.erudio.negocio.vo.base.BaseVO;

public class PessoaVO extends BaseVO {

    private static final long serialVersionUID = 1L;
    
    private Integer id;
    private String nome;
    private String email;
    private String nomePessoaReceita;
    private String cnpj;
    private String cpf;
    private Boolean usuario;
    private PaisVO pais;
    private TipoPessoaVO tipoPessoa;
    private UnidadeFederativaVO uf;
    private GrauInstrucaoVO grauInstrucao;
    private PronomeTratamentoVO pronomeTratamento;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNomePessoaReceita() {
        return nomePessoaReceita;
    }

    public void setNomePessoaReceita(String nomePessoaReceita) {
        this.nomePessoaReceita = nomePessoaReceita;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Boolean getUsuario() {
        return usuario;
    }

    public void setUsuario(Boolean usuario) {
        this.usuario = usuario;
    }

    public PaisVO getPais() {
        return pais;
    }

    public void setPais(PaisVO pais) {
        this.pais = pais;
    }

    public TipoPessoaVO getTipoPessoa() {
        return tipoPessoa;
    }

    public void setTipoPessoa(TipoPessoaVO tipoPessoa) {
        this.tipoPessoa = tipoPessoa;
    }

    public UnidadeFederativaVO getUf() {
        return uf;
    }

    public void setUf(UnidadeFederativaVO uf) {
        this.uf = uf;
    }

    public GrauInstrucaoVO getGrauInstrucao() {
        return grauInstrucao;
    }

    public void setGrauInstrucao(GrauInstrucaoVO grauInstrucao) {
        this.grauInstrucao = grauInstrucao;
    }

    public PronomeTratamentoVO getPronomeTratamento() {
        return pronomeTratamento;
    }

    public void setPronomeTratamento(PronomeTratamentoVO pronomeTratamento) {
        this.pronomeTratamento = pronomeTratamento;
    }
}