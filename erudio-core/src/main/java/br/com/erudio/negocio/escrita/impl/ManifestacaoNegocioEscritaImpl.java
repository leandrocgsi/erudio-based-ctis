package br.com.erudio.negocio.escrita.impl;

import static org.apache.commons.lang.StringUtils.isBlank;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import br.com.erudio.modelo.Manifestacao;
import br.com.erudio.modelo.TipoCanalEntrada;
import br.com.erudio.modelo.TipoRelacionamento;
import br.com.erudio.modelo.TipoSituacaoManifestacao;
import br.com.erudio.modelo.UnidadeFederativa;
import br.com.erudio.negocio.NegocioBaseErudio;
import br.com.erudio.negocio.escrita.ManifestacaoNegocioEscrita;
import br.com.erudio.negocio.vo.ManifestacaoVO;
import br.com.erudio.persistencia.PersistenciaErudio;

@Stateless
public class ManifestacaoNegocioEscritaImpl extends NegocioBaseErudio implements ManifestacaoNegocioEscrita {

    @EJB
    private PersistenciaErudio<Integer, Manifestacao> manifestacaoPersistencia;

    @Override
    public ManifestacaoVO cadastrarManifestacao(ManifestacaoVO manifestacaoVO) {
        Manifestacao manifestacao = constroiManifestacao(manifestacaoVO);
        validate(manifestacao);
        Manifestacao novaManifestacao = manifestacaoPersistencia.getEntityManager().merge(manifestacao);
        return converter(novaManifestacao, ManifestacaoVO.class);
    }

    private Manifestacao constroiManifestacao(ManifestacaoVO manifestacaoVO) {
        Manifestacao manifestacao = converter(manifestacaoVO, Manifestacao.class);
        constroiSistemaOrigem(manifestacao);
        constroiCanalEntrada(manifestacao);
        constroiUF(manifestacao);
        constroiTipoSituacao(manifestacao);
        return manifestacao;
    }

    private void constroiSistemaOrigem(Manifestacao manifestacao) {
        if (isBlank(manifestacao.getSistemaOrigem())) {
            manifestacao.setSistemaOrigem("ERUDIO");
        }
    }

    private void constroiCanalEntrada(Manifestacao manifestacao) {
        TipoCanalEntrada canal = new TipoCanalEntrada();
        canal.setId(TipoCanalEntrada.ID_FORMALARIO_ELETRONICO);
        manifestacao.setTipoCanalEntrada(canal);
    }

    private void constroiUF(Manifestacao manifestacao) {
        if (TipoRelacionamento.ID_SERVIDOR_DO_ERUDIO.equals(manifestacao.getTipoRelacionamento().getId())) {
            UnidadeFederativa uf = new UnidadeFederativa(UnidadeFederativa.DF);
            manifestacao.setUf(uf);
        }
    }
    
    private void constroiTipoSituacao(Manifestacao manifestacao){
        TipoSituacaoManifestacao tipoSituacaoManifestacao = new TipoSituacaoManifestacao();
        tipoSituacaoManifestacao.setId(TipoSituacaoManifestacao.ID_RECEBIDA);
        manifestacao.setTipoSituacaoManifestacao(tipoSituacaoManifestacao);
    }

}
