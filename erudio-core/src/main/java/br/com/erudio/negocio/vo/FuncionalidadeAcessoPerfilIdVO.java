package br.com.erudio.negocio.vo;

public class FuncionalidadeAcessoPerfilIdVO {
	 	
		private Integer idFuncionalidade;

	    private Integer idTipoAcesso;

	    private Integer idPerfilAcesso;

		public Integer getIdFuncionalidade() {
			return idFuncionalidade;
		}

		public void setIdFuncionalidade(Integer idFuncionalidade) {
			this.idFuncionalidade = idFuncionalidade;
		}

		public Integer getIdTipoAcesso() {
			return idTipoAcesso;
		}

		public void setIdTipoAcesso(Integer idTipoAcesso) {
			this.idTipoAcesso = idTipoAcesso;
		}

		public Integer getIdPerfilAcesso() {
			return idPerfilAcesso;
		}

		public void setIdPerfilAcesso(Integer idPerfilAcesso) {
			this.idPerfilAcesso = idPerfilAcesso;
		}
}	
