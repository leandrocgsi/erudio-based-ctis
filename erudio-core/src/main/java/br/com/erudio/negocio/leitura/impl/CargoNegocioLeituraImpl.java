package br.com.erudio.negocio.leitura.impl;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import br.com.erudio.modelo.Cargo;
import br.com.erudio.negocio.NegocioBaseErudio;
import br.com.erudio.negocio.leitura.CargoNegocioLeitura;
import br.com.erudio.negocio.vo.CargoVO;
import br.com.erudio.persistencia.PersistenciaErudio;

@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class CargoNegocioLeituraImpl extends NegocioBaseErudio
        implements CargoNegocioLeitura {

    @EJB
    private PersistenciaErudio<Integer, Cargo> persistencia;

    @Override
    public CargoVO recuperarCargoVOPorIdUsuarioInterno(Integer idUsuarioInterno) {
        Cargo cargo = null;
        cargo = recuperarCargoComissaoPorIdUsuarioInterno(idUsuarioInterno);
        if (cargo == null) {
            cargo = recuperarCargoNaoComissaoPorIdUsuarioInterno(idUsuarioInterno);
        }
        return converter(cargo, CargoVO.class);
    }
    
    private Cargo recuperarCargoComissaoPorIdUsuarioInterno(Integer idUsuarioInterno) {
        return persistencia
                .getEntityManager()
                .createNamedQuery("select cui.cargo from CargoUsuarioInterno cui where cui.cargo.cargoComissao != null and cui.usuarioInterno.id = :idUsuarioInterno",Cargo.class)
                .setParameter("idUsuarioInterno", idUsuarioInterno)
                .getSingleResult();
    }
    
    private Cargo recuperarCargoNaoComissaoPorIdUsuarioInterno(Integer idUsuarioInterno) {
        return persistencia
                .getEntityManager()
                .createNamedQuery("select cui.cargo from CargoUsuarioInterno cui where cui.cargo.cargoComissao == null and cui.usuarioInterno.id = :idUsuarioInterno",Cargo.class)
                .setParameter("idUsuarioInterno", idUsuarioInterno)
                .getSingleResult();
    }
}
