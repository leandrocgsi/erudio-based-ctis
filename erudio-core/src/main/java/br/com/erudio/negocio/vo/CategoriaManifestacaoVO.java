package br.com.erudio.negocio.vo;

import br.com.erudio.negocio.vo.base.BaseVO;

public class CategoriaManifestacaoVO extends BaseVO {
    private static final long serialVersionUID = 1L;

    private Integer id;
    private Boolean ativo;
    private String nome;
    private String descricao;
    private String descricaoCombo;

    public CategoriaManifestacaoVO() {
    }

    public CategoriaManifestacaoVO(Integer id, String nome, String descricao) {
        this.id = id;
        this.nome = nome;
        this.descricao = descricao;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getNomeCombo() {
        return descricao;
    }

    public String getDescricaoCombo() {
        return descricaoCombo;
    }

    public void setDescricaoCombo(String descricaoCombo) {
        this.descricaoCombo = descricaoCombo;
    }

}
