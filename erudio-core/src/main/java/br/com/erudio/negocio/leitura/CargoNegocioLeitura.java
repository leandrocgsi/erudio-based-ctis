package br.com.erudio.negocio.leitura;

import br.com.erudio.negocio.vo.CargoVO;

public interface CargoNegocioLeitura {

    CargoVO recuperarCargoVOPorIdUsuarioInterno(Integer idUsuarioInterno);
}
