package br.com.erudio.negocio;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;

import org.apache.commons.lang3.StringUtils;

import br.com.erudio.business.NegocioBase;
import br.com.erudio.dto.PaginacaoDTO;

public class NegocioBaseErudio extends NegocioBase {

    protected <S, T> List<T> converter(List<S> from, Class<T> voType) {
        List<T> result = new ArrayList<>();
        if (from == null) {
            return result;
        }
        for (S s : from) {
            T t = converter(s, voType);
            result.add(t);
        }
        return result;
    }
    
    protected void configurarPaginacao(Query query, PaginacaoDTO paginacaoDTO) {
        if (paginacaoDTO.getCurrentPage() != null) {
            query.setFirstResult((paginacaoDTO.getCurrentPage() - 1) * paginacaoDTO.getPageSize());
        } else {
            paginacaoDTO.setCurrentPage(0);
        }
        if (paginacaoDTO.getPageSize() != null) {
            query.setMaxResults(paginacaoDTO.getPageSize());
        } else {
            paginacaoDTO.setPageSize(0);
        }
    }
    
    protected void configurarOrdenacao(StringBuilder jpql, String alias, PaginacaoDTO paginacaoDTO) {
        jpql.append(" order by ")
        .append(alias).append(".")
        .append(paginacaoDTO.getSortFields())
        .append(" ")
        .append(paginacaoDTO.getSortDirections());
    }
    
    protected String configurarOrdenacao(String jpql, String alias, PaginacaoDTO paginacaoDTO) {
        StringBuilder sbJpql = new StringBuilder(jpql);
        configurarOrdenacao(sbJpql, alias, paginacaoDTO);
        return sbJpql.toString();
    }
    
    protected void configurarOrdenacao(StringBuilder jpql, PaginacaoDTO paginacaoDTO) {
        jpql.append(" order by ")
        .append(paginacaoDTO.getSortFields())
        .append(" ")
        .append(paginacaoDTO.getSortDirections());
    }
    
    protected String configurarOrdenacao(String jpql, PaginacaoDTO paginacaoDTO) {
        StringBuilder sbJpql = new StringBuilder(jpql);
        configurarOrdenacao(sbJpql, paginacaoDTO);
        return sbJpql.toString();
    }
    
    protected Object getValorNoMapa(Map<String, Object> mapa, String propriedade) {
        if (mapa == null) {
            return null;
        }
        
        String[] propriedades = propriedade.split("\\.");
        
        if (propriedades.length == 1) {
            return mapa.get(propriedades[0]);
        } else {
            Map<String, Object> subMapa = (Map<String, Object>) mapa.get(propriedades[0]);
            String subPropriedade = StringUtils.join(Arrays.copyOfRange(propriedades, 1, propriedades.length), ".");
            return getValorNoMapa(subMapa, subPropriedade);
        }
    }
    
    protected <T> List<T> tratarResultado(List<T> list) {
        List<T> retorno = new ArrayList<>();
        for (Object object : list) {
            retorno.add((T) ((Object[]) object)[0]);
        }
        return retorno;
    }
}
