package br.com.erudio.negocio.escrita.impl;

import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;

import br.com.erudio.exception.NegocioException;
import br.com.erudio.modelo.Pessoa;
import br.com.erudio.modelo.SenhaUsuarioExterno;
import br.com.erudio.modelo.SolicitacaoRedefinicaoSenha;
import br.com.erudio.modelo.Usuario;
import br.com.erudio.modelo.UsuarioExterno;
import br.com.erudio.modelo.UsuarioInterno;
import br.com.erudio.negocio.NegocioBaseErudio;
import br.com.erudio.negocio.enums.EmailEnum;
import br.com.erudio.negocio.enums.ParametroEnum;
import br.com.erudio.negocio.enums.PerfilAcessoEnum;
import br.com.erudio.negocio.enums.TipoUsuarioEnum;
import br.com.erudio.negocio.escrita.UsuarioNegocioEscrita;
import br.com.erudio.negocio.leitura.ParametroSistemaNegocioLeitura;
import br.com.erudio.negocio.leitura.PerfilAcessoNegocioLeitura;
import br.com.erudio.negocio.leitura.TipoUsuarioNegocioLeitura;
import br.com.erudio.negocio.leitura.UsuarioNegocioLeitura;
import br.com.erudio.negocio.vo.UsuarioExternoVO;
import br.com.erudio.negocio.vo.UsuarioInternoVO;
import br.com.erudio.persistencia.PersistenciaErudio;
import br.com.erudio.util.Util;
import br.com.erudio.util.email.EmailUtil;
import br.com.erudio.util.email.EmailVO;
import br.com.erudio.util.email.EmailVOBuilder;

@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class UsuarioNegocioEscritaImpl extends NegocioBaseErudio implements UsuarioNegocioEscrita {
    
    private static final Logger LOGGER = Logger.getLogger(UsuarioNegocioEscritaImpl.class.getName());

    @EJB
    private PersistenciaErudio<Integer, Pessoa> persistenciaPessoa;
    
    @EJB
    private PersistenciaErudio<Integer, Usuario> persistenciaUsuario;
    
    @EJB
    private PersistenciaErudio<Integer, UsuarioExterno> persistenciaUsuarioExterno;
 
    @EJB
    private PersistenciaErudio<Integer, SenhaUsuarioExterno> persistenciaSenhaUsuarioExterno;
    
    @EJB
    private PersistenciaErudio<Integer, SolicitacaoRedefinicaoSenha> persistenciaSolicitacaoRedefinicaoSenha;
    
    @EJB
    private PerfilAcessoNegocioLeitura perfilAcessoLeitura;
    
    @EJB
    private UsuarioNegocioLeitura usuarioLeitura;
    
    @EJB
    private TipoUsuarioNegocioLeitura tipoUsuarioLeitura;
    
    @EJB
    private ParametroSistemaNegocioLeitura parametroSistemaLeitura;

    /**
     * PARA_FAZER: RN162 - recuperar o nome na receita federal (criacao do webservece da receita)
     */
    @Override
    public void cadastrarUsuarioExterno(UsuarioExternoVO usuarioExternoVO) {
        validarUsuarioExternoVOParaCadastro(usuarioExternoVO);
        UsuarioExterno usuarioExterno = converter(usuarioExternoVO, UsuarioExterno.class);
        usuarioExterno.setDataCadastro(new Date());
        
        Usuario usuario = usuarioExterno.getUsuario();
        usuario.setAtivo(Boolean.FALSE);
        usuario.setPerfil(perfilAcessoLeitura.buscarPerfilAcessoPorNome(PerfilAcessoEnum.USUARIO_EXTERNO.getNome()));
        usuario.setTipoUsuario(tipoUsuarioLeitura.buscarTipoUsuarioPorNome(TipoUsuarioEnum.USUARIO_EXTERNO.getNome()));
        
        Pessoa pessoa = usuario.getPessoa();
        pessoa.setUsuario(Boolean.TRUE);
        pessoa.setNomePessoaReceita(null);
        
        SenhaUsuarioExterno senhaUsuarioExterno = converter(usuarioExternoVO.getSenhaUsuarioExternoVO(), SenhaUsuarioExterno.class);
        senhaUsuarioExterno.setSenha(Util.stringToMD5(senhaUsuarioExterno.getSenha()));
        senhaUsuarioExterno.setDataCriacao(new Date());
        senhaUsuarioExterno.setUsuarioExterno(usuarioExterno);
        
        persistenciaPessoa.gravar(pessoa);
        persistenciaUsuario.gravar(usuario);
        persistenciaUsuarioExterno.gravar(usuarioExterno);
        persistenciaSenhaUsuarioExterno.gravar(senhaUsuarioExterno);
        
        try {
            enviarEmailConfirmacaoCadastro(usuarioExterno);
        } catch (NegocioException ne) {
            LOGGER.log(Level.FINE, ne.getMessage(), ne);
            NegocioException.throwExceptionErro("MSG010");
        }
        
    }

    private void enviarEmailConfirmacaoCadastro(UsuarioExterno usuarioExterno) {
        /*Prezado(a) <NOME_USUARIO>,<br/><br/>Seu cadastro no "Sistema de Ouvidoria do Superior Tribunal de Justiça" foi realizado.<br/><br/>Para concluir a ativação do cadastro, clique no link abaixo:<br/><a href="<ENDERECO_SISTEMA>/public/ativarCadastro?codigo=<IDENTIFICADOR>"><ENDERECO_SISTEMA>/public/ativarCadastro?codigo=<IDENTIFICADOR></a><br/><br/>Caso não tenha solicitado cadastro no sistema, favor ignorar esta mensagem.<br/><br/>Atenciosamente,<br/><br/>Ouvidoria do Superior Tribunal de Justiça */
        String email = usuarioExterno.getUsuario().getPessoa().getEmail();
        String emailNoReply = parametroSistemaLeitura.buscarParametroSistemaPorNome(ParametroEnum.NO_REPLY_EMAIL.getParametro()).getDescricao();
        String enderecoSistema = parametroSistemaLeitura.buscarParametroSistemaPorNome(ParametroEnum.ENDERECO_SISTEMA.getParametro()).getDescricao();
        String conteudo = parametroSistemaLeitura.buscarParametroSistemaPorNome(EmailEnum.CADASTRO_USUARIO.getParametro()).getDescricao();
        String titulo = EmailEnum.CADASTRO_USUARIO.getTitulo();
        String nomeUsuario = usuarioExterno.getUsuario().getPessoa().getNome();
        EmailVO emailVO = EmailVOBuilder.buildEmailTipoHTML()
            .setDestinatario(email)
            .setRemetente(emailNoReply)
            .setConteudo(conteudo)
            .setTitulo(titulo)
            .addParametroConteudo(ParametroEnum.ENDERECO_SISTEMA.getParametro(), enderecoSistema)
            .addParametroConteudo(ParametroEnum.NOME_USUARIO.getParametro(), nomeUsuario)
            .addParametroConteudo(ParametroEnum.IDENTIFICADOR.getParametro(), email)
            .build();
        EmailUtil.enviar(emailVO);
    }

    private void validarUsuarioExternoVOParaCadastro(UsuarioExternoVO usuarioExternoVO) {
        UsuarioExterno usuarioExterno = usuarioLeitura
                .buscarUsuarioExternoPorEmail(usuarioExternoVO.getUsuario().getPessoa().getEmail());
        if (usuarioExterno != null) {
            NegocioException.throwExceptionErro("MSG065");
        }
        
        //validar tamanho da senha
        //validar caixa alta do nome completo do usuario
        //validar email
        //validar email ERUDIO
    }

    @Override
    public String ativarCadastro(String codigo) {
        String email = codigo;
        UsuarioExterno usuarioExterno = usuarioLeitura.buscarUsuarioExternoPorEmail(email);
        try {
            validadeCadastroJaAtivado(usuarioExterno);
            validarDataCadastroExpirado(usuarioExterno);
        } catch (NegocioException e) {
            LOGGER.log(Level.FINE, e.getMessage(), e);
            return e.getMessages().get(0).getMsg();
        }
        
        Usuario usuario = usuarioExterno.getUsuario();
        usuario.setAtivo(Boolean.TRUE);
        persistenciaUsuario.gravar(usuario);
        
        usuarioExterno.setDataConfirmacaoCadastro(new Date());
        persistenciaUsuarioExterno.gravar(usuarioExterno);
        
        return "MSG_CADASTRO_ATIVADO";
    }
    
    private void validadeCadastroJaAtivado(UsuarioExterno usuarioExterno) {
        if (usuarioExterno.getDataConfirmacaoCadastro() != null) {
            NegocioException.throwExceptionErro("MSG153");
        }
    }
    
    private void validarDataCadastroExpirado(UsuarioExterno usuarioExterno) {
        DateTime dataCadastro = new DateTime(usuarioExterno.getDataCadastro());
        DateTime dataLimiteCadastro  = dataCadastro.plusDays(1);
        if (dataLimiteCadastro.isBeforeNow()) {
            NegocioException.throwExceptionErro("MSG132");
        }
    }

    /**
     * PARA_FAZER: RN162 - recuperar o nome na receita federal (criacao do webservece da receita)
     */
    @Override
    public void atualizarUsuarioExterno(UsuarioExternoVO usuarioExternoVO) {
        UsuarioExterno usuarioExterno = converter(usuarioExternoVO, UsuarioExterno.class);
        
        Usuario usuario = usuarioExterno.getUsuario();
        
        Pessoa pessoa = usuario.getPessoa();
        pessoa.setNomePessoaReceita(null); 
        
        persistenciaPessoa.gravar(pessoa);
        persistenciaUsuario.gravar(usuario);
        persistenciaUsuarioExterno.gravar(usuarioExterno);
        atualizarSenha(usuarioExternoVO, usuarioExterno);
    }

    private void atualizarSenha(UsuarioExternoVO usuarioExternoVO, UsuarioExterno usuarioExterno) {
        if(StringUtils.isNotBlank(usuarioExternoVO.getSenhaUsuarioExternoVO().getSenha())){
            SenhaUsuarioExterno senhaUsuarioExterno = converter(usuarioExternoVO.getSenhaUsuarioExternoVO(), SenhaUsuarioExterno.class);
            senhaUsuarioExterno.setSenha(Util.stringToMD5(senhaUsuarioExterno.getSenha()));
            senhaUsuarioExterno.setDataCriacao(new Date());
            senhaUsuarioExterno.setUsuarioExterno(usuarioExterno);
            
            persistenciaSenhaUsuarioExterno.gravar(senhaUsuarioExterno);
        }
    }

    @Override
    public void solicitarRedefinicaoSenha(String email) {
        if (!StringUtils.isEmpty(email) && email.toLowerCase().endsWith("@erudio.com.br")){
            NegocioException.throwExceptionErro("MSG002");
        }
        UsuarioExterno usuarioExterno = usuarioLeitura.buscarUsuarioExternoPorEmail(email);
        if (usuarioExterno == null) {
            NegocioException.throwExceptionErro("MSG089");
        }
        SolicitacaoRedefinicaoSenha solicitacaoRedefinicaoSenha = new SolicitacaoRedefinicaoSenha();
        solicitacaoRedefinicaoSenha.setDataSolicitacao(new Date());
        solicitacaoRedefinicaoSenha.setUsuarioExterno(usuarioExterno);
        solicitacaoRedefinicaoSenha.setUtilizada(Boolean.FALSE);
        persistenciaSolicitacaoRedefinicaoSenha.gravar(solicitacaoRedefinicaoSenha);
        
        try {
            enviarEmailRedefinicaoSenha(usuarioExterno);
        } catch (NegocioException ne) {
            LOGGER.log(Level.FINE, ne.getMessage(), ne);
            NegocioException.throwExceptionErro("MSG097");
        }
    }
    
    private void enviarEmailRedefinicaoSenha(UsuarioExterno usuarioExterno) {
        /*Prezado(a) <NOME_USUARIO>,<br/><br/>Você solicitou cadastro de uma nova senha. Para redefinir sua senha de acesso ao ERUDIO <a href="<ENDERECO_SISTEMA>/public/redefinirSenha?codigo=<IDENTIFICADOR>">clique aqui</a> ou cole o seguinte link no seu navegador:<br/><br/><ENDERECO_SISTEMA>/public/redefinirSenha?codigo=<IDENTIFICADOR><br/><br/>O link vencerá em 24 horas, portanto, utilize-o imediatamente.<br/><br/>Caso não tenha solicitado cadastramento de nova senha, favor ignorar esta mensagem.<br/><br/>Agradecemos seu contato.<br/><br/>Atenciosamente,<br/><br/>Ouvidoria do Superior Tribunal de Justiça*/
        String email = usuarioExterno.getUsuario().getPessoa().getEmail();
        String emailNoReply = parametroSistemaLeitura.buscarParametroSistemaPorNome(ParametroEnum.NO_REPLY_EMAIL.getParametro()).getDescricao();
        String enderecoSistema = parametroSistemaLeitura.buscarParametroSistemaPorNome(ParametroEnum.ENDERECO_SISTEMA.getParametro()).getDescricao();
        String conteudo = parametroSistemaLeitura.buscarParametroSistemaPorNome(EmailEnum.REDEFINICAO_SENHA.getParametro()).getDescricao();
        String titulo = EmailEnum.REDEFINICAO_SENHA.getTitulo();
        String nomeUsuario = usuarioExterno.getUsuario().getPessoa().getNome();
        EmailVO emailVO = EmailVOBuilder.buildEmailTipoHTML()
            .setDestinatario(email)
            .setRemetente(emailNoReply)
            .setConteudo(conteudo)
            .setTitulo(titulo)
            .addParametroConteudo(ParametroEnum.ENDERECO_SISTEMA.getParametro(), enderecoSistema)
            .addParametroConteudo(ParametroEnum.NOME_USUARIO.getParametro(), nomeUsuario)
            .addParametroConteudo(ParametroEnum.IDENTIFICADOR.getParametro(), email)
            .build();
        EmailUtil.enviar(emailVO);
    }

    @Override
    public void associarUsuariosPerfil(List<Integer> idUsuarios, Integer idPerfil) {

        for (Integer idUsuario : idUsuarios) {

            Usuario usuario = new Usuario();
            usuario = usuarioLeitura.buscarUsuarioPorId(idUsuario);

            validarUsuarioJaAssociadoAPerfil(usuario);

            usuario.setPerfil(perfilAcessoLeitura.buscarPerfilAcessoPorId(idPerfil));
            persistenciaUsuario.gravar(usuario);
        }

    }

    private void validarUsuarioJaAssociadoAPerfil(Usuario usuario) {
        if (usuario != null && !PerfilAcessoEnum.USUARIO_INTERNO.getNome().equals(usuario.getPerfil().getNome())) {
            NegocioException.throwExceptionErro("MSG015");
        }
    }

    @Override
    public void excluirAssociarUsuariosPerfil(UsuarioInternoVO usuariosInternoExclusaoAssociacaoPerfil) {

        if (usuariosInternoExclusaoAssociacaoPerfil != null) {
            UsuarioInterno usuariosInterno = converter(usuariosInternoExclusaoAssociacaoPerfil, UsuarioInterno.class);

            if (usuariosInterno.getUsuario() != null) {
                Usuario usuario = usuariosInterno.getUsuario();
                usuario.setPerfil(perfilAcessoLeitura.buscarPerfilAcessoPorNome(PerfilAcessoEnum.USUARIO_INTERNO.getNome()));
                persistenciaUsuario.gravar(usuario);

            }
        }
    }

    @Override
    public void redefinicaoSenha(UsuarioExternoVO usuarioExternoVO) {
        UsuarioExterno usuarioExterno = converter(usuarioExternoVO, UsuarioExterno.class);
        SenhaUsuarioExterno novaSenha = converter(usuarioExternoVO.getSenhaUsuarioExternoVO(), SenhaUsuarioExterno.class);
        String novaSenhaMD5 = Util.stringToMD5(novaSenha.getSenha());
        List<SenhaUsuarioExterno> senhasAnteriores = usuarioLeitura.buscarUltimasTresSenhasDoUsuarioExterno(usuarioExterno);
        for (SenhaUsuarioExterno senhaUsuarioExterno : senhasAnteriores) {
            if (senhaUsuarioExterno.getSenha().equals(novaSenhaMD5)) {
                NegocioException.throwExceptionErro("MSG112");
            }
        }
        SolicitacaoRedefinicaoSenha solicitacaoRedefinicaoSenha = usuarioLeitura.buscarUltimaSolicitacaoRedefinicaoSenha(usuarioExterno);
        solicitacaoRedefinicaoSenha.setUtilizada(Boolean.TRUE);
        persistenciaSolicitacaoRedefinicaoSenha.gravar(solicitacaoRedefinicaoSenha);
        
        novaSenha.setDataCriacao(new Date());
        novaSenha.setSenha(novaSenhaMD5);
        novaSenha.setUsuarioExterno(usuarioExterno);
        
        persistenciaSenhaUsuarioExterno.gravar(novaSenha);
        
        this.ativarUsuarioExternoAposRedefinicaoSenha(usuarioExterno);
    }
    
    private void ativarUsuarioExternoAposRedefinicaoSenha(UsuarioExterno usuarioExterno) {
        Usuario usuario = usuarioExterno.getUsuario();
        if (!usuario.getAtivo()) {
            usuario.setAtivo(Boolean.TRUE);
            persistenciaUsuario.gravar(usuario);
            
            usuarioExterno.setDataConfirmacaoCadastro(new Date());
            persistenciaUsuarioExterno.gravar(usuarioExterno);
        }
    }

}