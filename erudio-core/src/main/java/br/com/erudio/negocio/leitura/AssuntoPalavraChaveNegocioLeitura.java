package br.com.erudio.negocio.leitura;

import java.util.List;

import javax.ejb.Local;

import br.com.erudio.dto.PaginacaoDTO;
import br.com.erudio.modelo.AssuntoPalavraChave;
import br.com.erudio.modelo.PalavraChave;
import br.com.erudio.modelo.TipoAssunto;
import br.com.erudio.negocio.vo.AssuntoPalavraChaveVO;
import br.com.erudio.negocio.vo.PalavraChaveVO;
import br.com.erudio.negocio.vo.TipoAssuntoVO;

@Local
public interface AssuntoPalavraChaveNegocioLeitura {

    TipoAssuntoVO buscaAssuntoPorId(Long id);
    List<TipoAssuntoVO> buscarTodosAssuntos();

    List<PalavraChaveVO> buscarPalavraChavesPorNome(String nome);
    
    PalavraChaveVO buscaPalavraChavePorId(Long id);
    
    Boolean verificarPalavraChaveVinculadaManifestacaoPorIdPalavraChave(Integer idPalavraChave);
    
    Boolean verificarPalavraChaveVinculadaManifestacaoPorIdTipoAssunto(Integer idTipoAssunto);
    
    Boolean verificarPalavraChaveVinculadaTipoAssuntoPorIdPalavraChave(Integer idPalavraChave);
    
    Boolean verificarPalavraChaveVinculadaTipoAssuntoPorIdTipoAssunto(Integer idTipoAssunto);
    
    List<AssuntoPalavraChave> buscarAssuntoPalavraChavePorIdPalavraChave(Integer idPalavraChave);
    
    List<AssuntoPalavraChaveVO> buscarAssuntoPalavraChaveVOPorIdPalavraChave(Integer idPalavraChave);
    
    List<AssuntoPalavraChaveVO> buscarAssuntoPalavraChaveVoPorIdTipoAssunto(Integer idAssunto);
    
    List<AssuntoPalavraChave> buscarAssuntoPalavraChavePorIdTipoAssunto(Integer idTipoAssunto);
    
    PalavraChave buscarPalavraChavePorNome(String nome);
    
    TipoAssunto buscarTipoAssuntoPorNome(String nome);
    
    void buscarPalavraChave(PaginacaoDTO<PalavraChaveVO> palavraChaveDTO);
    
    void buscarAssunto(PaginacaoDTO<TipoAssuntoVO> assuntoDTO);

}
