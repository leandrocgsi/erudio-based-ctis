package br.com.erudio.negocio.vo;

import java.util.List;

import br.com.erudio.negocio.vo.base.BaseVO;

public class FuncionalidadeVO extends BaseVO {

    private static final long serialVersionUID = 1L;

    private Integer id;

    private String nome;

    private String descricao;

    private Boolean ativa;

    private Boolean administrativa;
    
    private List<TipoAcessoVO> tipoAcessoVOs;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Boolean getAtiva() {
        return ativa;
    }

    public void setAtiva(Boolean ativa) {
        this.ativa = ativa;
    }

    public Boolean getAdministrativa() {
        return administrativa;
    }

    public void setAdministrativa(Boolean administrativa) {
        this.administrativa = administrativa;
    }

    public List<TipoAcessoVO> getTipoAcessoVOs() {
        return tipoAcessoVOs;
    }

    public void setTipoAcessoVOs(List<TipoAcessoVO> tipoAcessoVOs) {
        this.tipoAcessoVOs = tipoAcessoVOs;
    }
    
}
