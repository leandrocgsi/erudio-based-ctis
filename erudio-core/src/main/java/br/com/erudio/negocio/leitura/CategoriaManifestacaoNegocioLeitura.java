package br.com.erudio.negocio.leitura;

import java.util.List;

import br.com.erudio.dto.PaginacaoDTO;
import br.com.erudio.modelo.CategoriaManifestacao;
import br.com.erudio.negocio.vo.CategoriaManifestacaoVO;

public interface CategoriaManifestacaoNegocioLeitura {
    
    List<CategoriaManifestacaoVO> buscarTodas();
    
    List<CategoriaManifestacaoVO> buscarTodasAtiva();
    
    CategoriaManifestacao buscarPorNomeCategoriaManifestacao(String nome);

    void buscarCategoriaManifestacao(PaginacaoDTO<CategoriaManifestacaoVO> categoriaManifestacaoVO);

}
