package br.com.erudio.negocio.escrita;

import br.com.erudio.negocio.vo.TipoManifestacaoVO;

public interface TipoManifestacaoNegocioEscrita {
    TipoManifestacaoVO incluirTipoManifestacao(TipoManifestacaoVO tipoManifestacao);
    
    TipoManifestacaoVO alterarTipoManifestacao(TipoManifestacaoVO tipoManifestacao);
    
    void excluir(Integer idTipoManifestacao);

}