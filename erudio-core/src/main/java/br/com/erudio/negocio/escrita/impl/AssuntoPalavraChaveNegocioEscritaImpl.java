package br.com.erudio.negocio.escrita.impl;

import java.util.List;

import br.com.erudio.exception.NegocioException;
import br.com.erudio.modelo.AssuntoPalavraChave;
import br.com.erudio.modelo.PalavraChave;
import br.com.erudio.modelo.TipoAssunto;
import br.com.erudio.modelo.chavecomposta.AssuntoPalavraChaveId;
import br.com.erudio.negocio.NegocioBaseErudio;
import br.com.erudio.negocio.escrita.AssuntoPalavraChaveNegocioEscrita;
import br.com.erudio.negocio.leitura.AssuntoPalavraChaveNegocioLeitura;
import br.com.erudio.negocio.vo.PalavraChaveVO;
import br.com.erudio.negocio.vo.TipoAssuntoVO;
import br.com.erudio.persistencia.PersistenciaErudio;

@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class AssuntoPalavraChaveNegocioEscritaImpl extends NegocioBaseErudio
        implements AssuntoPalavraChaveNegocioEscrita {

    @EJB
    private PersistenciaErudio<Integer, TipoAssunto> persistenciaTipoAssunto;

    @EJB
    private PersistenciaErudio<Integer, PalavraChave> persistenciaPalavraChave;

    @EJB
    private PersistenciaErudio<AssuntoPalavraChaveId, AssuntoPalavraChave> persistenciaAssuntoPalavraChave;

    @EJB
    private AssuntoPalavraChaveNegocioLeitura assuntoPalavraChaveLeiura;

    @Override
    public TipoAssuntoVO inserirAssunto(TipoAssuntoVO tipoAssuntoVO) {
        validarTipoAssuntoVOParaGravacao(tipoAssuntoVO);
        TipoAssunto tipoAssunto = converter(tipoAssuntoVO, TipoAssunto.class);
        List<AssuntoPalavraChave> assuntoPalavraChaves = converter(tipoAssuntoVO.getAssuntoPalavraChaveVOs(),
                AssuntoPalavraChave.class);
        persistenciaTipoAssunto.gravar(tipoAssunto);
        for (AssuntoPalavraChave assuntoPalavraChave : assuntoPalavraChaves) {
            assuntoPalavraChave.setTipoAssunto(tipoAssunto);
            persistenciaAssuntoPalavraChave.gravar(assuntoPalavraChave);
        }
        return converter(tipoAssunto, TipoAssuntoVO.class);
    }

    @Override
    public TipoAssuntoVO alterarAssunto(TipoAssuntoVO tipoAssuntoVO) {
        validarTipoAssuntoVOParaGravacao(tipoAssuntoVO);
        TipoAssunto tipoAssunto = converter(tipoAssuntoVO, TipoAssunto.class);
        excluirAssuntoPalavraChaveVinculadoTipoAssuntoPorId(tipoAssunto.getId());
        List<AssuntoPalavraChave> assuntoPalavraChaves = converter(tipoAssuntoVO.getAssuntoPalavraChaveVOs(),
                AssuntoPalavraChave.class);
        for (AssuntoPalavraChave assuntoPalavraChave : assuntoPalavraChaves) {
            assuntoPalavraChave.setTipoAssunto(tipoAssunto);
            persistenciaAssuntoPalavraChave.gravar(assuntoPalavraChave);
        }
        persistenciaTipoAssunto.gravar(tipoAssunto);
        return converter(tipoAssunto, TipoAssuntoVO.class);
    }

    private void excluirAssuntoPalavraChaveVinculadoTipoAssuntoPorId(Integer idTipoAssunto) {
        List<AssuntoPalavraChave> assuntoPalavraChaves = assuntoPalavraChaveLeiura
                .buscarAssuntoPalavraChavePorIdTipoAssunto(idTipoAssunto);
        for (AssuntoPalavraChave assuntoPalavraChave : assuntoPalavraChaves) {
            AssuntoPalavraChave apc = persistenciaAssuntoPalavraChave.getEntityManager().merge(assuntoPalavraChave);
            persistenciaAssuntoPalavraChave.excluir(apc);
        }
    }

    @Override
    public PalavraChaveVO gravarPalavraChave(PalavraChaveVO palavraChaveVO) {
        validarPalavraChaveVOParaGravacao(palavraChaveVO);
        PalavraChave entidade = converter(palavraChaveVO, PalavraChave.class);
        PalavraChave entidadeGravada = persistenciaPalavraChave.gravar(entidade);
        return converter(entidadeGravada, PalavraChaveVO.class);
    }
    
    private void validarTipoAssuntoVOParaGravacao(TipoAssuntoVO tipoAssuntoVO){
        TipoAssunto tipoAssunto = assuntoPalavraChaveLeiura.buscarTipoAssuntoPorNome(tipoAssuntoVO.getNome());
        if(tipoAssunto != null && !tipoAssunto.getId().equals(tipoAssuntoVO.getId())){
            NegocioException.throwExceptionErro("MSG048");
        }
    }

    private void validarPalavraChaveVOParaGravacao(PalavraChaveVO palavraChaveVO) {
        PalavraChave palavraChave = assuntoPalavraChaveLeiura.buscarPalavraChavePorNome(palavraChaveVO.getNome());
        if (palavraChave != null && !palavraChave.getId().equals(palavraChaveVO.getId())) {
            NegocioException.throwExceptionErro("MSG060");
        }
    }

    @Override
    public void excluirPalavraChave(Integer idPalavraChave) {
        excluirAssuntoPalavraChavePorIdPalavraChave(idPalavraChave);
        persistenciaPalavraChave.excluir(PalavraChave.class, idPalavraChave);
    }

    private void excluirAssuntoPalavraChavePorIdPalavraChave(Integer idPalavraChave) {
        List<AssuntoPalavraChave> assuntoPalavraChaves = assuntoPalavraChaveLeiura
                .buscarAssuntoPalavraChavePorIdPalavraChave(idPalavraChave);
        for (AssuntoPalavraChave assuntoPalavraChave : assuntoPalavraChaves) {
            AssuntoPalavraChave apc = persistenciaAssuntoPalavraChave.getEntityManager().merge(assuntoPalavraChave);
            persistenciaAssuntoPalavraChave.excluir(apc);
        }
    }

    @Override
    public void excluirTipoAssunto(Integer idTipoAssunto) {
        excluirAssuntoPalavraChavePorIdTipoAssunto(idTipoAssunto);
        persistenciaTipoAssunto.excluir(TipoAssunto.class,idTipoAssunto);
    }

    private void excluirAssuntoPalavraChavePorIdTipoAssunto(Integer idTipoAssunto) {
        List<AssuntoPalavraChave> assuntoPalavrasChaves = assuntoPalavraChaveLeiura
                .buscarAssuntoPalavraChavePorIdTipoAssunto(idTipoAssunto);
        for (AssuntoPalavraChave assuntoPalavraChave : assuntoPalavrasChaves) {
            AssuntoPalavraChave apc = persistenciaAssuntoPalavraChave.getEntityManager().merge(assuntoPalavraChave);
            persistenciaAssuntoPalavraChave.excluir(apc);
        }
    }

    /**
     * PARA_FAZER:  @Override
          public Boolean verificarPalavraChaveVinculadaManifestacao(Integer id) {
                implementar PARA PC3 
            return Boolean.FALSE;
        }
     */

}
