package br.com.erudio.negocio.vo;

import java.util.Date;

import br.com.erudio.negocio.vo.base.BaseVO;

public class SenhaUsuarioExternoVO extends BaseVO {

    private static final long serialVersionUID = 1L;

    private Integer id;

    private UsuarioExternoVO usuarioExterno;

    private String senha;

    private Date dataCriacao;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public UsuarioExternoVO getUsuarioExterno() {
        return usuarioExterno;
    }

    public void setUsuarioExterno(UsuarioExternoVO usuarioExterno) {
        this.usuarioExterno = usuarioExterno;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public Date getDataCriacao() {
        return dataCriacao;
    }

    public void setDataCriacao(Date dataCriacao) {
        this.dataCriacao = dataCriacao;
    }

}