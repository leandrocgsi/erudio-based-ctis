package br.com.erudio.negocio.leitura;

import java.util.List;

import javax.ejb.Local;

import br.com.erudio.negocio.vo.PaisVO;

@Local
public interface PaisNegocioLeitura {

    List<PaisVO> buscarTodosAtivos();

    PaisVO buscarPorNome(String nome);
    
}