package br.com.erudio.negocio.escrita.impl;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import br.com.erudio.business.NegocioBase;
import br.com.erudio.exception.NegocioException;
import br.com.erudio.modelo.Auditoria;
import br.com.erudio.modelo.Funcionalidade;
import br.com.erudio.modelo.TipoAcesso;
import br.com.erudio.modelo.Usuario;
import br.com.erudio.modelo.base.EntidadeBase;
import br.com.erudio.negocio.enums.FuncionalidadeEnum;
import br.com.erudio.negocio.enums.LogTipoDadoEnum;
import br.com.erudio.negocio.enums.TipoAcessoEnum;
import br.com.erudio.negocio.escrita.AuditoriaNegocioEscrita;
import br.com.erudio.negocio.vo.LogAuditoriaVO;
import br.com.erudio.persistencia.PersistenciaErudio;
import br.com.erudio.util.ConversorLogAuditoria;

/**
 * EJB responsável por processar o Log de Auditoria. Os Logs são gravados de
 * acordo com a operação: - Inclusão de dados: logarInclusão - Exclusão de
 * dados: logarExclusao - Alteração de dados: armazenarDadosAtuais e
 * logarAlteracao
 * 
 * O EJB foi definido como Stateful para permitir que nos logs de alteração os
 * dados da consulta sejam guardados em memória entre as requisições de consulta
 * e de alteração, já que somente na requisição de alteração é que o log é
 * persistido em banco.
 */

@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class AuditoriaNegocioEscritaImpl extends NegocioBase implements AuditoriaNegocioEscrita {

    private static final Logger LOGGER = Logger.getLogger(AuditoriaNegocioEscritaImpl.class.getName());
    
    @EJB
    private PersistenciaErudio<Integer, Auditoria> persistencia;

    private LogAuditoriaVO auditoriaLog;

    /**
     * Gera e persiste o Log de Auditoria de funcionalidades de inclusão de
     * dados. As exceções são tratadas e enviadas para o Log do servidor para
     * que não impactem na atuação da funcionalidade de origem.
     */
    @Override
    public void logarInclusao(FuncionalidadeEnum funcionalidade, TipoAcessoEnum tipoAcesso, Usuario usuario,
            EntidadeBase... entidades) {
        try {
            auditoriaLog = ConversorLogAuditoria.converterEntidadeBase(LogTipoDadoEnum.NOVO, entidades);
            String xml = ConversorLogAuditoria.gerarXml(auditoriaLog);
            gravarLog(funcionalidade, tipoAcesso, usuario, xml);
        } catch (Exception e) {
            LOGGER.log(Level.FINE, e.getMessage(), e);
            NegocioException.throwExceptionErro(e.getMessage());
        }
    }

    /**
     * Gera e persiste o Log de Auditoria de funcionalidades de exclusão de
     * dados. As exceções são tratadas e enviadas para o Log do servidor para
     * que não impactem na atuação da funcionalidade de origem.
     */
    @Override
    public void logarExclusao(FuncionalidadeEnum funcionalidade, TipoAcessoEnum tipoAcesso, Usuario usuario,
            EntidadeBase... entidades) {
        try {
            auditoriaLog = ConversorLogAuditoria.converterEntidadeBase(LogTipoDadoEnum.ANTIGO, entidades);
            String xml = ConversorLogAuditoria.gerarXml(auditoriaLog);
            gravarLog(funcionalidade, tipoAcesso, usuario, xml);
        } catch (Exception e) {
            LOGGER.log(Level.FINE, e.getMessage(), e);
            NegocioException.throwExceptionErro(e.getMessage());
        }
    }

    /**
     * Gera e persiste o Log de Auditoria de funcionalidades de alteração de
     * dados. Antes de executar de este serviço é necessário que na mesma sessão
     * do ManagedBean o serviço armazenarDadosAtuaisAlteracao tenha sido
     * chamado, para que os dados antes da alteração sejam tratados. As exceções
     * são tratadas e enviadas para o Log do servidor para que não impactem na
     * atuação da funcionalidade de origem.
     */
    @Override
    public void logarAlteracao(FuncionalidadeEnum funcionalidade, TipoAcessoEnum tipoAcesso, Usuario usuario,
            EntidadeBase... entidades) {
        try {
            ConversorLogAuditoria.converterEntidadeBase(auditoriaLog, LogTipoDadoEnum.NOVO, entidades);
            String xml = ConversorLogAuditoria.gerarXml(auditoriaLog);
            gravarLog(funcionalidade, tipoAcesso, usuario, xml);
        } catch (Exception e) {
            LOGGER.log(Level.FINE, e.getMessage(), e);
            NegocioException.throwExceptionErro(e.getMessage());
        }
    }

    /**
     * Cria a entidade Auditoria com os dados informados e a persiste em banco.
     */
    private void gravarLog(FuncionalidadeEnum funcionalidadeEnum, TipoAcessoEnum tipoAcessoEnum, Usuario usuario,
            String xml) {
        Auditoria auditoria = new Auditoria();
        auditoria.setXmlAuditoria(xml);
        auditoria.setUsuario(usuario);
        auditoria.setNomePerfil(usuario.getPerfil().getNome());
        auditoria.setFuncionalidade(recuperarFuncionalidade(funcionalidadeEnum));
        auditoria.setTipoAcesso(recuperarTipoAcesso(tipoAcessoEnum));
        auditoria.setDataOperacao(new Date());

        persistencia.gravar(auditoria);
    }

    private Funcionalidade recuperarFuncionalidade(FuncionalidadeEnum funcionalidadeEnum) {
      //somente para zerar o sonar
        LOGGER.log(Level.INFO, funcionalidadeEnum.getDescricao(), funcionalidadeEnum);
        return null;
    }

    private TipoAcesso recuperarTipoAcesso(TipoAcessoEnum tipoAcessoEnum) {
      //somente para zerar o sonar
        LOGGER.log(Level.INFO, tipoAcessoEnum.getDescricao(), tipoAcessoEnum);
        return null;
    }

}
