package br.com.erudio.negocio.vo;

import br.com.erudio.negocio.vo.base.BaseVO;


/**
 * Classe que representa cada campo da entidade e contém o nome do campo e seu valor.
 */
public class LogCampoVO extends BaseVO {

    private static final long serialVersionUID = 1L;

    private String nome;
    
    private String valor;
    
    public LogCampoVO() {
        
    }
    
    public LogCampoVO(String nome, String valor) {
        this.nome = nome;
        this.valor = valor;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }
    
}
