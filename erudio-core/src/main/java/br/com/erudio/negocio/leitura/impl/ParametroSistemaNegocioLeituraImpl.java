package br.com.erudio.negocio.leitura.impl;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import br.com.erudio.modelo.ParametroSistema;
import br.com.erudio.negocio.NegocioBaseErudio;
import br.com.erudio.negocio.leitura.ParametroSistemaNegocioLeitura;
import br.com.erudio.persistencia.PersistenciaErudio;

@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class ParametroSistemaNegocioLeituraImpl extends NegocioBaseErudio implements ParametroSistemaNegocioLeitura {

    @EJB
    private PersistenciaErudio<Integer, ParametroSistema> persistencia;

    @Override
    public ParametroSistema buscarParametroSistemaPorNome(String nome) {
        CriteriaBuilder cb = persistencia.getEntityManager().getCriteriaBuilder();
        CriteriaQuery<ParametroSistema> criteriaQuery = cb.createQuery(ParametroSistema.class);
        Root<ParametroSistema> fromParametroSistema = criteriaQuery.from(ParametroSistema.class);

        criteriaQuery.select(fromParametroSistema).where(cb.equal(fromParametroSistema.get("nome"), nome));

        return persistencia.getEntityManager().createQuery(criteriaQuery).getSingleResult();
    }

    

}
