package br.com.erudio.negocio.leitura.impl;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import br.com.erudio.modelo.PronomeTratamento;
import br.com.erudio.negocio.NegocioBaseErudio;
import br.com.erudio.negocio.leitura.PronomeTratamentoNegocioLeitura;
import br.com.erudio.negocio.vo.PronomeTratamentoVO;
import br.com.erudio.persistencia.PersistenciaErudio;

@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class PronomeTratamentoNegocioLeituraImpl extends NegocioBaseErudio implements PronomeTratamentoNegocioLeitura {

    @EJB
    private PersistenciaErudio<Integer, PronomeTratamento> persistencia;

    @Override
    public List<PronomeTratamentoVO> buscarTodos() {
        List<PronomeTratamento> pronomeTratamentos = persistencia.obterTodos(PronomeTratamento.class);
        return converter(pronomeTratamentos, PronomeTratamentoVO.class);
    }

}
