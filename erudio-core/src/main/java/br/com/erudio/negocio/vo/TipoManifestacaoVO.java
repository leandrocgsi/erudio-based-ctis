package br.com.erudio.negocio.vo;

import br.com.erudio.negocio.vo.base.BaseVO;

public class TipoManifestacaoVO extends BaseVO {

    private static final long serialVersionUID = 1L;

    private Integer id;

    private String nome;

    private Integer prazoTratamentoOuvidoria;

    private Integer prazoTratamentoUnidade;

    private Boolean prorrogacaoPrazo;

    private Integer quantidadeDiasProrrogaveis;

    private TipoContagemPrazoVO tipoContagemPrazo;

    private CategoriaManifestacaoVO categoriaManifestacao;

    private Boolean ativo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getPrazoTratamentoOuvidoria() {
        return prazoTratamentoOuvidoria;
    }

    public void setPrazoTratamentoOuvidoria(Integer prazoTratamentoOuvidoria) {
        this.prazoTratamentoOuvidoria = prazoTratamentoOuvidoria;
    }

    public Integer getPrazoTratamentoUnidade() {
        return prazoTratamentoUnidade;
    }

    public void setPrazoTratamentoUnidade(Integer prazoTratamentoUnidade) {
        this.prazoTratamentoUnidade = prazoTratamentoUnidade;
    }

    public Boolean getProrrogacaoPrazo() {
        return prorrogacaoPrazo;
    }

    public void setProrrogacaoPrazo(Boolean prorrogacaoPrazo) {
        this.prorrogacaoPrazo = prorrogacaoPrazo;
    }

    public Integer getQuantidadeDiasProrrogaveis() {
        return quantidadeDiasProrrogaveis;
    }

    public void setQuantidadeDiasProrrogaveis(Integer quantidadeDiasProrrogaveis) {
        this.quantidadeDiasProrrogaveis = quantidadeDiasProrrogaveis;
    }

    public TipoContagemPrazoVO getTipoContagemPrazo() {
        return tipoContagemPrazo;
    }

    public void setTipoContagemPrazo(TipoContagemPrazoVO tipoContagemPrazo) {
        this.tipoContagemPrazo = tipoContagemPrazo;
    }

    public CategoriaManifestacaoVO getCategoriaManifestacao() {
        return categoriaManifestacao;
    }

    public void setCategoriaManifestacao(CategoriaManifestacaoVO categoriaManifestacao) {
        this.categoriaManifestacao = categoriaManifestacao;
    }

    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }

}