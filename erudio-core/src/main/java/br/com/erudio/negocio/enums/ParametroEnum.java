package br.com.erudio.negocio.enums;

public enum ParametroEnum {
    NOME_USUARIO("NOME_USUARIO"),
    ENDERECO_SISTEMA("ENDERECO_SISTEMA"),
    IDENTIFICADOR("IDENTIFICADOR"),
    CADASTRAR_USUARIO_EMAIL("CADASTRAR_USUARIO_EMAIL"),
    REDEFINICAO_SENHA_EMAIL("REDEFINICAO_SENHA_EMAIL"),
    NO_REPLY_EMAIL("NO_REPLY_EMAIL"),
    PATH_ARQUIVOS("PATH_ARQUIVOS"),
    PATH_ARQUIVOS_DESENV("PATH_ARQUIVOS_DESENV");
    
    private String parametro;
    
    private ParametroEnum(String parametro) {
        this.parametro = parametro;
    }

    public String getParametro() {
        return parametro;
    }

}