package br.com.erudio.negocio.escrita;

import java.util.List;

import br.com.erudio.negocio.vo.UsuarioExternoVO;
import br.com.erudio.negocio.vo.UsuarioInternoVO;

public interface UsuarioNegocioEscrita {

    void cadastrarUsuarioExterno(UsuarioExternoVO usuarioExternoVO);

    String ativarCadastro(String codigo);
    
    void atualizarUsuarioExterno(UsuarioExternoVO usuarioExternoVO);

    void solicitarRedefinicaoSenha(String email);
    
    void associarUsuariosPerfil(List<Integer> idUsuarios, Integer idPerfil);
    
    void excluirAssociarUsuariosPerfil(UsuarioInternoVO usuariosInterno);

    void redefinicaoSenha(UsuarioExternoVO usuarioExternoVO);

}
