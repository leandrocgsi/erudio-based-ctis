package br.com.erudio.negocio.vo;

import br.com.erudio.negocio.vo.base.BaseVO;

public class CargoVO extends BaseVO {

    private static final long serialVersionUID = 1L;

    private Integer id;

    private TipoCargoVO tipoCargo;

    private String nome;
    
    private CargoComissaoVO cargoComissao;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TipoCargoVO getTipoCargo() {
        return tipoCargo;
    }

    public void setTipoCargo(TipoCargoVO tipoCargo) {
        this.tipoCargo = tipoCargo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public CargoComissaoVO getCargoComissao() {
        return cargoComissao;
    }

    public void setCargoComissao(CargoComissaoVO cargoComissao) {
        this.cargoComissao = cargoComissao;
    }
    
}
