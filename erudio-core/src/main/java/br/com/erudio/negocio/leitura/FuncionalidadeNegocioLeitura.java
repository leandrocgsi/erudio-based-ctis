package br.com.erudio.negocio.leitura;

import java.util.List;

import javax.ejb.Local;

import br.com.erudio.modelo.Funcionalidade;

@Local
public interface FuncionalidadeNegocioLeitura {
    List<Funcionalidade> listarFuncionalidadesAtivas();
}
