package br.com.erudio.negocio.enums;

/**
 * Enumerator dos tipos de Dado: ANTIGO, NOVO.
 */
public enum LogTipoDadoEnum {
    
    ANTIGO("ANTIGO"),
    NOVO("NOVO");
    
    private String tipo;
    
    LogTipoDadoEnum(String tipo) {
        this.tipo = tipo;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
    
}
