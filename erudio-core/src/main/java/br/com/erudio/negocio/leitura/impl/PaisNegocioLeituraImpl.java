package br.com.erudio.negocio.leitura.impl;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import br.com.erudio.modelo.Pais;
import br.com.erudio.modelo.PronomeTratamento;
import br.com.erudio.negocio.NegocioBaseErudio;
import br.com.erudio.negocio.leitura.PaisNegocioLeitura;
import br.com.erudio.negocio.vo.PaisVO;
import br.com.erudio.persistencia.PersistenciaErudio;

@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class PaisNegocioLeituraImpl extends NegocioBaseErudio implements PaisNegocioLeitura {

    @EJB
    private PersistenciaErudio<Integer, PronomeTratamento> persistencia;

    @Override
    public List<PaisVO> buscarTodosAtivos() {
        List<Pais> pronomeTratamentos = persistencia.obterTodosAtivos(Pais.class);
        return converter(pronomeTratamentos, PaisVO.class);
    }

    @Override
    public PaisVO buscarPorNome(String nome) {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Pais> criteriaQuery = cb.createQuery(Pais.class);
        Root<Pais> fromPais = criteriaQuery.from(Pais.class);

        criteriaQuery.select(fromPais).where(cb.equal(fromPais.get("nome"), nome));

        Pais pais = getEntityManager().createQuery(criteriaQuery).getSingleResult();
        return converter(pais, PaisVO.class);
    }

}
