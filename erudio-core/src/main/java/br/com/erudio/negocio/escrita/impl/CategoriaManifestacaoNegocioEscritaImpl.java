package br.com.erudio.negocio.escrita.impl;

import javax.persistence.TypedQuery;

import br.com.erudio.exception.NegocioException;
import br.com.erudio.modelo.CategoriaManifestacao;
import br.com.erudio.negocio.NegocioBaseErudio;
import br.com.erudio.negocio.escrita.CategoriaManifestacaoNegocioEscrita;
import br.com.erudio.negocio.leitura.CategoriaManifestacaoNegocioLeitura;
import br.com.erudio.negocio.vo.CategoriaManifestacaoVO;
import br.com.erudio.persistencia.PersistenciaErudio;

@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class CategoriaManifestacaoNegocioEscritaImpl extends NegocioBaseErudio
        implements CategoriaManifestacaoNegocioEscrita {

    @EJB
    private PersistenciaErudio<Integer, CategoriaManifestacao> persistenciaCategoriaManifestacao;

    @EJB
    private CategoriaManifestacaoNegocioLeitura categoriaManifestacaoLeitura;

    @Override
    public CategoriaManifestacaoVO incluir(CategoriaManifestacaoVO categoriaManifestacaoVO) {

        validarCategoriaManifestacaoVOParaGravacao(categoriaManifestacaoVO);

        CategoriaManifestacao categoriaManifestacao = converter(categoriaManifestacaoVO, CategoriaManifestacao.class);
        persistenciaCategoriaManifestacao.gravar(categoriaManifestacao);

        return converter(categoriaManifestacao, CategoriaManifestacaoVO.class);
    }

    @Override
    public CategoriaManifestacaoVO alterar(CategoriaManifestacaoVO categoriaManifestacaoVO) {
        validarCategoriaManifestacaoVOParaGravacao(categoriaManifestacaoVO);
        CategoriaManifestacao categoriaManifestacao = converter(categoriaManifestacaoVO, CategoriaManifestacao.class);
        persistenciaCategoriaManifestacao.gravar(categoriaManifestacao);

        return converter(categoriaManifestacao, CategoriaManifestacaoVO.class);
    }

    @Override
    public void excluir(Integer idCategoriaManifestacao) {
        if(idCategoriaManifestacao != null){
            validarCategoriaManifestacaoParaExclusao(idCategoriaManifestacao);
            CategoriaManifestacao categoriaManifestacao = persistenciaCategoriaManifestacao.obter(CategoriaManifestacao.class, idCategoriaManifestacao);
            categoriaManifestacao = persistenciaCategoriaManifestacao.getEntityManager().merge(categoriaManifestacao);
            persistenciaCategoriaManifestacao.getEntityManager().remove(categoriaManifestacao);
        }

    }
    
    private void validarCategoriaManifestacaoParaExclusao(Integer idCategoriaManifestacao) {
        if (existeCategoriaManifestacaoVinculadoTipoManifestacao(idCategoriaManifestacao)) {
            NegocioException.throwExceptionErro("MSG138");
        }
    }

    public boolean existeCategoriaManifestacaoVinculadoTipoManifestacao(Integer idcategoriaManifestacao) {
        String jpql = "select count(tm) from TipoManifestacao tm where tm.categoriaManifestacao.id = :idcategoriaManifestacao";
        TypedQuery<Long> query = persistenciaCategoriaManifestacao.getEntityManager().createQuery(jpql, Long.class);
        query.setParameter("idcategoriaManifestacao", idcategoriaManifestacao);
        Long count = query.getSingleResult();
        return count > 0;
    }

    private void validarCategoriaManifestacaoVOParaGravacao(CategoriaManifestacaoVO categoriaManifestacaoVO) {
        CategoriaManifestacao categoriaManifestacao = categoriaManifestacaoLeitura
                .buscarPorNomeCategoriaManifestacao(categoriaManifestacaoVO.getNome());
        if (categoriaManifestacao != null && !categoriaManifestacao.getId().equals(categoriaManifestacaoVO.getId())) {
            NegocioException.throwExceptionErro("MSG052");
        }

    }
}
