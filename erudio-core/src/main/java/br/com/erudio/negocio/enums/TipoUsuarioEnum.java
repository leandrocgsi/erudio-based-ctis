package br.com.erudio.negocio.enums;

public enum TipoUsuarioEnum {
    USUARIO_INTERNO("Usuário Interno"),
    USUARIO_EXTERNO("Usuário Externo");
    
    private String nome;
    
    private TipoUsuarioEnum(String nome) {
        this.nome = nome;
    }
    
    public String getNome(){
        return nome;
    }

}
