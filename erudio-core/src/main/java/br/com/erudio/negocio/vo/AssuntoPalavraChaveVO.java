package br.com.erudio.negocio.vo;

import br.com.erudio.negocio.vo.base.BaseVO;

public class AssuntoPalavraChaveVO extends BaseVO {

    private static final long serialVersionUID = 1L;

    private AssuntoPalavraChaveIdVO id;

    private PalavraChaveVO palavraChave;

    private TipoAssuntoVO tipoAssunto;

    private Boolean ativo;

    public AssuntoPalavraChaveIdVO getId() {
        return id;
    }

    public void setId(AssuntoPalavraChaveIdVO id) {
        this.id = id;
    }

    public PalavraChaveVO getPalavraChave() {
        return palavraChave;
    }

    public void setPalavraChave(PalavraChaveVO palavraChave) {
        this.palavraChave = palavraChave;
    }

    public TipoAssuntoVO getTipoAssunto() {
        return tipoAssunto;
    }

    public void setTipoAssunto(TipoAssuntoVO tipoAssunto) {
        this.tipoAssunto = tipoAssunto;
    }

    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }
}
