package br.com.erudio.negocio.leitura;

import java.util.List;

import javax.ejb.Local;

import br.com.erudio.negocio.vo.GrauInstrucaoVO;

@Local
public interface GrauInstrucaoNegocioLeitura {

    List<GrauInstrucaoVO> buscarTodos();
    
}