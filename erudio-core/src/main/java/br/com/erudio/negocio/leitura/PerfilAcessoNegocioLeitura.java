package br.com.erudio.negocio.leitura;

import java.util.List;

import javax.ejb.Local;

import br.com.erudio.dto.PaginacaoDTO;
import br.com.erudio.modelo.PerfilAcesso;
import br.com.erudio.negocio.vo.PerfilAcessoPermissaoVO;
import br.com.erudio.negocio.vo.PerfilAcessoVO;

@Local
public interface PerfilAcessoNegocioLeitura {

    PerfilAcessoVO buscarPerfilAcessoVOPorId(Integer id);
    PerfilAcesso buscarPerfilAcessoPorId(Integer id);
    List<PerfilAcessoVO> buscarTodos();
    void buscaPaginada(PaginacaoDTO<PerfilAcessoVO> perfilAcessoVO);
    PerfilAcesso buscarPerfilAcessoPorNome(String nome);
    PerfilAcesso buscarPerfilAcessoDetalhadoPorId(Integer id);
    PerfilAcessoPermissaoVO buscarPerfilAcessoVODetalhadoPorId(Integer id);
    List<PerfilAcesso> listarPerfisAtivosComFuncionalidades();
    List<PerfilAcessoVO> buscarTodosPerfisInternos();
}