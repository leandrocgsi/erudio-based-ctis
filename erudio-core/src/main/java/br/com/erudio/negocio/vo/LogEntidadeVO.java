package br.com.erudio.negocio.vo;

import java.util.ArrayList;
import java.util.List;

import br.com.erudio.negocio.vo.base.BaseVO;


/**
 * Classe que representa uma Entidade com sua lista de campos.
 */
public class LogEntidadeVO extends BaseVO{
    
    private static final long serialVersionUID = 1L;

    private String nome;
    
    private List<LogCampoVO> campos;
    
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<LogCampoVO> getCampos() {
        return campos;
    }

    public void setCampos(List<LogCampoVO> campos) {
        this.campos = campos;
    }
    
    public void addLogCampoVO(LogCampoVO campo) {
        if (campos == null) {
            campos = new ArrayList<LogCampoVO>();
        }
        campos.add(campo);
    }
    
}
