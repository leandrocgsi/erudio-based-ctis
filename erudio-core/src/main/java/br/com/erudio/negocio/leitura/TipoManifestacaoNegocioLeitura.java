package br.com.erudio.negocio.leitura;

import java.util.List;

import br.com.erudio.dto.PaginacaoDTO;
import br.com.erudio.modelo.TipoManifestacao;
import br.com.erudio.negocio.vo.TipoManifestacaoVO;

public interface TipoManifestacaoNegocioLeitura {

    List<TipoManifestacaoVO> buscarTodos();
    
    void buscarTipoManifestacao(PaginacaoDTO<TipoManifestacaoVO> tipoManifestacaoDTO);

    List<TipoManifestacaoVO> buscarTipoManifestacaoVOPorIdCategoria(Integer idCategoria);
    
    TipoManifestacao buscarTipoManifestacaoPorNomePorIdCategoriaManifestacao(String nome, Integer idCategoriaManifestacao);
}
