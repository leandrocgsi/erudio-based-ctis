package br.com.erudio.negocio.escrita.impl;

import java.util.List;

import javax.persistence.Query;

import br.com.erudio.business.NegocioBase;
import br.com.erudio.exception.NegocioException;
import br.com.erudio.modelo.FuncionalidadeAcessoPerfil;
import br.com.erudio.modelo.PerfilAcesso;
import br.com.erudio.modelo.Usuario;
import br.com.erudio.modelo.chavecomposta.FuncionalidadeAcessoPerfilId;
import br.com.erudio.negocio.escrita.PerfilAcessoNegocioEscrita;
import br.com.erudio.negocio.leitura.PerfilAcessoNegocioLeitura;
import br.com.erudio.negocio.vo.PerfilAcessoPermissaoVO;
import br.com.erudio.negocio.vo.PerfilAcessoVO;
import br.com.erudio.persistencia.PersistenciaErudio;

@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class PerfilAcessoNegocioEscritaImpl extends NegocioBase implements PerfilAcessoNegocioEscrita {

    @EJB
    private PersistenciaErudio<Integer, PerfilAcesso> persistencia;
    
    @EJB
    private PersistenciaErudio<FuncionalidadeAcessoPerfilId, FuncionalidadeAcessoPerfil> persistenciaFuncionalidade;

    @EJB
    private PerfilAcessoNegocioLeitura perfilAcessoLeitura;
    
    @Override
    public PerfilAcessoVO gravar(PerfilAcessoVO vo) {
        PerfilAcesso entidade = converter(vo, PerfilAcesso.class);
        PerfilAcesso perfilAcessoCadastrado = buscarPerfilAcesso(entidade.getNome());
		if (jaExisteEOsIdsSaoDiferentes(entidade, perfilAcessoCadastrado)) {
            NegocioException.throwExceptionErro("MSG018");
        }
        PerfilAcesso entidadeGravada = persistencia.gravar(entidade);
        return converter(entidadeGravada, PerfilAcessoVO.class);
    }

	private boolean jaExisteEOsIdsSaoDiferentes(PerfilAcesso entidade, PerfilAcesso perfilAcessoCadastrado) {
		return perfilAcessoCadastrado != null && perfilAcessoCadastrado.getId() != entidade.getId();
	}

    @Override
    public void excluir(Integer id) {
    	List<Usuario> usuariosAssociados = buscarUsuariosAssociados(id);
		if (!usuariosAssociados.isEmpty()) {
            NegocioException.throwExceptionErro("MSG020");
        }
        persistencia.excluir(PerfilAcesso.class, id);
    }

    @Override
    public PerfilAcesso buscarPerfilAcesso(String nome) {
        String jpql = "select pa from PerfilAcesso pa where pa.nome = :nome";
        Query query = persistencia.getEntityManager().createQuery(jpql);
        query.setParameter("nome", nome);
        return (PerfilAcesso) query.getSingleResult();
    }

	@Override
	public void atualizarPermissoesPerfil(PerfilAcessoPermissaoVO perfilVO) {
		PerfilAcesso perfilBanco = perfilAcessoLeitura.buscarPerfilAcessoDetalhadoPorId(perfilVO.getId());
		PerfilAcesso perfilAlterado = converter(perfilVO, PerfilAcesso.class);
		excluirPermissoesPerfil(perfilBanco);
		for (FuncionalidadeAcessoPerfil permissao : perfilAlterado.getPermissoes()) {
			permissao.setPerfilAcesso(perfilBanco);
			persistenciaFuncionalidade.gravar(permissao);
		}
		
	}

	private void excluirPermissoesPerfil(PerfilAcesso perfil){
		for (FuncionalidadeAcessoPerfil permissao : perfil.getPermissoes()) {
			FuncionalidadeAcessoPerfil p = persistenciaFuncionalidade.getEntityManager().merge(permissao);
			persistenciaFuncionalidade.excluir(p);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Usuario> buscarUsuariosAssociados(Integer idPerfil) {
        String jpql = "select u from Usuario u where u.perfil.id = :idPerfil";
        Query query = persistencia.getEntityManager().createQuery(jpql);
        query.setParameter("idPerfil", idPerfil);
        return query.getResultList();
    }

}