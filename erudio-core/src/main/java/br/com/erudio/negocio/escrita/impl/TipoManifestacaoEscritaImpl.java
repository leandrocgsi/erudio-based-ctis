package br.com.erudio.negocio.escrita.impl;

import javax.persistence.TypedQuery;

import br.com.erudio.exception.NegocioException;
import br.com.erudio.modelo.TipoManifestacao;
import br.com.erudio.negocio.NegocioBaseErudio;
import br.com.erudio.negocio.escrita.TipoManifestacaoNegocioEscrita;
import br.com.erudio.negocio.leitura.TipoManifestacaoNegocioLeitura;
import br.com.erudio.negocio.vo.TipoManifestacaoVO;
import br.com.erudio.persistencia.PersistenciaErudio;

@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class TipoManifestacaoEscritaImpl extends NegocioBaseErudio implements TipoManifestacaoNegocioEscrita {

    @EJB
    private PersistenciaErudio<Integer, TipoManifestacao> persistenciaTipoManifestacao;

    @EJB
    private TipoManifestacaoNegocioLeitura tipoManifestacaoLeitura;

    @Override
    public TipoManifestacaoVO incluirTipoManifestacao(TipoManifestacaoVO tipoManifestacaoVO) {

        validarTipoManifestacaoVOParaGravacao(tipoManifestacaoVO);
        TipoManifestacao tipoManifestacao = converter(tipoManifestacaoVO, TipoManifestacao.class);
        persistenciaTipoManifestacao.gravar(tipoManifestacao);

        return converter(tipoManifestacao, TipoManifestacaoVO.class);
    }

    @Override
    public TipoManifestacaoVO alterarTipoManifestacao(TipoManifestacaoVO tipoManifestacaoVO) {
        if (tipoManifestacaoVO != null) {
            validarTipoManifestacaoVOParaGravacao(tipoManifestacaoVO);
            TipoManifestacao tipoManifestacao = converter(tipoManifestacaoVO, TipoManifestacao.class);
            tipoManifestacao = persistenciaTipoManifestacao.gravar(tipoManifestacao);
            return converter(tipoManifestacao, TipoManifestacaoVO.class);

        }
        return tipoManifestacaoVO;
    }

    @Override
    public void excluir(Integer idTipoManifestacao) {
        if (idTipoManifestacao != null) {
            validarTipoManifestacaoParaExclusao(idTipoManifestacao);
            TipoManifestacao tipoManifestacao = persistenciaTipoManifestacao.obter(TipoManifestacao.class,idTipoManifestacao);
            tipoManifestacao = persistenciaTipoManifestacao.getEntityManager().merge(tipoManifestacao);
            persistenciaTipoManifestacao.getEntityManager().remove(tipoManifestacao);
        }
    }

    private void validarTipoManifestacaoParaExclusao(Integer idTipoManifestacao) {
        if (existeTipoManifestacaoVinculadoManifestacao(idTipoManifestacao)) {
            NegocioException.throwExceptionErro("MSG141");
        }
    }

    private boolean existeTipoManifestacaoVinculadoManifestacao(Integer idTipoManifestacao) {
        String jpql = "select count(m) from Manifestacao m where m.tipoManifestacao.id = :idTipoManifestacao";
        TypedQuery<Long> query = persistenciaTipoManifestacao.getEntityManager().createQuery(jpql, Long.class);
        query.setParameter("idTipoManifestacao", idTipoManifestacao);
        Long count = query.getSingleResult();
        return count > 0;
    }

    private void validarTipoManifestacaoVOParaGravacao(TipoManifestacaoVO tipoManifestacaoVO) {
        TipoManifestacao tipoManuifestacao = tipoManifestacaoLeitura.buscarTipoManifestacaoPorNomePorIdCategoriaManifestacao(tipoManifestacaoVO.getNome(),
                        tipoManifestacaoVO.getCategoriaManifestacao().getId());
        if (tipoManuifestacao != null && !tipoManuifestacao.getId().equals(tipoManifestacaoVO.getId())) {
            NegocioException.throwExceptionErro("MSG140");
        }

    }

}