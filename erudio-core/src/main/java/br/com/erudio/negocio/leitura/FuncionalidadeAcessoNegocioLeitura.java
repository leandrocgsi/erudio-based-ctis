package br.com.erudio.negocio.leitura;

import java.util.List;

import javax.ejb.Local;

import br.com.erudio.negocio.vo.FuncionalidadeVO;
import br.com.erudio.negocio.vo.TipoAcessoVO;

@Local
public interface FuncionalidadeAcessoNegocioLeitura {

    List<FuncionalidadeVO> recuperarTodasFuncionalidadeVOsAtivos();
    
    List<TipoAcessoVO> recuperarTipoAcessosPorIdFuncionalidade(Integer idFuncionalidade);

}
