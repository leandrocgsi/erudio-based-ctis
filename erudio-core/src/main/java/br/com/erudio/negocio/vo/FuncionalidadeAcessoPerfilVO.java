package br.com.erudio.negocio.vo;

import br.com.erudio.negocio.vo.base.BaseVO;

public class FuncionalidadeAcessoPerfilVO extends BaseVO {
	
	private static final long serialVersionUID = 1L;
	
	private FuncionalidadeAcessoPerfilIdVO id;
	private FuncionalidadeVO funcionalidade;
	private TipoAcessoVO tipoAcesso;
	
	
	public FuncionalidadeAcessoPerfilIdVO getId() {
		return id;
	}
	
	public void setId(FuncionalidadeAcessoPerfilIdVO id) {
		this.id = id;
	}
	
	public FuncionalidadeVO getFuncionalidade() {
		return funcionalidade;
	}
	
	public void setFuncionalidade(FuncionalidadeVO funcionalidade) {
		this.funcionalidade = funcionalidade;
	}
	
	public TipoAcessoVO getTipoAcesso() {
		return tipoAcesso;
	}
	
	public void setTipoAcesso(TipoAcessoVO tipoAcesso) {
		this.tipoAcesso = tipoAcesso;
	}
}
