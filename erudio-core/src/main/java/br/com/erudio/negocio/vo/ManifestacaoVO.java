package br.com.erudio.negocio.vo;

import br.com.erudio.negocio.vo.base.BaseVO;

public class ManifestacaoVO extends BaseVO {

    private static final long serialVersionUID = 1L;

    private Integer id;

    private TipoManifestacaoVO tipoManifestacao;

    private TipoCanalEntradaVO tipoCanalEntrada;

    private TipoRespostaManifestacaoVO tipoRespostaManifestacao;

    private TipoRelacionamentoVO tipoRelacionamento;

    private PessoaVO pessoaManifestante;

    private PaisVO pais;

    private UnidadeFederativaVO uf;

    private ManifestacaoVO manifestacaoPai;

    private TipoSituacaoManifestacaoVO tipoSituacaoManifestacao;

    private String numeroProcesso;

    private String descricao;

    private String logradouro;

    private String numeroPostal;

    private String bairro;

    private String localidade;

    private String numeroEndereco;

    private String complementoEndereco;

    private String sistemaOrigem;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TipoManifestacaoVO getTipoManifestacao() {
        return tipoManifestacao;
    }

    public void setTipoManifestacao(TipoManifestacaoVO tipoManifestacao) {
        this.tipoManifestacao = tipoManifestacao;
    }

    public TipoCanalEntradaVO getTipoCanalEntrada() {
        return tipoCanalEntrada;
    }

    public void setTipoCanalEntrada(TipoCanalEntradaVO tipoCanalEntrada) {
        this.tipoCanalEntrada = tipoCanalEntrada;
    }

    public TipoRespostaManifestacaoVO getTipoRespostaManifestacao() {
        return tipoRespostaManifestacao;
    }

    public void setTipoRespostaManifestacao(TipoRespostaManifestacaoVO tipoRespostaManifestacao) {
        this.tipoRespostaManifestacao = tipoRespostaManifestacao;
    }

    public TipoRelacionamentoVO getTipoRelacionamento() {
        return tipoRelacionamento;
    }

    public void setTipoRelacionamento(TipoRelacionamentoVO tipoRelacionamento) {
        this.tipoRelacionamento = tipoRelacionamento;
    }

    public PessoaVO getPessoaManifestante() {
        return pessoaManifestante;
    }

    public void setPessoaManifestante(PessoaVO pessoaManifestante) {
        this.pessoaManifestante = pessoaManifestante;
    }

    public PaisVO getPais() {
        return pais;
    }

    public void setPais(PaisVO pais) {
        this.pais = pais;
    }

    public UnidadeFederativaVO getUf() {
        return uf;
    }

    public void setUf(UnidadeFederativaVO uf) {
        this.uf = uf;
    }

    public ManifestacaoVO getManifestacaoPai() {
        return manifestacaoPai;
    }

    public void setManifestacaoPai(ManifestacaoVO manifestacaoPai) {
        this.manifestacaoPai = manifestacaoPai;
    }

    public TipoSituacaoManifestacaoVO getTipoSituacaoManifestacao() {
        return tipoSituacaoManifestacao;
    }

    public void setTipoSituacaoManifestacao(TipoSituacaoManifestacaoVO tipoSituacaoManifestacao) {
        this.tipoSituacaoManifestacao = tipoSituacaoManifestacao;
    }

    public String getNumeroProcesso() {
        return numeroProcesso;
    }

    public void setNumeroProcesso(String numeroProcesso) {
        this.numeroProcesso = numeroProcesso;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public String getNumeroPostal() {
        return numeroPostal;
    }

    public void setNumeroPostal(String numeroPostal) {
        this.numeroPostal = numeroPostal;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getLocalidade() {
        return localidade;
    }

    public void setLocalidade(String localidade) {
        this.localidade = localidade;
    }

    public String getNumeroEndereco() {
        return numeroEndereco;
    }

    public void setNumeroEndereco(String numeroEndereco) {
        this.numeroEndereco = numeroEndereco;
    }

    public String getComplementoEndereco() {
        return complementoEndereco;
    }

    public void setComplementoEndereco(String complementoEndereco) {
        this.complementoEndereco = complementoEndereco;
    }

    public String getSistemaOrigem() {
        return sistemaOrigem;
    }

    public void setSistemaOrigem(String sistemaOrigem) {
        this.sistemaOrigem = sistemaOrigem;
    }

}
