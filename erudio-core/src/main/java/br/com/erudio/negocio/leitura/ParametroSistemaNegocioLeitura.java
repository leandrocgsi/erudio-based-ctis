package br.com.erudio.negocio.leitura;

import javax.ejb.Local;

import br.com.erudio.modelo.ParametroSistema;

@Local
public interface ParametroSistemaNegocioLeitura {

    ParametroSistema buscarParametroSistemaPorNome(String nome);
}