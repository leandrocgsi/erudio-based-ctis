package br.com.erudio.negocio.vo;

import java.util.List;

import br.com.erudio.negocio.vo.base.BaseVO;

public class TipoAssuntoVO extends BaseVO {

    private static final long serialVersionUID = 1L;

    private Integer id;
    private String nome;
    private Boolean ativo;
    private String descricao;
    private List<AssuntoPalavraChaveVO> assuntoPalavraChaveVOs;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public List<AssuntoPalavraChaveVO> getAssuntoPalavraChaveVOs() {
        return assuntoPalavraChaveVOs;
    }

    public void setAssuntoPalavraChaveVOs(List<AssuntoPalavraChaveVO> assuntoPalavraChaveVOs) {
        this.assuntoPalavraChaveVOs = assuntoPalavraChaveVOs;
    }
    
}
