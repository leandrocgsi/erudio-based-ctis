package br.com.erudio.negocio.escrita;

import java.util.List;

import javax.ejb.Local;

import br.com.erudio.modelo.PerfilAcesso;
import br.com.erudio.modelo.Usuario;
import br.com.erudio.negocio.vo.PerfilAcessoPermissaoVO;
import br.com.erudio.negocio.vo.PerfilAcessoVO;

@Local
public interface PerfilAcessoNegocioEscrita {

    PerfilAcessoVO gravar(PerfilAcessoVO objeto);
    PerfilAcesso buscarPerfilAcesso(String nome);
    List<Usuario> buscarUsuariosAssociados(Integer id);
    void excluir(Integer id);
    void atualizarPermissoesPerfil(PerfilAcessoPermissaoVO perfil);

}