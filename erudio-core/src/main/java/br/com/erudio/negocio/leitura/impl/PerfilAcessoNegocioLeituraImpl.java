package br.com.erudio.negocio.leitura.impl;

import java.util.List;

import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;

import br.com.erudio.dto.PaginacaoDTO;
import br.com.erudio.exception.NegocioException;
import br.com.erudio.modelo.PerfilAcesso;
import br.com.erudio.negocio.NegocioBaseErudio;
import br.com.erudio.negocio.enums.PerfilAcessoEnum;
import br.com.erudio.negocio.leitura.PerfilAcessoNegocioLeitura;
import br.com.erudio.negocio.vo.PerfilAcessoPermissaoVO;
import br.com.erudio.negocio.vo.PerfilAcessoVO;
import br.com.erudio.persistencia.PersistenciaErudio;
import br.com.erudio.util.Constante;

@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class PerfilAcessoNegocioLeituraImpl extends NegocioBaseErudio implements PerfilAcessoNegocioLeitura {

    @EJB
    private PersistenciaErudio<Integer, PerfilAcesso> persistencia;
 
    @Override
    public PerfilAcessoVO buscarPerfilAcessoVOPorId(Integer id) {
        PerfilAcesso perfilAcesso = persistencia.obter(PerfilAcesso.class, id);
        if (perfilAcesso == null) {
            NegocioException.throwExceptionErroIfNull(perfilAcesso, "registro.nao.encontrado");
        }
        return converter(perfilAcesso, PerfilAcessoVO.class);
    }

    @Override
    public List<PerfilAcessoVO> buscarTodosPerfisInternos() {
        String interno = PerfilAcessoEnum.USUARIO_INTERNO.getNome();
        String externo = PerfilAcessoEnum.USUARIO_EXTERNO.getNome();
        String jpql = "select pa from PerfilAcesso pa where pa.nome != :interno and pa.nome != :externo";
        Query query = persistencia.getEntityManager().createQuery(jpql);
        query.setParameter("interno", interno);
        query.setParameter("externo", externo);
        List<PerfilAcesso> listaPerfilAcesso = (List<PerfilAcesso>) query.getResultList();
        return converter(listaPerfilAcesso, PerfilAcessoVO.class);
    }

    @Override
    public void buscaPaginada(PaginacaoDTO<PerfilAcessoVO> perfilAcessoDTO) {
        Long total = getQueryTotalBuscaPerfilAcesso(perfilAcessoDTO).getSingleResult();
        List<PerfilAcesso> perfilAcessos = getQueryResultadoBuscaPerfilAcesso(perfilAcessoDTO).getResultList();
        
        List<PerfilAcessoVO> perfilAcessoVOs = converter(perfilAcessos, PerfilAcessoVO.class);
        perfilAcessoDTO.setList(perfilAcessoVOs);
        perfilAcessoDTO.setTotalResults(total.intValue());
    }
    
    private Query getQueryResultadoBuscaPerfilAcesso(PaginacaoDTO<PerfilAcessoVO> perfilAcessoDTO) {
        String jpql = getJpqlBuscaPerfilAcesso(perfilAcessoDTO, "pa");
        jpql = configurarOrdenacao(jpql, "pa", perfilAcessoDTO);
        Query query = persistencia.getEntityManager().createQuery(jpql);
        setFiltosBuscaPerfilAcesso(query, perfilAcessoDTO);
        configurarPaginacao(query, perfilAcessoDTO);
        return query;
    }
    
    private TypedQuery<Long> getQueryTotalBuscaPerfilAcesso(PaginacaoDTO<PerfilAcessoVO> perfilAcessoDTO) {
        String jpql = getJpqlBuscaPerfilAcesso(perfilAcessoDTO, "count(pa)");
        TypedQuery<Long> query = persistencia.getEntityManager().createQuery(jpql, Long.class);
        setFiltosBuscaPerfilAcesso(query, perfilAcessoDTO);
        return query;
    }
    
    private String getJpqlBuscaPerfilAcesso(PaginacaoDTO<PerfilAcessoVO> perfilAcessoDTO, String retorno) {
        String filtroNome = perfilAcessoDTO.getFiltros().get("nome").toString();
        String filtroSigla = perfilAcessoDTO.getFiltros().get(Constante.SIGLA).toString();
        Boolean filtroAtivo = (Boolean) perfilAcessoDTO.getFiltros().get(Constante.ATIVO);
        
        StringBuilder jpql = new StringBuilder();
        
        jpql.append("select ").append(retorno).append(" from PerfilAcesso pa");
        
        String filtro = " where ";
        if (!StringUtils.isEmpty(filtroNome)) {
            jpql.append(filtro).append("pa.nome = :nome");
            filtro = Constante.AND;
        }
        if (!StringUtils.isEmpty(filtroSigla)) {
            jpql.append(filtro).append("pa.sigla = :sigla");
            filtro = Constante.AND;
        }
        if (filtroAtivo != null) {
            jpql.append(filtro).append("pa.ativo = :ativo");
            filtro = Constante.AND;
        }
//        jpql.append(filtro).append("pa.nome != :externo");
//        filtro = Constante.AND;
        return jpql.toString();
    }
    
    private void setFiltosBuscaPerfilAcesso(Query query, PaginacaoDTO<PerfilAcessoVO> perfilAcessoDTO) {
        String filtroNome = perfilAcessoDTO.getFiltros().get("nome").toString();
        String filtroSigla = perfilAcessoDTO.getFiltros().get(Constante.SIGLA).toString();
        Boolean filtroAtivo = (Boolean) perfilAcessoDTO.getFiltros().get(Constante.ATIVO);
        
        if (!StringUtils.isEmpty(filtroNome)) {
            query.setParameter("nome", filtroNome);
        }
        if (!StringUtils.isEmpty(filtroSigla)) {
            query.setParameter(Constante.SIGLA, filtroSigla);
        }
        if (filtroAtivo != null) {
            query.setParameter(Constante.ATIVO, filtroAtivo);
        }
//        query.setParameter("externo", PerfilAcessoEnum.USUARIO_EXTERNO.getNome());
    }

    @Override
    public List<PerfilAcessoVO> buscarTodos() {
        List<PerfilAcesso> perfilAcessos = persistencia.obterTodos(PerfilAcesso.class);
        return converter(perfilAcessos, PerfilAcessoVO.class);
    }
    
    @Override
    public PerfilAcesso buscarPerfilAcessoPorNome(String nome) {
        CriteriaBuilder cb = persistencia.getEntityManager().getCriteriaBuilder();
        CriteriaQuery<PerfilAcesso> criteriaQuery = cb.createQuery(PerfilAcesso.class);
        Root<PerfilAcesso> fromPerfilAcesso = criteriaQuery.from(PerfilAcesso.class);
        fromPerfilAcesso.fetch("permissoes", JoinType.LEFT);
        criteriaQuery.select(fromPerfilAcesso).where(cb.equal(fromPerfilAcesso.get("nome"), nome));
        return persistencia.getEntityManager().createQuery(criteriaQuery).getSingleResult();
    }
    
    @Override
    public List<PerfilAcesso> listarPerfisAtivosComFuncionalidades(){
        
        CriteriaBuilder cb = persistencia.getEntityManager().getCriteriaBuilder();
        CriteriaQuery<PerfilAcesso> criteriaQuery = cb.createQuery(PerfilAcesso.class);
        Root<PerfilAcesso> fromPerfilAcesso = criteriaQuery.from(PerfilAcesso.class);
        fromPerfilAcesso.fetch("permissoes",JoinType.LEFT);
        criteriaQuery.select(fromPerfilAcesso).distinct(Boolean.TRUE)
            .where(cb.equal(fromPerfilAcesso.get(Constante.ATIVO), Boolean.TRUE));
        
        return (List<PerfilAcesso>) persistencia.getEntityManager().createQuery(criteriaQuery)
                .getResultList();
    }

    @Override
    public PerfilAcesso buscarPerfilAcessoPorId(Integer idPerfil) {
        PerfilAcesso perfilAcesso = persistencia.obter(PerfilAcesso.class, idPerfil);
        if (perfilAcesso == null) {
            NegocioException.throwExceptionErroIfNull(perfilAcesso, "registro.nao.encontrado");
        }
        return perfilAcesso;
    }
    
    @Override
    public PerfilAcesso buscarPerfilAcessoDetalhadoPorId(Integer id){
    	CriteriaBuilder cb = persistencia.getEntityManager().getCriteriaBuilder();
		CriteriaQuery<PerfilAcesso> criteriaQuery = cb.createQuery(PerfilAcesso.class);
		Root<PerfilAcesso> fromPerfilAcesso = criteriaQuery.from(PerfilAcesso.class);
		fromPerfilAcesso.fetch("permissoes", JoinType.LEFT);
		criteriaQuery.select(fromPerfilAcesso).where(cb.equal(fromPerfilAcesso.get("id"), id));
		return persistencia.getEntityManager().createQuery(criteriaQuery).getSingleResult();
    }
    
	@Override
	public PerfilAcessoPermissaoVO buscarPerfilAcessoVODetalhadoPorId(Integer id){
		PerfilAcesso perfilAcesso = buscarPerfilAcessoDetalhadoPorId(id);
		PerfilAcessoPermissaoVO perfilAcessoVO = converter(perfilAcesso, PerfilAcessoPermissaoVO.class);	        
		return perfilAcessoVO;
	}
	
	
}