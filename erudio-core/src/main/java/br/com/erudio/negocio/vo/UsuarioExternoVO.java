package br.com.erudio.negocio.vo;

import java.util.Date;

import br.com.erudio.negocio.vo.base.BaseVO;

public class UsuarioExternoVO extends BaseVO {

    private static final long serialVersionUID = 1L;

    private Integer id;

    private UsuarioVO usuario;

    private Date dataCadastro;

    private Date dataConfirmacaoCadastro;
    
    private SenhaUsuarioExternoVO senhaUsuarioExternoVO;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public UsuarioVO getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioVO usuario) {
        this.usuario = usuario;
    }

    public Date getDataCadastro() {
        return dataCadastro;
    }

    public void setDataCadastro(Date dataCadastro) {
        this.dataCadastro = dataCadastro;
    }

    public Date getDataConfirmacaoCadastro() {
        return dataConfirmacaoCadastro;
    }

    public void setDataConfirmacaoCadastro(Date dataConfirmacaoCadastro) {
        this.dataConfirmacaoCadastro = dataConfirmacaoCadastro;
    }

    public SenhaUsuarioExternoVO getSenhaUsuarioExternoVO() {
        return senhaUsuarioExternoVO;
    }

    public void setSenhaUsuarioExternoVO(SenhaUsuarioExternoVO senhaUsuarioExternoVO) {
        this.senhaUsuarioExternoVO = senhaUsuarioExternoVO;
    }
    
}