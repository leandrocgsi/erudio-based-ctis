package br.com.erudio.negocio.leitura.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import br.com.erudio.modelo.Funcionalidade;
import br.com.erudio.negocio.NegocioBaseErudio;
import br.com.erudio.negocio.leitura.FuncionalidadeAcessoNegocioLeitura;
import br.com.erudio.negocio.leitura.FuncionalidadeNegocioLeitura;
import br.com.erudio.negocio.vo.FuncionalidadeVO;
import br.com.erudio.negocio.vo.TipoAcessoVO;
import br.com.erudio.persistencia.PersistenciaErudio;

@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class FuncionalidadeAcessoNegocioLeituraImpl extends NegocioBaseErudio implements FuncionalidadeAcessoNegocioLeitura {

    
    @EJB
    private PersistenciaErudio<Integer, Funcionalidade> persistenciaFuncionalidade;
    
    @EJB
    private FuncionalidadeNegocioLeitura funcionalidadeLeitura;
    
    @Override
    public List<FuncionalidadeVO> recuperarTodasFuncionalidadeVOsAtivos() {
        List<Funcionalidade> funcionalidades = funcionalidadeLeitura.listarFuncionalidadesAtivas();
        List<FuncionalidadeVO> funcionalidadeVOs = new ArrayList<FuncionalidadeVO>(); 
        for (Funcionalidade funcionalidade : funcionalidades) {
            FuncionalidadeVO funcionalidadeVO = converter(funcionalidade, FuncionalidadeVO.class);
            List<TipoAcessoVO> tipoAcessos = recuperarTipoAcessosPorIdFuncionalidade(funcionalidade.getId());
            funcionalidadeVO.setTipoAcessoVOs(converter(tipoAcessos, TipoAcessoVO.class));
            funcionalidadeVOs.add(funcionalidadeVO);
        }
        return funcionalidadeVOs;
    }

    @Override
    public List<TipoAcessoVO> recuperarTipoAcessosPorIdFuncionalidade(Integer idFuncionalidade) {
        return persistenciaFuncionalidade
                .getEntityManager()
                .createQuery("select fa.tipoAcesso from FuncionalidadeAcesso fa where fa.funcionalidade.id = :idFuncionalidade")
                .setParameter("idFuncionalidade", idFuncionalidade)
                .getResultList();
                
    }
    

}
