package br.com.erudio.negocio.vo;

import java.util.ArrayList;
import java.util.List;

import br.com.erudio.negocio.vo.base.BaseVO;

/**
 * Classe principal do Log de Auditoria.
 * Guarda em sua estrutura uma lista de Entidades Log dos dados antigos e 
 * outra lista de Entidades Log dos dados novos.
 */
public class LogAuditoriaVO extends BaseVO {
    
    private static final long serialVersionUID = 1L;

    private List<LogEntidadeVO> dadosAntigos;
    
    private List<LogEntidadeVO> dadosNovos;

    public List<LogEntidadeVO> getDadosAntigos() {
        return dadosAntigos;
    }

    public void setDadosAntigos(List<LogEntidadeVO> entidadesAntigas) {
        this.dadosAntigos = entidadesAntigas;
    }

    public List<LogEntidadeVO> getDadosNovos() {
        return dadosNovos;
    }

    public void setDadosNovos(List<LogEntidadeVO> entidadesNovas) {
        this.dadosNovos = entidadesNovas;
    }

    public void addLogEntidadeVOAntiga(LogEntidadeVO entidade) {
        if (dadosAntigos == null) {
            dadosAntigos = new ArrayList<LogEntidadeVO>();
        }
        dadosAntigos.add(entidade);
    }
    
    public void addLogEntidadeVONova(LogEntidadeVO entidade) {
        if (dadosNovos == null) {
            dadosNovos = new ArrayList<LogEntidadeVO>();
        }
        dadosNovos.add(entidade);
    }
}
