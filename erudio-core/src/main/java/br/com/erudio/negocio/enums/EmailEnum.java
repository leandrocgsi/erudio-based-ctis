package br.com.erudio.negocio.enums;

public enum EmailEnum {
    CADASTRO_USUARIO(ParametroEnum.CADASTRAR_USUARIO_EMAIL, "Cadastro de usuário"),
    REDEFINICAO_SENHA(ParametroEnum.REDEFINICAO_SENHA_EMAIL, "Redefinição de senha");
    
    private ParametroEnum parametro;
    
    private String titulo;
    
    private EmailEnum(ParametroEnum parametro, String titulo) {
        this.parametro = parametro;
        this.titulo = titulo;
    }
    
    public String getParametro() {
        return parametro.getParametro();
    }

    public String getTitulo() {
        return titulo;
    }

}
