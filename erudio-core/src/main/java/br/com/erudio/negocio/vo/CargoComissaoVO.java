package br.com.erudio.negocio.vo;

import br.com.erudio.negocio.vo.base.BaseVO;

public class CargoComissaoVO extends BaseVO {

    private static final long serialVersionUID = 1L;

    private Integer id;

    private UnidadeVO unidade;

    private Boolean direcao;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public UnidadeVO getUnidade() {
        return unidade;
    }

    public void setUnidade(UnidadeVO unidade) {
        this.unidade = unidade;
    }

    public Boolean getDirecao() {
        return direcao;
    }

    public void setDirecao(Boolean direcao) {
        this.direcao = direcao;
    }
}
