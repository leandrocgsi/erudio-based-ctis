package br.com.erudio.negocio.vo;

import java.util.List;


public class PerfilAcessoPermissaoVO extends PerfilAcessoVO {
		
	private static final long serialVersionUID = 1L;
	
	private List<FuncionalidadeAcessoPerfilVO> permissoes;

	public List<FuncionalidadeAcessoPerfilVO> getPermissoes() {
		return permissoes;
	}

	public void setPermissoes(List<FuncionalidadeAcessoPerfilVO> permissoes) {
		this.permissoes = permissoes;
	}


}
