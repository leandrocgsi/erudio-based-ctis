package br.com.erudio.negocio.leitura.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;

import br.com.erudio.dto.PaginacaoDTO;
import br.com.erudio.modelo.CategoriaManifestacao;
import br.com.erudio.negocio.NegocioBaseErudio;
import br.com.erudio.negocio.leitura.CategoriaManifestacaoNegocioLeitura;
import br.com.erudio.negocio.vo.CategoriaManifestacaoVO;
import br.com.erudio.persistencia.PersistenciaErudio;
import br.com.erudio.util.Constante;

@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class CategoriaManifestacaoNegocioLeituraImpl extends NegocioBaseErudio
        implements CategoriaManifestacaoNegocioLeitura {
    
    @EJB
    private PersistenciaErudio<Integer, CategoriaManifestacao> persistencia;
    

    @Override
    public List<CategoriaManifestacaoVO> buscarTodas() {
        List<CategoriaManifestacao> categoriaManifestacao = persistencia.obterTodos(CategoriaManifestacao.class);
        return converter(categoriaManifestacao, CategoriaManifestacaoVO.class);
    }

    @Override
    public List<CategoriaManifestacaoVO> buscarTodasAtiva() {
        CriteriaBuilder cb = getCriteriaBuilder();
        CriteriaQuery<CategoriaManifestacaoVO> criteria = cb.createQuery(CategoriaManifestacaoVO.class);
        Root<CategoriaManifestacao> categoria = criteria.from(CategoriaManifestacao.class);
        criteria.where(cb.equal(categoria.get(Constante.ATIVO), Boolean.TRUE));
        criteria.multiselect(categoria.get("id"), categoria.get("nome"), categoria.get("descricao"));
        return getEntityManager().createQuery(criteria).getResultList();
    }
  

    @Override
    protected EntityManager getEntityManager() {
        return persistencia.getEntityManager();
    }

    @Override
    public CategoriaManifestacao buscarPorNomeCategoriaManifestacao(String nome) {

        String jpql = "select cm from CategoriaManifestacao cm where cm.nome = :nome";
        Query query = persistencia.getEntityManager().createQuery(jpql);
        query.setParameter("nome", nome);
        return (CategoriaManifestacao) query.getSingleResult();
    }
 
    @Override
    public void buscarCategoriaManifestacao(PaginacaoDTO<CategoriaManifestacaoVO> categoriaManifestacaoDTO) {
        Long total = getQueryTotalBuscaCategoriaManifestacao(categoriaManifestacaoDTO).getSingleResult();
        List<CategoriaManifestacao> categoriaManifestacoes = getQueryResultadoBuscaCategoriaManifestacao(categoriaManifestacaoDTO).getResultList();
        
        List<CategoriaManifestacaoVO> categoriaManifestacaoVOs = converter(categoriaManifestacoes, CategoriaManifestacaoVO.class);
        categoriaManifestacaoDTO.setList(categoriaManifestacaoVOs);
        categoriaManifestacaoDTO.setTotalResults(total.intValue());
    }

    private  TypedQuery<Long> getQueryTotalBuscaCategoriaManifestacao(PaginacaoDTO<CategoriaManifestacaoVO> categoriaManifestacaoDTO) {
        String jpql = getJpqlBuscaCategoriaManifestacao(categoriaManifestacaoDTO, "count(cm)");
        TypedQuery<Long> query = persistencia.getEntityManager().createQuery(jpql, Long.class);
        setFiltosBuscaCategoriaManifestacao(query, categoriaManifestacaoDTO);
        return query;
    }
    private Query getQueryResultadoBuscaCategoriaManifestacao(PaginacaoDTO<CategoriaManifestacaoVO> categoriaManifestacaoDTO) {
        String jpql = getJpqlBuscaCategoriaManifestacao(categoriaManifestacaoDTO, "cm");
        jpql = configurarOrdenacao(jpql, "cm", categoriaManifestacaoDTO);
        Query query = persistencia.getEntityManager().createQuery(jpql);
        setFiltosBuscaCategoriaManifestacao(query, categoriaManifestacaoDTO);
        configurarPaginacao(query, categoriaManifestacaoDTO);
        return query;
    }
    private String getJpqlBuscaCategoriaManifestacao(PaginacaoDTO<CategoriaManifestacaoVO> categoriaManifestacaoDTO, String retorno) {
        String filtroNome = categoriaManifestacaoDTO.getFiltros().get("nome").toString();
        
        Boolean filtroAtivo = (Boolean) categoriaManifestacaoDTO.getFiltros().get(Constante.ATIVO);
        
        StringBuilder jpql = new StringBuilder();
        
        jpql.append("select ")
        
        .append(retorno)
        
        .append(" from CategoriaManifestacao cm");
        
        String filtro = " where ";
        if (!StringUtils.isEmpty(filtroNome)) {
            jpql
                .append(filtro).append("cm.nome = :nome");
            filtro = " and ";
        }
        if (filtroAtivo != null) {
            jpql
            .append(filtro).append("cm.ativo = :ativo");
            filtro = " and ";
        }
        return jpql.toString();
    }
    private void setFiltosBuscaCategoriaManifestacao(Query query, PaginacaoDTO<CategoriaManifestacaoVO> categoriaManifestacaoDTO) {
        String filtroNome = categoriaManifestacaoDTO.getFiltros().get("nome").toString();
        Boolean filtroAtivo = (Boolean) categoriaManifestacaoDTO.getFiltros().get(Constante.ATIVO);
        
        if (!StringUtils.isEmpty(filtroNome)) {
            query.setParameter("nome", filtroNome);
        }
        if (filtroAtivo != null) {
            query.setParameter(Constante.ATIVO, filtroAtivo);
        }
    }
}
