package br.com.erudio.negocio.vo;

import br.com.erudio.negocio.vo.base.BaseVO;
import br.com.erudio.modelo.chavecomposta.CargoUsuarioInternoId;

public class CargoUsuarioInternoVO extends BaseVO {

    private static final long serialVersionUID = 1L;

    private CargoUsuarioInternoId id;

    private UsuarioInternoVO usuarioInterno;

    private CargoVO cargo;

    public CargoUsuarioInternoId getId() {
        return id;
    }

    public void setId(CargoUsuarioInternoId id) {
        this.id = id;
    }

    public UsuarioInternoVO getUsuarioInterno() {
        return usuarioInterno;
    }

    public void setUsuarioInterno(UsuarioInternoVO usuarioInterno) {
        this.usuarioInterno = usuarioInterno;
    }

    public CargoVO getCargo() {
        return cargo;
    }

    public void setCargo(CargoVO cargo) {
        this.cargo = cargo;
    }

}
