package br.com.erudio.negocio.vo;

import br.com.erudio.negocio.vo.base.BaseVO;

public class AssuntoPalavraChaveIdVO extends BaseVO {

    private static final long serialVersionUID = 1L;

    private Integer idPalavraChave;

    private Integer idTipoAssunto;

    public Integer getIdPalavraChave() {
        return idPalavraChave;
    }

    public void setIdPalavraChave(Integer idPalavraChave) {
        this.idPalavraChave = idPalavraChave;
    }

    public Integer getIdTipoAssunto() {
        return idTipoAssunto;
    }

    public void setIdTipoAssunto(Integer idTipoAssunto) {
        this.idTipoAssunto = idTipoAssunto;
    }

}