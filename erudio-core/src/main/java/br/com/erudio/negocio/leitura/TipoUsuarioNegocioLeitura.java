package br.com.erudio.negocio.leitura;

import javax.ejb.Local;

import br.com.erudio.modelo.TipoUsuario;

@Local
public interface TipoUsuarioNegocioLeitura {

    TipoUsuario buscarTipoUsuarioPorNome(String nome);
    
}