package br.com.erudio.negocio.vo;

import br.com.erudio.negocio.vo.base.BaseVO;

public class UsuarioVO extends BaseVO {

    private static final long serialVersionUID = 1L;

    private Integer id;
    private PessoaVO pessoa;
    private PerfilAcessoVO perfil;
    private TipoUsuarioVO tipoUsuario;
    private Boolean ativo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public PessoaVO getPessoa() {
        return pessoa;
    }

    public void setPessoa(PessoaVO pessoa) {
        this.pessoa = pessoa;
    }

    public PerfilAcessoVO getPerfil() {
        return perfil;
    }

    public void setPerfil(PerfilAcessoVO perfil) {
        this.perfil = perfil;
    }

    public TipoUsuarioVO getTipoUsuario() {
        return tipoUsuario;
    }

    public void setTipoUsuario(TipoUsuarioVO tipoUsuario) {
        this.tipoUsuario = tipoUsuario;
    }

    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }
}