package br.com.erudio.negocio.escrita;

import javax.ejb.Local;

import br.com.erudio.negocio.vo.PalavraChaveVO;
import br.com.erudio.negocio.vo.TipoAssuntoVO;

@Local
public interface AssuntoPalavraChaveNegocioEscrita {

    TipoAssuntoVO inserirAssunto(TipoAssuntoVO objeto);
    PalavraChaveVO gravarPalavraChave(PalavraChaveVO objeto);

    void excluirPalavraChave(Integer id);
    void excluirTipoAssunto(Integer id);
    TipoAssuntoVO alterarAssunto(TipoAssuntoVO assunto);

}
