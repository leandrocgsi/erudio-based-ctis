package br.com.erudio.negocio.escrita;

import javax.ejb.Local;

import br.com.erudio.negocio.vo.ManifestacaoVO;

@Local
public interface ManifestacaoNegocioEscrita {

    ManifestacaoVO cadastrarManifestacao(ManifestacaoVO manifestacaoVO);

}
