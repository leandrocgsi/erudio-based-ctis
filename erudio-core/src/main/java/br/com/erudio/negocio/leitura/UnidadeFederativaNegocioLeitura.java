package br.com.erudio.negocio.leitura;

import java.util.List;

import javax.ejb.Local;

import br.com.erudio.negocio.vo.UnidadeFederativaVO;

@Local
public interface UnidadeFederativaNegocioLeitura {

    List<UnidadeFederativaVO> buscarTodos();
    
}