package br.com.erudio.negocio.leitura.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;

import br.com.erudio.dto.PaginacaoDTO;
import br.com.erudio.modelo.TipoManifestacao;
import br.com.erudio.negocio.NegocioBaseErudio;
import br.com.erudio.negocio.leitura.TipoManifestacaoNegocioLeitura;
import br.com.erudio.negocio.vo.TipoManifestacaoVO;
import br.com.erudio.persistencia.PersistenciaErudio;
import br.com.erudio.util.Constante;

@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class TipoManifestacaoLeituraImpl extends NegocioBaseErudio implements TipoManifestacaoNegocioLeitura {
    @EJB
    private PersistenciaErudio<Integer, TipoManifestacao> persistenciaTipoManifestacao;

    @Override
    public List<TipoManifestacaoVO> buscarTodos() {
        List<TipoManifestacao> tipoManifestacao = persistenciaTipoManifestacao.obterTodos(TipoManifestacao.class);
        return converter(tipoManifestacao, TipoManifestacaoVO.class);
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public List<TipoManifestacaoVO> buscarTipoManifestacaoVOPorIdCategoria(Integer idCategoria) {
        List<TipoManifestacaoVO> tipoManifestacaoVOs = new ArrayList<>();
        if (idCategoria != null) {
            tipoManifestacaoVOs = converter(buscarTipoManifestacaoPorIdCategoria(idCategoria),
                    TipoManifestacaoVO.class);
        }
        return tipoManifestacaoVOs;

    }

    public List<TipoManifestacao> buscarTipoManifestacaoPorIdCategoria(Integer idCategoria) {
        List<TipoManifestacao> tipoManifestacao = new ArrayList<>();

        if (idCategoria != null) {
            CriteriaBuilder cb = persistenciaTipoManifestacao.getEntityManager().getCriteriaBuilder();
            CriteriaQuery<TipoManifestacao> criteriaQuery = cb.createQuery(TipoManifestacao.class);
            Root<TipoManifestacao> fromTipoManifestacao = criteriaQuery.from(TipoManifestacao.class);

            criteriaQuery.multiselect(fromTipoManifestacao.get("id"), fromTipoManifestacao.get("nome"))
                    .where(cb.equal(fromTipoManifestacao.get("categoriaManifestacao").get("id"), idCategoria));

            tipoManifestacao = persistenciaTipoManifestacao.getEntityManager().createQuery(criteriaQuery)
                    .getResultList();

            return tipoManifestacao;
        }
        return tipoManifestacao;

    }

    @Override
    public TipoManifestacao buscarTipoManifestacaoPorNomePorIdCategoriaManifestacao(String nome,Integer idCategoriaManifestacao) {
        String jpql = "select tm from TipoManifestacao tm where tm.nome = :nome and tm.categoriaManifestacao.id = :idCategoriaManifestacao";
        Query query = persistenciaTipoManifestacao.getEntityManager().createQuery(jpql);
        query.setParameter("nome", nome);
        query.setParameter("idCategoriaManifestacao", idCategoriaManifestacao);
        return (TipoManifestacao) query.getSingleResult();
    }
    
    @Override
    public void buscarTipoManifestacao(PaginacaoDTO<TipoManifestacaoVO> tipoManifestacaoDTO) {
        Long total = getQueryTotalBuscaTipoManifestacao(tipoManifestacaoDTO).getSingleResult();
        List<TipoManifestacao> tipoManifestacao = getQueryResultadoBuscaTipoManifestacao(tipoManifestacaoDTO).getResultList();
        
        List<TipoManifestacaoVO> tipoManifestacaoVOs = converter(tipoManifestacao, TipoManifestacaoVO.class);
        tipoManifestacaoDTO.setList(tipoManifestacaoVOs);
        tipoManifestacaoDTO.setTotalResults(total.intValue());
    }
    private TypedQuery<Long> getQueryTotalBuscaTipoManifestacao(PaginacaoDTO<TipoManifestacaoVO> tipoManifestacaoDTO) {
        String jpql = getJpqlBuscaTipoManifestacao(tipoManifestacaoDTO, "count(tm)");
        TypedQuery<Long> query = persistenciaTipoManifestacao.getEntityManager().createQuery(jpql, Long.class);
        setFiltosBuscaTipoManifestacao(query, tipoManifestacaoDTO);
        return query;
    }
    
    private Query getQueryResultadoBuscaTipoManifestacao(PaginacaoDTO<TipoManifestacaoVO> tipoManifestacaoDTO) {
        String jpql = getJpqlBuscaTipoManifestacao(tipoManifestacaoDTO, "tm");
        jpql = configurarOrdenacao(jpql, "tm", tipoManifestacaoDTO);
        Query query = persistenciaTipoManifestacao.getEntityManager().createQuery(jpql);
        setFiltosBuscaTipoManifestacao(query, tipoManifestacaoDTO);
        configurarPaginacao(query, tipoManifestacaoDTO);
        return query;
    }
    
    private String getJpqlBuscaTipoManifestacao(PaginacaoDTO<TipoManifestacaoVO> tipoManifestacaoDTO, String retorno) {
        String filtroTipoManifestacao = (String) getValorNoMapa(tipoManifestacaoDTO.getFiltros(), "tipoManifestacao.nome");
        
        String filtroCategoriaManifestacao = (String) getValorNoMapa(tipoManifestacaoDTO.getFiltros(), "categoriaManifestacao.nome");

        Boolean filtroAtivo = (Boolean) tipoManifestacaoDTO.getFiltros().get(Constante.ATIVO);
        
        StringBuilder jpql = new StringBuilder();
        
        jpql.append("select ")
        
        .append(retorno)
        
        .append(" from TipoManifestacao tm");
        
        String filtro = " where ";
        if (!StringUtils.isEmpty(filtroTipoManifestacao)) {
            jpql
                .append(filtro).append("tm.nome = :tipoManifestacao");
            filtro = Constante.AND;
        }
        if (!StringUtils.isEmpty(filtroCategoriaManifestacao)) {
            jpql
                .append(filtro).append("tm.categoriaManifestacao.nome = :categoriaManifestacao");
            filtro = Constante.AND;
        }
        if (filtroAtivo != null) {
            jpql
            .append(filtro).append("tm.ativo = :ativo");
            filtro = Constante.AND;
        }
        return jpql.toString();
    }
    
    private void setFiltosBuscaTipoManifestacao(Query query, PaginacaoDTO<TipoManifestacaoVO> tipoManifestacaoDTO) {
        
        String filtroTipoManifestacao = (String) getValorNoMapa(tipoManifestacaoDTO.getFiltros(), "tipoManifestacao.nome");
        
        String filtroCategoriaManifestacao = (String) getValorNoMapa(tipoManifestacaoDTO.getFiltros(), "categoriaManifestacao.nome");
        
        Boolean filtroAtivo = (Boolean) tipoManifestacaoDTO.getFiltros().get(Constante.ATIVO);
        
        if (!StringUtils.isEmpty(filtroCategoriaManifestacao)) {
            query.setParameter("categoriaManifestacao", filtroCategoriaManifestacao);
        }
        if (!StringUtils.isEmpty(filtroTipoManifestacao)) {
            query.setParameter("tipoManifestacao", filtroTipoManifestacao);
        }
        if (filtroAtivo != null) {
            query.setParameter(Constante.ATIVO, filtroAtivo);
        }
    }

}
