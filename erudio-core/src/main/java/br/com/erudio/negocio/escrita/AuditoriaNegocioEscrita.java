package br.com.erudio.negocio.escrita;

import br.com.erudio.modelo.Usuario;
import br.com.erudio.modelo.base.EntidadeBase;
import br.com.erudio.negocio.enums.FuncionalidadeEnum;
import br.com.erudio.negocio.enums.TipoAcessoEnum;

@Local
public interface AuditoriaNegocioEscrita {

    void logarInclusao(FuncionalidadeEnum funcionalidadeEnum, TipoAcessoEnum tipoAcessoEnum, Usuario usuario,
            EntidadeBase... entidades);

    void logarExclusao(FuncionalidadeEnum funcionalidadeEnum, TipoAcessoEnum tipoAcessoEnum, Usuario usuario,
            EntidadeBase... entidades);

    void logarAlteracao(FuncionalidadeEnum funcionalidadeEnum, TipoAcessoEnum tipoAcessoEnum, Usuario usuario,
            EntidadeBase... listaEntidades);

}
