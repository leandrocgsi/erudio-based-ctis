package br.com.erudio.persistencia;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.persistence.FlushModeType;
import javax.persistence.LockModeType;
import javax.persistence.Parameter;
import javax.persistence.Query;
import javax.persistence.TemporalType;
import org.apache.commons.collections.CollectionUtils;

public class QueryDecorator implements Query {
	private Query query;

	public QueryDecorator(Query query) {
		this.query = query;
	}

	public int executeUpdate() {
		return query.executeUpdate();
	}

	public int getFirstResult() {
		return query.getFirstResult();
	}

	public FlushModeType getFlushMode() {
		return query.getFlushMode();
	}

	public Map<String, Object> getHints() {
		return query.getHints();
	}

	public LockModeType getLockMode() {
		return query.getLockMode();
	}

	public int getMaxResults() {
		return query.getMaxResults();
	}

	public <T> Parameter<T> getParameter(int arg0, Class<T> arg1) {
		return query.getParameter(arg0, arg1);
	}

	public Parameter<?> getParameter(int arg0) {
		return query.getParameter(arg0);
	}

	public <T> Parameter<T> getParameter(String arg0, Class<T> arg1) {
		return query.getParameter(arg0, arg1);
	}

	public Parameter<?> getParameter(String arg0) {
		return query.getParameter(arg0);
	}

	public Object getParameterValue(int arg0) {
		return query.getParameterValue(arg0);
	}

	public <T> T getParameterValue(Parameter<T> arg0) {
		return (T) query.getParameterValue(arg0);
	}

	public Object getParameterValue(String arg0) {
		return query.getParameterValue(arg0);
	}

	public Set<Parameter<?>> getParameters() {
		return query.getParameters();
	}

	public List getResultList() {
		return query.getResultList();
	}

	public Object getSingleResult() {
		List resultado = getResultList();
		if (CollectionUtils.isNotEmpty(resultado)) {
			return resultado.get(0);
		}
		return null;
	}

	public boolean isBound(Parameter<?> arg0) {
		return query.isBound(arg0);
	}

	public Query setFirstResult(int arg0) {
		return query.setFirstResult(arg0);
	}

	public Query setFlushMode(FlushModeType arg0) {
		return query.setFlushMode(arg0);
	}

	public Query setHint(String arg0, Object arg1) {
		return query.setHint(arg0, arg1);
	}

	public Query setLockMode(LockModeType arg0) {
		return query.setLockMode(arg0);
	}

	public Query setMaxResults(int arg0) {
		return query.setMaxResults(arg0);
	}

	public Query setParameter(int arg0, Calendar arg1, TemporalType arg2) {
		return query.setParameter(arg0, arg1, arg2);
	}

	public Query setParameter(int arg0, Date arg1, TemporalType arg2) {
		return query.setParameter(arg0, arg1, arg2);
	}

	public Query setParameter(int arg0, Object arg1) {
		return query.setParameter(arg0, arg1);
	}

	public Query setParameter(Parameter<Calendar> arg0, Calendar arg1, TemporalType arg2) {
		return query.setParameter(arg0, arg1, arg2);
	}

	public Query setParameter(Parameter<Date> arg0, Date arg1, TemporalType arg2) {
		return query.setParameter(arg0, arg1, arg2);
	}

	public <T> Query setParameter(Parameter<T> arg0, T arg1) {
		return query.setParameter(arg0, arg1);
	}

	public Query setParameter(String arg0, Calendar arg1, TemporalType arg2) {
		return query.setParameter(arg0, arg1, arg2);
	}

	public Query setParameter(String arg0, Date arg1, TemporalType arg2) {
		return query.setParameter(arg0, arg1, arg2);
	}

	public Query setParameter(String arg0, Object arg1) {
		return query.setParameter(arg0, arg1);
	}

	public <T> T unwrap(Class<T> arg0) {
		return (T) query.unwrap(arg0);
	}
}