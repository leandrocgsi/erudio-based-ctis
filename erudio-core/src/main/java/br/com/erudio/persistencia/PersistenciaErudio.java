package br.com.erudio.persistencia;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import br.com.erudio.modelo.ExclusaoLogica;
import br.com.erudio.modelo.base.EntidadeBase;

@Stateless
public class PersistenciaErudio<K extends Serializable, T extends EntidadeBase<K>> extends PersistenceBase<K, T> {

    @PersistenceContext(unitName = "emErudio")
    private EntityManager em;

    @Override
    public EntityManager getEntityManager() {
        
        return new EntityManagerDecorator(em);
    }
    
    public List<T> obterTodos(Class<T> classe) {
        CriteriaBuilder cb =  getEntityManager().getCriteriaBuilder();
        CriteriaQuery<T> criteriaQuery = cb.createQuery(classe);
        Root<T> root = criteriaQuery.from(classe);
        criteriaQuery.select(root);
        return getEntityManager().createQuery(criteriaQuery).getResultList();
    }
    
    public <T extends EntidadeBase<K> & ExclusaoLogica> List<T> obterTodosAtivos(Class<T> type) {
        CriteriaBuilder cb =  getEntityManager().getCriteriaBuilder();
        CriteriaQuery<T> cq = cb.createQuery(type);
        Root<T> r = cq.from(type);
        
        cq.select(r).where(cb.equal(r.get("ativo"),Boolean.TRUE));
        
        return getEntityManager().createQuery(cq).getResultList();
    }

}
