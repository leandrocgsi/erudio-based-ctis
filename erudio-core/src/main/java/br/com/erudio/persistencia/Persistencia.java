package br.com.erudio.persistencia;

import java.io.Serializable;

import br.com.erudio.modelo.base.EntidadeBase;

public abstract interface Persistencia<PK extends Serializable, T extends EntidadeBase<PK>>
{
  public abstract T obter(Class<T> paramClass, PK paramPK);
  
  public abstract T gravar(T paramT);
  
  public abstract void excluir(Class<T> paramClass, PK paramPK);
  
  public abstract void excluir(T paramT);
}
