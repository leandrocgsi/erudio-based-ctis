package br.com.erudio.persistencia;

import java.io.Serializable;

import br.com.erudio.business.NegocioBase;
import br.com.erudio.exception.NegocioException;
import br.com.erudio.modelo.base.EntidadeBase;


public abstract class PersistenceBase<PK extends Serializable, T extends EntidadeBase<PK>>
  extends NegocioBase
  implements Persistencia<PK, T>
{
  @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
  public T obter(Class<T> tipo, PK id)
  {
    NegocioException.throwExceptionErroIfNull(id, "id.nao.informado");
    
    return (EntidadeBase)getEntityManager().find(tipo, id);
  }
  

  @TransactionAttribute(TransactionAttributeType.REQUIRED)
  public T gravar(T objeto)
  {
    validate(objeto);
    
    if (objeto.getId() == null)
    {
      getEntityManager().persist(objeto);
      
      return objeto;
    }
    
    return (EntidadeBase)getEntityManager().merge(objeto);
  }
  

  @TransactionAttribute(TransactionAttributeType.REQUIRED)
  public void excluir(Class<T> tipo, PK id)
  {
    T objeto = obter(tipo, id);
    
    NegocioException.throwExceptionErroIfNull(objeto, "registro.nao.encontrado");
    
    excluir(objeto);
  }
  

  @TransactionAttribute(TransactionAttributeType.REQUIRED)
  public void excluir(T objeto)
  {
    NegocioException.throwExceptionErroIfNull(objeto, "id.nao.informado");
    
    getEntityManager().remove(objeto);
  }
}