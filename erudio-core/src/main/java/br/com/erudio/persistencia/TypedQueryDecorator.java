package br.com.erudio.persistencia;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.persistence.FlushModeType;
import javax.persistence.LockModeType;
import javax.persistence.Parameter;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;
import org.apache.commons.collections.CollectionUtils;

public class TypedQueryDecorator<T> implements TypedQuery<T> {
	private TypedQuery<T> typedQuery;

	public TypedQueryDecorator(TypedQuery<T> typedQuery) {
		this.typedQuery = typedQuery;
	}

	public int executeUpdate() {
		return typedQuery.executeUpdate();
	}

	public int getFirstResult() {
		return typedQuery.getFirstResult();
	}

	public FlushModeType getFlushMode() {
		return typedQuery.getFlushMode();
	}

	public Map<String, Object> getHints() {
		return typedQuery.getHints();
	}

	public LockModeType getLockMode() {
		return typedQuery.getLockMode();
	}

	public int getMaxResults() {
		return typedQuery.getMaxResults();
	}

	public <T> Parameter<T> getParameter(int arg0, Class<T> arg1) {
		return typedQuery.getParameter(arg0, arg1);
	}

	public Parameter<?> getParameter(int arg0) {
		return typedQuery.getParameter(arg0);
	}

	public <T> Parameter<T> getParameter(String arg0, Class<T> arg1) {
		return typedQuery.getParameter(arg0, arg1);
	}

	public Parameter<?> getParameter(String arg0) {
		return typedQuery.getParameter(arg0);
	}

	public Object getParameterValue(int arg0) {
		return typedQuery.getParameterValue(arg0);
	}

	public <T> T getParameterValue(Parameter<T> arg0) {
		return (T) typedQuery.getParameterValue(arg0);
	}

	public Object getParameterValue(String arg0) {
		return typedQuery.getParameterValue(arg0);
	}

	public Set<Parameter<?>> getParameters() {
		return typedQuery.getParameters();
	}

	public List<T> getResultList() {
		return typedQuery.getResultList();
	}

	public T getSingleResult() {
		List<T> resultado = getResultList();
		if (CollectionUtils.isNotEmpty(resultado)) {
			return (T) resultado.get(0);
		}
		return null;
	}

	public boolean isBound(Parameter<?> arg0) {
		return typedQuery.isBound(arg0);
	}

	public TypedQuery<T> setFirstResult(int arg0) {
		return typedQuery.setFirstResult(arg0);
	}

	public TypedQuery<T> setFlushMode(FlushModeType arg0) {
		return typedQuery.setFlushMode(arg0);
	}

	public TypedQuery<T> setHint(String arg0, Object arg1) {
		return typedQuery.setHint(arg0, arg1);
	}

	public TypedQuery<T> setLockMode(LockModeType arg0) {
		return typedQuery.setLockMode(arg0);
	}

	public TypedQuery<T> setMaxResults(int arg0) {
		return typedQuery.setMaxResults(arg0);
	}

	public TypedQuery<T> setParameter(int arg0, Calendar arg1, TemporalType arg2) {
		return typedQuery.setParameter(arg0, arg1, arg2);
	}

	public TypedQuery<T> setParameter(int arg0, Date arg1, TemporalType arg2) {
		return typedQuery.setParameter(arg0, arg1, arg2);
	}

	public TypedQuery<T> setParameter(int arg0, Object arg1) {
		return typedQuery.setParameter(arg0, arg1);
	}

	public TypedQuery<T> setParameter(Parameter<Calendar> arg0, Calendar arg1, TemporalType arg2) {
		return typedQuery.setParameter(arg0, arg1, arg2);
	}

	public TypedQuery<T> setParameter(Parameter<Date> arg0, Date arg1, TemporalType arg2) {
		return typedQuery.setParameter(arg0, arg1, arg2);
	}

	public TypedQuery<T> setParameter(String arg0, Calendar arg1, TemporalType arg2) {
		return typedQuery.setParameter(arg0, arg1, arg2);
	}

	public TypedQuery<T> setParameter(String arg0, Date arg1, TemporalType arg2) {
		return typedQuery.setParameter(arg0, arg1, arg2);
	}

	public TypedQuery<T> setParameter(String arg0, Object arg1) {
		return typedQuery.setParameter(arg0, arg1);
	}

	public <T> T unwrap(Class<T> arg0) {
		return (T) typedQuery.unwrap(arg0);
	}

	public <U> TypedQuery<T> setParameter(Parameter<U> arg0, U arg1) {
		return typedQuery.setParameter(arg0, arg1);
	}
}