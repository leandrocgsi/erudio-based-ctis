package br.com.erudio.util;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.text.SimpleDateFormat;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.persistence.Transient;

import org.apache.commons.beanutils.PropertyUtils;
import org.hibernate.Hibernate;
import org.hibernate.collection.internal.PersistentBag;
import org.hibernate.proxy.HibernateProxy;

import br.com.erudio.exception.NegocioException;
import br.com.erudio.modelo.base.EntidadeBase;
import br.com.erudio.negocio.enums.LogTipoDadoEnum;
import br.com.erudio.negocio.vo.LogAuditoriaVO;
import br.com.erudio.negocio.vo.LogCampoVO;
import br.com.erudio.negocio.vo.LogEntidadeVO;

/**
 * Classe responsável pelas conversões do Log de Auditoria.
 */
public class ConversorLogAuditoria {

    /** Utilizado na formatação dos tipos data/hora. */
    private static final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss:SSS");

    private static final Logger LOGGER = Logger.getLogger(ConversorLogAuditoria.class.getName());

    private static final String SERIAL_VERSION = "serialVersionUID";

    private ConversorLogAuditoria() {

    }

    /**
     * Converte uma ou mais EntidadesBase em estruturas de EntidadesLog dentro
     * de um LogAuditoriaVO e o retorna.
     * 
     * @param tipoDadoLog
     *            Tipo do dado: ANTIGO ou NOVO.
     * @param entidadesBase
     *            Uma ou mais EntidadesBase a serem convertidas.
     * @return LogAuditoriaVO com a estrutura de dados preenchida.
     * @throws LogAuditoriaException
     *             Lança exceção em caso de erro na conversão.
     */
    public static LogAuditoriaVO converterEntidadeBase(LogTipoDadoEnum tipoDadoLog, EntidadeBase... entidadesBase) {
        LogAuditoriaVO log = new LogAuditoriaVO();
        converterEntidadeBase(log, tipoDadoLog, entidadesBase);
        return log;
    }

    /**
     * Converte uma ou mais EntidadesBase em estruturas de EntidadesLog dentro
     * do LogAuditoriaVO informado.
     * 
     * @param log
     *            LogAuditoriaVO a ser preenchido.
     * @param tipoDadoLog
     *            Tipo do dado: ANTIGO ou NOVO.
     * @param entidadesBase
     *            Uma ou mais EntidadesBase a serem convertidas.
     * @throws LogAuditoriaException
     *             Lança exceção em caso de erro na conversão.
     */
    public static void converterEntidadeBase(LogAuditoriaVO log, LogTipoDadoEnum tipoDadoLog,
            EntidadeBase... entidadesBase) {
        try {
            for (EntidadeBase entidadeBase : entidadesBase) {
                iterarEntidade(log, entidadeBase, tipoDadoLog, null);
            }
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            LOGGER.log(Level.FINE, e.getMessage(), e);
            NegocioException.throwExceptionErro(e.getMessage());
        }
    }

    /**
     * Itera os dados de uma entidade e gera a estrutra de dados dentro do
     * LogAuditoriaVO. Os dados iterados podem ser campos, outras entidades ou
     * Lists. Caso os dados sejas Entidade ou List, serão realizadas iterações
     * destes.
     * 
     * @param log
     *            LogAuditoriaVO a ser preenchido.
     * @param entidadeBase
     *            EntidadeBase a ser iterada.
     * @param tipoDadoLog
     *            Tipo do dado: ANTIGO ou NOVO.
     * @param entidadeBasePai
     *            Caso esta entidade tenha uma EntidadePai, este campo será
     *            preenchido, caso o contrário, será null.
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     * @throws NoSuchMethodException
     */
    private static void iterarEntidade(LogAuditoriaVO log, EntidadeBase entidadeBase, LogTipoDadoEnum tipoDadoLog,
            EntidadeBase entidadeBasePai)
                    throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        LogEntidadeVO entidadeLog = new LogEntidadeVO();

        addLogEntidade(log, tipoDadoLog, entidadeLog);

        entidadeLog.setNome(entidadeBase.getClass().getSimpleName());

        Field[] campos = entidadeBase.getClass().getDeclaredFields();
        for (Field campo : campos) {
            String nomeCampo = campo.getName();
            if (nomeCampo.equalsIgnoreCase(SERIAL_VERSION) || !PropertyUtils.isReadable(entidadeBase, nomeCampo)
                    || campo.isAnnotationPresent(Transient.class)) {
                continue;
            }

            Class<?> classeCampo = campo.getType();
            Object objValor = PropertyUtils.getSimpleProperty(entidadeBase, nomeCampo);

            String valor = getValor(log, entidadeBase, tipoDadoLog, entidadeBasePai, classeCampo, objValor);

            verificaValor(entidadeLog, nomeCampo, valor);
        }

    }

    private static String getValor(LogAuditoriaVO log, EntidadeBase entidadeBase, LogTipoDadoEnum tipoDadoLog,
            EntidadeBase entidadeBasePai, Class<?> classeCampo, Object objValor)
                    throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        String valor = null;

        if (objValor != null) {
            valor = verificaClasseCampo(log, entidadeBase, tipoDadoLog, entidadeBasePai, classeCampo, objValor);
        }
        return valor;
    }

    private static String verificaClasseCampo(LogAuditoriaVO log, EntidadeBase entidadeBase,
            LogTipoDadoEnum tipoDadoLog, EntidadeBase entidadeBasePai, Class<?> classeCampo, Object objValor)
                    throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        String resultado = null;
        if (classeCampo.isPrimitive() || isWrapper(classeCampo) || classeCampo.equals(String.class)) {
            resultado = objValor.toString();
        } else if (isTipoData(classeCampo)) {
            resultado = formataTipoData(objValor);
        } else if (classeCampo.equals(List.class)) {
            verificaEntidadeCarregada(log, entidadeBase, tipoDadoLog, objValor);
        } else if (isEntidadeBase(classeCampo) && objValor != entidadeBasePai && isEntidadeCarregada(entidadeBase)
                && entidadeBasePai == null) {
            iterarEntidade(log, (EntidadeBase) objValor, tipoDadoLog, entidadeBase);
        }
        return resultado;
    }

    private static void verificaEntidadeCarregada(LogAuditoriaVO log, EntidadeBase entidadeBase,
            LogTipoDadoEnum tipoDadoLog, Object objValor)
                    throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        if (isEntidadeCarregada(objValor)) {
            for (Object item : (List<?>) objValor) {
                if (isEntidadeCarregada(item)) {
                    iterarEntidade(log, (EntidadeBase) item, tipoDadoLog, entidadeBase);
                }
            }
        }
    }

    private static void addLogEntidade(LogAuditoriaVO log, LogTipoDadoEnum tipoDadoLog, LogEntidadeVO entidadeLog) {
        if (tipoDadoLog.getTipo().equals(LogTipoDadoEnum.ANTIGO.getTipo())) {
            log.addLogEntidadeVOAntiga(entidadeLog);
        } else if (tipoDadoLog.getTipo().equals(LogTipoDadoEnum.NOVO.getTipo())) {
            log.addLogEntidadeVONova(entidadeLog);
        }
    }

    private static void verificaValor(LogEntidadeVO entidadeLog, String nomeCampo, String valor) {
        if (valor != null) {
            LogCampoVO campoLog = new LogCampoVO();
            entidadeLog.addLogCampoVO(campoLog);
            campoLog.setNome(nomeCampo);
            campoLog.setValor(valor);
        }
    }

    /**
     * Verifica se a classe informada é do filha de EntidadeBase.
     * 
     * @param classeCampo
     *            Classe a ser verificada.
     * @return True se for filha de EntidadeBase, caso o contrário, retorna
     *         False.
     */
    private static boolean isEntidadeBase(Class<?> classeCampo) {
        return classeCampo.getSuperclass().equals(EntidadeBase.class);
    }

    /**
     * Verifica se classe informada é uma classe Wrapper.
     * 
     * @param classeCampo
     *            Classe a ser verificada.
     * @return True se for Wrapper, caso o contrário, retorna False.
     */
    private static boolean isWrapper(Class<?> classeCampo) {
        Boolean[] wrappers = new Boolean[] { classeCampo.equals(Boolean.class), classeCampo.equals(Byte.class),
                classeCampo.equals(Character.class), classeCampo.equals(Integer.class), classeCampo.equals(Float.class),
                classeCampo.equals(Double.class), classeCampo.equals(Long.class), classeCampo.equals(Short.class) };
        for (Boolean wrapper : wrappers) {
            if (wrapper) {
                return true;
            }
        }
        return false;
    }

    /**
     * Verifica se classe é do tipo Data.
     * 
     * @param classeCampo
     *            Classe a ser verificada.
     * @return True se for do tipoData, caso o contrário, retorna False.
     */
    private static boolean isTipoData(Class<?> classeCampo) {
        return classeCampo.equals(java.util.Date.class) || classeCampo.equals(java.sql.Date.class)
                || classeCampo.equals(java.sql.Timestamp.class) || classeCampo.equals(java.sql.Time.class);
    }

    /**
     * Formata em String um objeto do tipo data.
     * 
     * @param data
     *            Objeto do tipo data a ser formatado.
     * @return String com a data no formato: dd/MM/yyyy HH:mm:ss:SSS
     */
    private static String formataTipoData(Object data) {
        return sdf.format(data);
    }

    /**
     * Verifica se o objeto (EntidadeBase) está inicializado pelo Hibernate.
     * 
     * @param object
     *            EntidadeBase a ser verificada.
     * @return True se o objeto tiver sido inicializado, caso o contrário,
     *         retorna False.
     */
    private static boolean isEntidadeCarregada(Object object) {
        if (object instanceof HibernateProxy
                && ((HibernateProxy) object).getHibernateLazyInitializer().isUninitialized()) {
            return Boolean.FALSE;
        }
        if (object instanceof PersistentBag && !Hibernate.isInitialized(object)) {
            return Boolean.FALSE;
        }

        return object != null;
    }

    /**
     * Converte um LogAuditoriaVO em XML.
     * 
     * @param log
     *            LogAuditoriaVO a ser convertido.
     * @return String com o XML resultante.
     */
    public static String gerarXml(LogAuditoriaVO log) {
        LOGGER.log(Level.FINE, log.toString());
        return null;
    }

    /**
     * Converte um XML em LogAuditoriaVO.
     * 
     * @param xml
     *            XML a ser convertido.
     * @return LogAuditoriaVO resultante.
     */
    public static LogAuditoriaVO gerarObjeto(String xml) {
        LOGGER.log(Level.FINE, xml);
        return null;
    }

    /**
     * Converte em Map os dados antigos do XML.
     * 
     * @param xml
     * @return Retorna um LinkedHashMap com os dados na ordem em que estão no
     *         XML.
     */
    public static Map<String, String> converterDadosAntigos(String xml) {
        LogAuditoriaVO logAuditoriaVO = gerarObjeto(xml);
        return converterDadosAntigos(logAuditoriaVO);
    }

    /**
     * Converte em Map os dados novos do XML.
     * 
     * @param xml
     *            XML com os dados a serem convertidos.
     * @return Retorna um LinkedHashMap com os dados na ordem em que estão no
     *         XML.
     */
    public static Map<String, String> converterDadosNovos(String xml) {
        LogAuditoriaVO logAuditoriaVO = gerarObjeto(xml);
        return converterDadosNovos(logAuditoriaVO);
    }

    /**
     * Converte em Map os dados antigos do Auditoria Log.
     * 
     * @param logAuditoriaVO
     *            LogAuditoriaVO com os dados a serem convertidos.
     * @return Retorna um LinkedHashMap com os dados na ordem em que estão no
     *         LogAuditoriaVO.
     */
    public static Map<String, String> converterDadosAntigos(LogAuditoriaVO logAuditoriaVO) {
        List<LogEntidadeVO> listaEntidades = logAuditoriaVO.getDadosAntigos();
        if (listaEntidades == null) {
            return null;
        }
        return converterDados(listaEntidades);
    }

    /**
     * Converte em Map os dados novos do Auditoria Log.
     * 
     * @param logAuditoriaVO
     *            LogAuditoriaVO com os dados a serem convertidos.
     * @return Retorna um LinkedHashMap com os dados na ordem em que estão no
     *         LogAuditoriaVO.
     */
    public static Map<String, String> converterDadosNovos(LogAuditoriaVO logAuditoriaVO) {
        List<LogEntidadeVO> listaEntidades = logAuditoriaVO.getDadosNovos();
        if (listaEntidades == null) {
            return null;
        }
        return converterDados(listaEntidades);
    }

    /**
     * Realiza a extração de cada campo da entidade, inclui no Map e o retorna
     * no final. Exclui os campos com nome 'id'.
     * 
     * @param listaEntidades
     *            Lista com as Entidades Log a serem tratadas.
     * @return LinkedHashMap com os dados na ordem em que estão nas Entidades
     *         Log.
     */
    private static Map<String, String> converterDados(List<LogEntidadeVO> listaEntidades) {
        Map<String, String> map = new LinkedHashMap<String, String>();
        for (LogEntidadeVO entidade : listaEntidades) {
            if (entidade.getCampos() != null) {
                addNomeCampos(entidade.getCampos(), map);
            }
        }

        return map;
    }

    private static void addNomeCampos(List<LogCampoVO> log, Map<String, String> map) {
        for (LogCampoVO campo : log) {
            if (!"id".equals(campo.getNome())) {
                map.put(campo.getNome(), campo.getValor());
            }
        }
    }
}
