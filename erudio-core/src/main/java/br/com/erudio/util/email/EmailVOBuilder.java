package br.com.erudio.util.email;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import br.com.erudio.util.email.EmailVO.TipoEmailEnum;

public class EmailVOBuilder {

    private String para;
    private String de;
    private String titulo;
    private String conteudo;
    private Map<String, String> parametrosConteudo = new HashMap<>();
    private TipoEmailEnum tipoEmailEnum = TipoEmailEnum.SIMPLES;
    
    private Properties properties;
    
    private EmailVOBuilder(TipoEmailEnum tipoEmailEnum) {
        super();
        this.properties = new Properties();
        this.properties.setProperty("mail.smtp.host", "172.24.129.214");
        this.properties.setProperty("mail.smtp.port", "1025");
        this.tipoEmailEnum = tipoEmailEnum;
    }
    
    private EmailVOBuilder(EmailVOBuilder emailVOBuilder) {
        this.para = emailVOBuilder.para;
        this.de = emailVOBuilder.de;
        this.titulo = emailVOBuilder.titulo;
        this.conteudo = emailVOBuilder.conteudo;
        this.parametrosConteudo = new HashMap<>(emailVOBuilder.parametrosConteudo);
        this.tipoEmailEnum = emailVOBuilder.tipoEmailEnum;
        this.properties = new Properties(emailVOBuilder.properties);
    }

    public static EmailVOBuilder buildEmailTipoHTML() {
        return new EmailVOBuilder(TipoEmailEnum.HTML);
    }
    
    public static EmailVOBuilder buildEmailTipoSimples() {
        return new EmailVOBuilder(TipoEmailEnum.SIMPLES);
    }
    
    public static EmailVOBuilder buildEmailVOBuilder(EmailVOBuilder emailVOBuilder) {
        return new EmailVOBuilder(emailVOBuilder);
    }
    
    public EmailVOBuilder setRemetente(String remetente) {
        this.de = remetente;
        return new EmailVOBuilder(this);
    }
    
    public EmailVOBuilder setDestinatario(String destinatario) {
        this.para = destinatario;
        return new EmailVOBuilder(this);
    }
    
    public EmailVOBuilder setTitulo(String titulo) {
        this.titulo = titulo;
        return new EmailVOBuilder(this);
    }
    
    public EmailVOBuilder setConteudo(String conteudo) {
        this.conteudo = conteudo;
        return new EmailVOBuilder(this);
    }
    
    public EmailVOBuilder addParametroConteudo(String parametro, String valor) {
        parametrosConteudo.put(parametro, valor);
        return new EmailVOBuilder(this);
    }
    
    public EmailVOBuilder setTipoEmailSimples() {
        this.tipoEmailEnum = TipoEmailEnum.SIMPLES;
        return new EmailVOBuilder(this);
    }
    
    public EmailVOBuilder setTipoEmailHTML() {
        this.tipoEmailEnum = TipoEmailEnum.HTML;
        return new EmailVOBuilder(this);
    }
    
    private String buildConteudo() {
        String retorno = conteudo;
        for (String parametro : parametrosConteudo.keySet()) {
            retorno = retorno.replaceAll("<"+parametro+">", parametrosConteudo.get(parametro)); 
        }
        return retorno;
    }
    
    public EmailVO build() {
        String conteudoFinal = buildConteudo();
        return new EmailVO(para, de, titulo, conteudoFinal, tipoEmailEnum, properties);
    }
    
}
