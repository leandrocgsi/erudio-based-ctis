package br.com.erudio.util;

public final class Constante {
    
    public static final long UM = 1L;
    public static final long DOIS = 2L;
    public static final long TRES = 3L;
    public static final long QUATRO = 4L;
    public static final long CINCO = 5L;
    
    public static final String ATIVO = "ativo";
    public static final String AND = " and ";
    public static final String PERFIL_ACESSO_ID = "perfilAcesso.id";
    public static final String NOME_PESSOA = "nomePessoa";
    public static final String EMAIL_PESSOA = "emailPessoa";
    public static final String ID_USUARIO_EXTERNO = "idUsuarioExterno";
    public static final String SIGLA = "sigla";
    
    private Constante() {
        
    }
    
}
