package br.com.erudio.util.email;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import br.com.erudio.exception.NegocioException;

public class EmailUtil {

    private static final Logger LOGGER = Logger.getLogger(EmailUtil.class.getName());
    
    private EmailUtil () {
        
    }
    
    public static void enviar(EmailVO emailVO) {
        Session session = Session.getDefaultInstance(emailVO.properties);

        try {
            MimeMessage message = new MimeMessage(session);

            message.setFrom(new InternetAddress(emailVO.para));

            message.addRecipient(Message.RecipientType.TO, new InternetAddress(emailVO.de));

            message.setSubject(emailVO.titulo, "UTF-8");
            if (EmailVO.TipoEmailEnum.SIMPLES.equals(emailVO.tipoEmail)) {
                message.setText(emailVO.conteudo, "UTF-8");
            } else if (EmailVO.TipoEmailEnum.HTML.equals(emailVO.tipoEmail)) {
                message.setContent(emailVO.conteudo, emailVO.tipoEmail.valor+"; charset=UTF-8");
            }

            Transport.send(message);
        } catch (MessagingException mex) {
            LOGGER.log(Level.FINE, mex.getMessage(), mex);
            NegocioException.throwExceptionErro(mex.getMessage());
        }
    }
}
