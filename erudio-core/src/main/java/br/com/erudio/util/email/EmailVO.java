package br.com.erudio.util.email;

import java.util.Properties;

public class EmailVO {
    
    protected String para;
    protected String de;
    protected String titulo;
    protected String conteudo;
    protected TipoEmailEnum tipoEmail;
    
    protected Properties properties;
    
    protected EmailVO(String para, String de, String titulo, String conteudo, TipoEmailEnum tipoEmail, Properties properties) {
        super();
        this.para = para;
        this.de = de;
        this.titulo = titulo;
        this.conteudo = conteudo;
        this.tipoEmail = tipoEmail;
        this.properties = properties;
    }

    protected enum TipoEmailEnum {
        SIMPLES("text"),
        HTML("text/html");

        protected String valor;
        
        private TipoEmailEnum(String valor) {
            this.valor = valor;
        }
        
    }
    
}
