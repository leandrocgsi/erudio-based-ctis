package br.com.erudio.util;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

import br.com.erudio.exception.NegocioException;
import br.com.erudio.modelo.base.EntidadeBase;

public final class Util {

    private static final Logger LOGGER = Logger.getLogger(Util.class.getName());
    private static final int QUANTIDADE_DIGITOS = 16;
    
    private Util() {
    }

    /**
     * Método responsável por verificar se o objeto esta vazio e se o Id está
     * preenchido.
     * 
     * @param obj
     * @return
     */
    public static boolean isNotEmpty(EntidadeBase<?> obj) {
        if (obj == null) {
            return Boolean.FALSE;
        } else if (obj.getId() == null) {
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }
    
    public static String stringToMD5(String string) {
        MessageDigest messageDigest = null;
        try {
            messageDigest = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            LOGGER.log(Level.FINE, e.getMessage(), e);
            NegocioException.throwExceptionErro(e.getMessage());
        }
        messageDigest.update(string.getBytes(), 0, string.length());
        return new BigInteger(1, messageDigest.digest()).toString(QUANTIDADE_DIGITOS);
    }
    
    public static String toStringNullToEmpty(Object objeto) {
        if (objeto == null) {
            return "";
        } else {
            return objeto.toString();
        }
    }
    
    public static String toStringNullToNull(Object objeto) {
        if (objeto == null) {
            return null;
        } else {
            return objeto.toString();
        }
    }

}
