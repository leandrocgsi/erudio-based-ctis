package br.com.erudio.exception;

import javax.xml.bind.annotation.XmlAttribute;

public abstract class Mensagem {
	private String type;
	private String msg;

	public Mensagem() {
	}

	public Mensagem(String tipo, String msg) {
		type = tipo;
		this.msg = msg;
	}

	@XmlAttribute
	public String getType() {
		return type;
	}

	@XmlAttribute
	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
}