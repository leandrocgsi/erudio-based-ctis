package br.com.erudio.exception;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class NegocioException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	@XmlElement
	private final List<Mensagem> mensagens = new ArrayList();

	public NegocioException(Mensagem message) {
		mensagens.add(message);
	}

	public void add(Mensagem message) {
		mensagens.add(message);
	}

	public List<Mensagem> getMessages() {
		return mensagens;
	}

	public static void throwExceptionErro(String mensagem) {
		throw new NegocioException(new MensagemErro(mensagem));
	}

	public static void throwExceptionAlerta(String mensagem) {
		throw new NegocioException(new MensagemAlerta(mensagem));
	}

	public static <T> void throwExceptionErroIfNull(T objeto, String mensagem) {
		if (objeto == null) {
			throwExceptionErro(mensagem);
		}
	}

	public static <T> void throwExceptionAlertaIfNull(T objeto, String mensagem) {
		if (objeto == null) {
			throwExceptionAlerta(mensagem);
		}
	}
}