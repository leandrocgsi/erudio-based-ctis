package br.com.erudio.business;

import br.com.erudio.exception.NegocioException;
import br.com.erudio.util.Conversor;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

public abstract class NegocioBase {
	private Validator validator;
	@Inject
	private Conversor conversor;

	@PostConstruct
	public void initValidator() {
		validator = Validation.buildDefaultValidatorFactory().getValidator();
	}

	protected CriteriaBuilder getCriteriaBuilder() {
		return getEntityManager().getCriteriaBuilder();
	}

	protected <T extends Serializable> CriteriaQuery<T> getCriteriaQuery(Class<T> tipo) {
		return getCriteriaBuilder().createQuery(tipo);
	}

	protected Validator getValidator() {
		return validator;
	}

	protected <T> void validate(T objeto) {
		NegocioException.throwExceptionErroIfNull(objeto, "Erro ao realizar operação. Registro não pode ser nulo.");
	}

	protected <S, T> T converter(S entidade, Class<T> tipoVO, String... excludeFields) {
		return (T) conversor.converter(entidade, tipoVO, excludeFields);
	}

	protected <S, T> T converter(S from, Class<T> tipoVO) {
		return (T) conversor.converter(from, tipoVO);
	}

	public void setConversor(Conversor conversor) {
		this.conversor = conversor;
	}

	protected EntityManager getEntityManager() {
		throw new IllegalStateException("Entity manager não implementado.");
	}
}