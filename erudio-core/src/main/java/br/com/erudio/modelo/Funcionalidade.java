package br.com.erudio.modelo;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import br.com.erudio.modelo.base.EntidadeBase;
import br.com.erudio.modelo.conversor.ConversorSimNao;

@Entity
@Table(name = "FUNCIONALIDADE")
@TypeDef(name = "sim_nao", typeClass = ConversorSimNao.class)
public class Funcionalidade extends EntidadeBase<Integer> {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "SQ_FUNCIONALIDADE", nullable = false)
    private Integer id;

    @Column(name = "NM_FUNCIONALIDADE", nullable = false, length = 50)
    private String nome;

    @Column(name = "DS_FUNCIONALIDADE", nullable = false, length = 300)
    private String descricao;

    @Column(name = "ST_FUNCIONALIDADE_ATIVA", nullable = false, length = 1)
    @Type(type = "sim_nao")
    private Boolean ativa;

    @Column(name = "ST_FU_AD", nullable = false, length = 1)
    @Type(type = "sim_nao")
    private Boolean administrativa;

    @OneToMany(cascade=CascadeType.DETACH, fetch=FetchType.LAZY)
    @JoinTable(name = "FUNCIONALIDADE_ACESSO",
        joinColumns = @JoinColumn(name="SQ_FUNCIONALIDADE",referencedColumnName="SQ_FUNCIONALIDADE"),
            inverseJoinColumns = @JoinColumn(name="SQ_TIPO_ACESSO", referencedColumnName="SQ_TIPO_ACESSO"))
    private List<TipoAcesso> tiposAcesso;
    
    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Boolean getAtiva() {
        return ativa;
    }

    public void setAtiva(Boolean ativa) {
        this.ativa = ativa;
    }

    public Boolean getAdministrativa() {
        return administrativa;
    }

    public void setAdministrativa(Boolean administrativa) {
        this.administrativa = administrativa;
    }

    public List<TipoAcesso> getTiposAcesso() {
        return tiposAcesso;
    }

    public void setTiposAcesso(List<TipoAcesso> tiposAcesso) {
        this.tiposAcesso = tiposAcesso;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(this.id)
                .append(this.administrativa)
                .toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Funcionalidade == Boolean.FALSE) {
            return Boolean.FALSE;
        }
        if (this == obj) {
            return Boolean.TRUE;
        }

        final Funcionalidade otherObject = (Funcionalidade) obj;

        return new EqualsBuilder()
                .append(this.id, otherObject.id)
                .append(this.administrativa, otherObject.administrativa)
                .isEquals();
    }

}
