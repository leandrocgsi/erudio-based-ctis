package br.com.erudio.modelo.chavecomposta;

import java.io.Serializable;

import javax.persistence.Embeddable;

import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.EqualsBuilder;

@Embeddable
public class FuncionalidadeAcessoPerfilId implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer idFuncionalidade;

    private Integer idTipoAcesso;

    private Integer idPerfilAcesso;

    public Integer getIdFuncionalidade() {
        return idFuncionalidade;
    }

    public void setIdFuncionalidade(Integer idFuncionalidade) {
        this.idFuncionalidade = idFuncionalidade;
    }

    public Integer getIdTipoAcesso() {
        return idTipoAcesso;
    }

    public void setIdTipoAcesso(Integer idTipoAcesso) {
        this.idTipoAcesso = idTipoAcesso;
    }

    public Integer getIdPerfilAcesso() {
        return idPerfilAcesso;
    }

    public void setIdPerfilAcesso(Integer idPerfilAcesso) {
        this.idPerfilAcesso = idPerfilAcesso;
    }

    @Override
    public int hashCode() {

        return new HashCodeBuilder()
                .append(this.idFuncionalidade)
                .append(this.idPerfilAcesso)
                .append(this.idTipoAcesso)
                .toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof FuncionalidadeAcessoPerfilId == Boolean.FALSE) {
            return Boolean.FALSE;
        }
        if (this == obj) {
            return Boolean.TRUE;
        }

        final FuncionalidadeAcessoPerfilId otherObject = (FuncionalidadeAcessoPerfilId) obj;

        return new EqualsBuilder()
                .append(this.idFuncionalidade, otherObject.idFuncionalidade)
                .append(this.idPerfilAcesso, otherObject.idPerfilAcesso)
                .append(this.idTipoAcesso, otherObject.idTipoAcesso)
                .isEquals();
    }
}


