package br.com.erudio.modelo;

public interface ExclusaoLogica {

    Boolean getAtivo();
}
