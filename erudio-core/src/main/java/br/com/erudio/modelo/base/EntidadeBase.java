package br.com.erudio.modelo.base;

import java.io.Serializable;

import javax.persistence.MappedSuperclass;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

@MappedSuperclass
public abstract class EntidadeBase<K extends Serializable>
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  
  public abstract K getId();
  
  public boolean equals(Object obj)
  {
    if (this == obj)
    {
      return true;
    }
    
    if (obj == null)
    {
      return false;
    }
    
    if (getClass() != obj.getClass())
    {
      return false;
    }
    
    EntidadeBase<?> other = (EntidadeBase)obj;
    
    EqualsBuilder eb = new EqualsBuilder();
    
    eb.append(getId(), other.getId());
    
    return eb.isEquals();
  }
  




  public int hashCode()
  {
    return new HashCodeBuilder().append(getId()).toHashCode();
  }
}