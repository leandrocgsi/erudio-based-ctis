package br.com.erudio.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.erudio.modelo.base.EntidadeBase;

@Entity
@Table(name = "EMAIL_UNIDADE")
public class EmailUnidade extends EntidadeBase<Integer> {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "SQ_EMAIL")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "SQ_UNIDADE", nullable = false)
    private Unidade unidade;

    @Column(name = "NM_EMAIL_UNIDADE", nullable = false, length = 150)
    private String email;

    public EmailUnidade() {
        super();
    }

    public EmailUnidade(Integer id, String email) {
        super();
        this.id = id;
        this.email = email;
    }

    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Unidade getUnidade() {
        return unidade;
    }

    public void setUnidade(Unidade unidade) {
        this.unidade = unidade;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
