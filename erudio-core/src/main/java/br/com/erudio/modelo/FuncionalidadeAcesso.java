package br.com.erudio.modelo;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import br.com.erudio.modelo.base.EntidadeBase;
import br.com.erudio.modelo.chavecomposta.FuncionalidadeAcessoId;

@Entity
@Table(name="FUNCIONALIDADE_ACESSO")
public class FuncionalidadeAcesso extends EntidadeBase<FuncionalidadeAcessoId> {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private FuncionalidadeAcessoId id;

    @MapsId(value = "idFuncionalidade")
    @ManyToOne
    @JoinColumn(name = "SQ_FUNCIONALIDADE", nullable = false)
    private Funcionalidade funcionalidade;

    @MapsId(value = "idTipoAcesso")
    @ManyToOne
    @JoinColumn(name = "SQ_TIPO_ACESSO", nullable = false)
    private TipoAcesso tipoAcesso;

    @Override
    public FuncionalidadeAcessoId getId() {
        return id;
    }

    public void setId(FuncionalidadeAcessoId id) {
        this.id = id;
    }

    public Funcionalidade getFuncionalidade() {
        return funcionalidade;
    }

    public void setFuncionalidade(Funcionalidade funcionalidade) {
        this.funcionalidade = funcionalidade;
    }

    public TipoAcesso getTipoAcesso() {
        return tipoAcesso;
    }

    public void setTipoAcesso(TipoAcesso tipoAcesso) {
        this.tipoAcesso = tipoAcesso;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(this.id)
                .toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof FuncionalidadeAcesso == Boolean.FALSE) {
            return Boolean.FALSE;
        }
        if (this == obj) {
            return Boolean.TRUE;
        }

        final FuncionalidadeAcesso otherObject = (FuncionalidadeAcesso) obj;

        return new EqualsBuilder()
                .append(this.id, otherObject.id)
                .isEquals();
    }

}
