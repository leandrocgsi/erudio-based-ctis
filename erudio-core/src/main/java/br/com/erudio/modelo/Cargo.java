package br.com.erudio.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import br.com.erudio.modelo.base.EntidadeBase;

@Entity
@Table(name = "CARGO")
public class Cargo extends EntidadeBase<Integer> {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "SQ_CARGO")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "ID_TIPO_CARGO", nullable = false)
    private TipoCargo tipoCargo;

    @Column(name = "NM_CARGO", nullable = false, length = 50)
    private String nome;
    
    @OneToOne(mappedBy="cargo")
    private CargoComissao cargoComissao;

    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TipoCargo getTipoCargo() {
        return tipoCargo;
    }

    public void setTipoCargo(TipoCargo tipoCargo) {
        this.tipoCargo = tipoCargo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public CargoComissao getCargoComissao() {
        return cargoComissao;
    }

    public void setCargoComissao(CargoComissao cargoComissao) {
        this.cargoComissao = cargoComissao;
    }
    
}
