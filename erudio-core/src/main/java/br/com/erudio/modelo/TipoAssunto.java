package br.com.erudio.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import br.com.erudio.modelo.base.EntidadeBase;
import br.com.erudio.modelo.conversor.ConversorSimNao;

@Entity
@Table(name = "TIPO_ASSUNTO")
@TypeDef(name = "sim_nao", typeClass = ConversorSimNao.class)
public class TipoAssunto extends EntidadeBase<Integer> implements ExclusaoLogica{


    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "SQ_TIPO_ASSUNTO")
    private Integer id;

    @Column(name = "NM_TIPO_ASSUNTO", nullable = false, length = 50)
    private String nome;
    
    @Column(name = "ST_TIPO_ASSUNTO_ATIVO", nullable = false, length = 1)
    @Type(type = "sim_nao")
    private Boolean ativo;

    @Column(name = "DS_TIPO_ASSUNTO", nullable = false, length = 200)
    private String descricao;

    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @Override
    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

}
