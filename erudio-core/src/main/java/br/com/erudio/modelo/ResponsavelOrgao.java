package br.com.erudio.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import br.com.erudio.modelo.base.EntidadeBase;

@Entity
@Table(name = "RESPONSAVEL_ORGAO")
public class ResponsavelOrgao extends EntidadeBase<Integer> {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "SQ_RESPONSAVEL_ORGAO")
    private Integer id;
    
    @Column(name = "NM_RESPONSAVEL_ORGAO", length = 100)
    private String nome;
    
    @Column(name = "DS_FUNCAO", length = 30)
    private String funcao;
    
    @Column(name = "DS_CARGO")
    private String cargo;
    
    @OneToOne
    @JoinColumn(name = "SQ_ORGAO", nullable = false)
    private Orgao orgao;
    
    @ManyToOne
    @JoinColumn(name = "ID_PRONOME_TRATAMENTO", nullable=false)
    private PronomeTratamento pronomeTratamento;
    
    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getFuncao() {
        return funcao;
    }

    public void setFuncao(String funcao) {
        this.funcao = funcao;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public Orgao getOrgao() {
        return orgao;
    }

    public void setOrgao(Orgao orgao) {
        this.orgao = orgao;
    }

    public PronomeTratamento getPronomeTratamento() {
        return pronomeTratamento;
    }

    public void setPronomeTratamento(PronomeTratamento pronomeTratamento) {
        this.pronomeTratamento = pronomeTratamento;
    }
}