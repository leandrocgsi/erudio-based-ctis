package br.com.erudio.modelo.chavecomposta;

import java.io.Serializable;

import javax.persistence.Embeddable;

import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.EqualsBuilder;

@Embeddable
public class FuncionalidadeAcessoId implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer idFuncionalidade;

    private Integer idTipoAcesso;

    public Integer getIdFuncionalidade() {
        return idFuncionalidade;
    }

    public void setIdFuncionalidade(Integer idFuncionalidade) {
        this.idFuncionalidade = idFuncionalidade;
    }

    public Integer getIdTipoAcesso() {
        return idTipoAcesso;
    }

    public void setIdTipoAcesso(Integer idTipoAcesso) {
        this.idTipoAcesso = idTipoAcesso;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(this.idFuncionalidade)
                .append(this.idTipoAcesso)
                .toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof FuncionalidadeAcessoId == Boolean.FALSE) {
            return Boolean.FALSE;
        }
        if (this == obj) {
            return Boolean.TRUE;
        }

        final FuncionalidadeAcessoId otherObject = (FuncionalidadeAcessoId) obj;

        return new EqualsBuilder()
                .append(this.idFuncionalidade, otherObject.idFuncionalidade)
                .append(this.idTipoAcesso, otherObject.idTipoAcesso)
                .isEquals();
    }

}
