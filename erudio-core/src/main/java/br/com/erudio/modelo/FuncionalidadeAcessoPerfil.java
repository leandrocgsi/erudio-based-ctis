package br.com.erudio.modelo;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

import br.com.erudio.modelo.base.EntidadeBase;
import br.com.erudio.modelo.chavecomposta.FuncionalidadeAcessoPerfilId;

@Entity
@Table(name="FUNCIONALIDADE_ACESSO_PERFIL")
public class FuncionalidadeAcessoPerfil extends EntidadeBase<FuncionalidadeAcessoPerfilId> {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private FuncionalidadeAcessoPerfilId id;

    @MapsId(value = "idFuncionalidade")
    @ManyToOne
    @JoinColumn(name = "SQ_FUNCIONALIDADE", nullable = false)
    private Funcionalidade funcionalidade;

    @MapsId(value = "idTipoAcesso")
    @ManyToOne
    @JoinColumn(name = "SQ_TIPO_ACESSO", nullable = false)
    private TipoAcesso tipoAcesso;

    @MapsId(value = "idPerfilAcesso")
    @ManyToOne
    @JoinColumn(name = "SQ_PERFIL_ACESSO", nullable = false)
    private PerfilAcesso perfilAcesso;

    @Override
    public FuncionalidadeAcessoPerfilId getId() {
        return id;
    }

    public void setId(FuncionalidadeAcessoPerfilId id) {
        this.id = id;
    }

    public Funcionalidade getFuncionalidade() {
        return funcionalidade;
    }

    public void setFuncionalidade(Funcionalidade funcionalidade) {
        this.funcionalidade = funcionalidade;
    }

    public TipoAcesso getTipoAcesso() {
        return tipoAcesso;
    }

    public void setTipoAcesso(TipoAcesso tipoAcesso) {
        this.tipoAcesso = tipoAcesso;
    }

    public PerfilAcesso getPerfilAcesso() {
        return perfilAcesso;
    }

    public void setPerfilAcesso(PerfilAcesso perfilAcesso) {
        this.perfilAcesso = perfilAcesso;
    }

}
