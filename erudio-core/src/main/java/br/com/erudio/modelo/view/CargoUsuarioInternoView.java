package br.com.erudio.modelo.view;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

import br.com.erudio.modelo.base.EntidadeBase;
import br.com.erudio.modelo.Cargo;
import br.com.erudio.modelo.UsuarioInterno;
import br.com.erudio.modelo.view.chavecomposta.CargoUsuarioInternoViewId;

@Entity
@Table(name = "VW_CARGO_USUARIO_INTERNO")
public class CargoUsuarioInternoView extends EntidadeBase<CargoUsuarioInternoViewId> {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private CargoUsuarioInternoViewId id;

    @MapsId(value = "idUsuarioInterno")
    @ManyToOne
    @JoinColumn(name = "SQ_PESSOA", nullable = false)
    private UsuarioInterno usuarioInterno;

    @MapsId(value = "idCargo")
    @ManyToOne
    @JoinColumn(name = "SQ_CARGO", nullable = false)
    private Cargo cargo;

    @Override
    public CargoUsuarioInternoViewId getId() {
        return id;
    }

    public void setId(CargoUsuarioInternoViewId id) {
        this.id = id;
    }

    public UsuarioInterno getUsuarioInterno() {
        return usuarioInterno;
    }

    public void setUsuarioInterno(UsuarioInterno usuarioInterno) {
        this.usuarioInterno = usuarioInterno;
    }

    public Cargo getCargo() {
        return cargo;
    }

    public void setCargo(Cargo cargo) {
        this.cargo = cargo;
    }

}
