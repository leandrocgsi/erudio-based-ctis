package br.com.erudio.modelo;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import br.com.erudio.modelo.base.EntidadeBase;
import br.com.erudio.modelo.conversor.ConversorSimNao;

@Entity
@Table(name = "USUARIO")
@TypeDef(name = "sim_nao", typeClass = ConversorSimNao.class)
public class Usuario extends EntidadeBase<Integer> {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "SQ_PESSOA", nullable = false)
    private Integer id;

    @MapsId
    @OneToOne
    @JoinColumn(name = "SQ_PESSOA", nullable = false)
    private Pessoa pessoa;

    @ManyToOne (cascade= CascadeType.MERGE)
    @JoinColumn(name = "SQ_PERFIL", nullable = false)
    private PerfilAcesso perfil;

    @ManyToOne
    @JoinColumn(name = "ID_TIPO_USUARIO", nullable = false)
    private TipoUsuario tipoUsuario;

    @Column(name = "ST_USUARIO_ATIVO", nullable = false, length = 1)
    @Type(type = "sim_nao")
    private Boolean ativo;

    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Pessoa getPessoa() {
        return pessoa;
    }

    public void setPessoa(Pessoa pessoa) {
        this.pessoa = pessoa;
    }

    public PerfilAcesso getPerfil() {
        return perfil;
    }

    public void setPerfil(PerfilAcesso perfil) {
        this.perfil = perfil;
    }

    public TipoUsuario getTipoUsuario() {
        return tipoUsuario;
    }

    public void setTipoUsuario(TipoUsuario tipoUsuario) {
        this.tipoUsuario = tipoUsuario;
    }

    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }

}
