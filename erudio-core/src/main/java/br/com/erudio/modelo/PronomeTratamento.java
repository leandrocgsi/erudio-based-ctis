package br.com.erudio.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import br.com.erudio.modelo.base.EntidadeBase;

@Entity
@Table(name = "PRONOME_TRATAMENTO")
public class PronomeTratamento extends EntidadeBase<Integer> {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "ID_PRONOME_TRATAMENTO", nullable = false)
    private Integer id;

    @Column(name = "NM_PRONOME_TRATAMENTO", nullable = false, length = 20)
    private String nome;

    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

}