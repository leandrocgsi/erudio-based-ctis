package br.com.erudio.modelo;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import br.com.erudio.modelo.base.EntidadeBase;
import br.com.erudio.modelo.chavecomposta.AssuntoPalavraChaveId;
import br.com.erudio.modelo.conversor.ConversorSimNao;

@Entity
@Table(name = "ASSUNTO_PALAVRA_CHAVE")
@TypeDef(name = "sim_nao", typeClass = ConversorSimNao.class)
public class AssuntoPalavraChave extends EntidadeBase<AssuntoPalavraChaveId> implements ExclusaoLogica{

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private AssuntoPalavraChaveId id;

    @MapsId(value = "idPalavraChave")
    @ManyToOne
    @JoinColumn(name = "SQ_PALAVRA_CHAVE", nullable = false)
    private PalavraChave palavraChave;

    @MapsId(value = "idTipoAssunto")
    @ManyToOne
    @JoinColumn(name = "SQ_TIPO_ASSUNTO", nullable = false)
    private TipoAssunto tipoAssunto;

    @Column(name = "ST_ASSUNTO_PALAVRA_CHAVE_ATIVA", nullable = false, length = 1)
    @Type(type = "sim_nao")
    private Boolean ativo;

    @Override
    public AssuntoPalavraChaveId getId() {
        return id;
    }

    public void setId(AssuntoPalavraChaveId id) {
        this.id = id;
    }

    public PalavraChave getPalavraChave() {
        return palavraChave;
    }

    public void setPalavraChave(PalavraChave palavraChave) {
        this.palavraChave = palavraChave;
    }

    public TipoAssunto getTipoAssunto() {
        return tipoAssunto;
    }

    public void setTipoAssunto(TipoAssunto tipoAssunto) {
        this.tipoAssunto = tipoAssunto;
    }

    @Override
    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }

}
