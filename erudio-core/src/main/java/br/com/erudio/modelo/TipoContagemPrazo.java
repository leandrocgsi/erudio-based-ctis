package br.com.erudio.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import br.com.erudio.modelo.base.EntidadeBase;

@Entity
@Table(name = "TIPO_CONTAGEM_PRAZO")
public class TipoContagemPrazo extends EntidadeBase<Integer> {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ID_TIPO_CONTAGEM_PRAZO")
    private Integer id;

    @Column(name = "NM_TIPO_CONTAGEM_PRAZO", nullable = false, length = 100)
    private String nome;

    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

}