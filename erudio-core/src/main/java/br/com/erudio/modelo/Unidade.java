package br.com.erudio.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import br.com.erudio.modelo.base.EntidadeBase;
import br.com.erudio.modelo.conversor.ConversorSimNao;

@Entity
@Table(name = "UNIDADE")
@TypeDef(name = "sim_nao", typeClass = ConversorSimNao.class)
public class Unidade extends EntidadeBase<Integer> implements ExclusaoLogica{

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "SQ_UNIDADE")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "SQ_UNIDADE_PAI")
    private Unidade unidadePai;

    @Column(name = "SG_UNIDADE", nullable = false, length = 6)
    private String sigla;

    @Column(name = "NM_UNIDADE", nullable = false, length = 150)
    private String nome;

    @Column(name = "DS_OBSERVACAO", length = 300)
    private String observacao;

    @Column(name = "ST_UNIDADE_TRATA_MANIFESTACAO", nullable = false, length = 1)
    @Type(type = "sim_nao")
    private Boolean trataManifestacao;

    @Column(name = "ST_UNIDADE_ATIVA", nullable = false, length = 1)
    @Type(type = "sim_nao")
    private Boolean ativo;
    
    public Unidade() {
        super();
    }
    
    public Unidade(Integer id, String sigla, String nome, String observacao, Boolean trataManifestacao, Boolean ativo) {
        super();
        this.id = id;
        this.sigla = sigla;
        this.nome = nome;
        this.observacao = observacao;
        this.trataManifestacao = trataManifestacao;
        this.ativo = ativo;
    }

    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Unidade getUnidadePai() {
        return unidadePai;
    }

    public void setUnidadePai(Unidade unidadePai) {
        this.unidadePai = unidadePai;
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public Boolean getTrataManifestacao() {
        return trataManifestacao;
    }

    public void setTrataManifestacao(Boolean trataManifestacao) {
        this.trataManifestacao = trataManifestacao;
    }

    @Override
    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }

}
