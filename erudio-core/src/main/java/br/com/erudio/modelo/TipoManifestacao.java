package br.com.erudio.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import br.com.erudio.modelo.base.EntidadeBase;
import br.com.erudio.modelo.conversor.ConversorSimNao;

@Entity
@Table(name = "TIPO_MANIFESTACAO")
@TypeDef(name = "sim_nao", typeClass = ConversorSimNao.class)
public class TipoManifestacao extends EntidadeBase<Integer> implements ExclusaoLogica {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "SQ_TIPO_MANIFESTACAO", nullable = false)
    private Integer id;

    @Column(name = "NM_TIPO_MANIFESTACAO", nullable = false, length = 50)
    private String nome;

    @Column(name = "QT_PRAZO_TRATAMENTO_OUVIDORIA", nullable = false, columnDefinition = "numeric")
    private Integer prazoTratamentoOuvidoria;

    @Column(name = "QT_PRAZO_TRATAMENTO_UNIDADE", nullable = false, columnDefinition = "numeric")
    private Integer prazoTratamentoUnidade;

    @Column(name = "ST_PRORROGACAO_PRAZO", nullable = false)
    @Type(type = "sim_nao")
    private Boolean prorrogacaoPrazo;

    @Column(name = "QT_DIAS_PRORROGAVEIS", columnDefinition = "numeric")
    private Integer quantidadeDiasProrrogaveis;

    @ManyToOne
    @JoinColumn(name = "ID_TIPO_CONTAGEM_PRAZO", nullable = false)
    private TipoContagemPrazo tipoContagemPrazo;

    @ManyToOne
    @JoinColumn(name = "SQ_CATEGORIA_MANIFESTACAO", nullable = false)
    private CategoriaManifestacao categoriaManifestacao;

    @Column(name = "ST_TIPO_MANIFESTACAO_ATIVA", nullable = false, length = 1)
    @Type(type = "sim_nao")
    private Boolean ativo;

    public TipoManifestacao() {
        super();
    }

    public TipoManifestacao(Integer id, String nome) {
        super();
        this.id = id;
        this.nome = nome;

    }

    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getPrazoTratamentoOuvidoria() {
        return prazoTratamentoOuvidoria;
    }

    public void setPrazoTratamentoOuvidoria(Integer prazoTratamentoOuvidoria) {
        this.prazoTratamentoOuvidoria = prazoTratamentoOuvidoria;
    }

    public Integer getPrazoTratamentoUnidade() {
        return prazoTratamentoUnidade;
    }

    public void setPrazoTratamentoUnidade(Integer prazoTratamentoUnidade) {
        this.prazoTratamentoUnidade = prazoTratamentoUnidade;
    }

    public Boolean getProrrogacaoPrazo() {
        return prorrogacaoPrazo;
    }

    public void setProrrogacaoPrazo(Boolean prorrogacaoPrazo) {
        this.prorrogacaoPrazo = prorrogacaoPrazo;
    }

    public Integer getQuantidadeDiasProrrogaveis() {
        return quantidadeDiasProrrogaveis;
    }

    public void setQuantidadeDiasProrrogaveis(Integer quantidadeDiasProrrogaveis) {
        this.quantidadeDiasProrrogaveis = quantidadeDiasProrrogaveis;
    }

    public TipoContagemPrazo getTipoContagemPrazo() {
        return tipoContagemPrazo;
    }

    public void setTipoContagemPrazo(TipoContagemPrazo tipoContagemPrazo) {
        this.tipoContagemPrazo = tipoContagemPrazo;
    }

    public CategoriaManifestacao getCategoriaManifestacao() {
        return categoriaManifestacao;
    }

    public void setCategoriaManifestacao(CategoriaManifestacao categoriaManifestacao) {
        this.categoriaManifestacao = categoriaManifestacao;
    }

    @Override
    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }

}