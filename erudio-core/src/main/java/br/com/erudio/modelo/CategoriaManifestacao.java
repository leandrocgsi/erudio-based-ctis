package br.com.erudio.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import br.com.erudio.modelo.base.EntidadeBase;
import br.com.erudio.modelo.conversor.ConversorSimNao;

@Entity
@Table(name = "CATEGORIA_MANIFESTACAO")
@TypeDef(name = "sim_nao", typeClass = ConversorSimNao.class)
public class CategoriaManifestacao extends EntidadeBase<Integer> implements ExclusaoLogica {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "SQ_CATEGORIA_MANIFESTACAO")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "ST_CATEGORIA_MANIFESTACAO_ATIVA", nullable = false, length = 1)
    @Type(type = "sim_nao")
    private Boolean ativo;

    @Column(name = "NM_CATEGORIA_MANIFESTACAO", nullable = false, length = 50)
    private String nome;

    @Column(name = "DS_CATEGORIA_MANIFESTACAO", nullable = false, length = 200)
    private String descricao;

    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

}
