package br.com.erudio.modelo;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

import br.com.erudio.modelo.base.EntidadeBase;
import br.com.erudio.modelo.chavecomposta.CargoUsuarioInternoId;

@Entity
@Table(name = "CARGO_USUARIO_INTERNO")
public class CargoUsuarioInterno extends EntidadeBase<CargoUsuarioInternoId> {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private CargoUsuarioInternoId id;

    @MapsId(value = "idUsuarioInterno")
    @ManyToOne
    @JoinColumn(name = "SQ_PESSOA", nullable = false)
    private UsuarioInterno usuarioInterno;

    @MapsId(value = "idCargo")
    @ManyToOne
    @JoinColumn(name = "SQ_CARGO", nullable = false)
    private Cargo cargo;

    @Override
    public CargoUsuarioInternoId getId() {
        return id;
    }

    public void setId(CargoUsuarioInternoId id) {
        this.id = id;
    }

    public UsuarioInterno getUsuarioInterno() {
        return usuarioInterno;
    }

    public void setUsuarioInterno(UsuarioInterno usuarioInterno) {
        this.usuarioInterno = usuarioInterno;
    }

    public Cargo getCargo() {
        return cargo;
    }

    public void setCargo(Cargo cargo) {
        this.cargo = cargo;
    }

}
