package br.com.erudio.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import br.com.erudio.modelo.base.EntidadeBase;

@Entity
@Table(name = "TIPO_CARGO")
public class TipoCargo extends EntidadeBase<Integer> {

    private static final long serialVersionUID = 1L;
    
    public static final String TIPO_CARGO_ESTAGIARIO = "ESTAGIÁRIO";

    @Id
    @Column(name = "ID_TIPO_CARGO")
    private Integer id;

    @Column(name = "NM_TIPO_CARGO", nullable = false, length = 100)
    private String nome;

    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

}
