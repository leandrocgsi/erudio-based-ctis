package br.com.erudio.modelo.chavecomposta;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class AssuntoPalavraChaveId implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer idPalavraChave;

    private Integer idTipoAssunto;

    public Integer getIdPalavraChave() {
        return idPalavraChave;
    }

    public void setIdPalavraChave(Integer idPalavraChave) {
        this.idPalavraChave = idPalavraChave;
    }

    public Integer getIdTipoAssunto() {
        return idTipoAssunto;
    }

    public void setIdTipoAssunto(Integer idTipoAssunto) {
        this.idTipoAssunto = idTipoAssunto;
    }

}
