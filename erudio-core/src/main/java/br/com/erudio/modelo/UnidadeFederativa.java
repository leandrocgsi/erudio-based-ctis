package br.com.erudio.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import br.com.erudio.modelo.base.EntidadeBase;

@Entity
@Table(name = "UNIDADE_FEDERACAO")
public class UnidadeFederativa extends EntidadeBase<String> {

    private static final long serialVersionUID = 1L;

    public static final String DF = "DF";

    @Id
    @Column(name = "SG_UF", nullable = false, length = 2, columnDefinition = "char")
    private String id;

    @Column(name = "NM_UF", nullable = false, length = 25)
    private String nome;

    public UnidadeFederativa() {
    }

    public UnidadeFederativa(String id) {
        super();
        this.id = id;
    }

    @Override
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

}