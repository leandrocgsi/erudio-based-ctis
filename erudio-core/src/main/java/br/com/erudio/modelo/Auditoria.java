package br.com.erudio.modelo;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.com.erudio.modelo.base.EntidadeBase;

@Entity
@Table(name = "AUDITORIA")
public class Auditoria extends EntidadeBase<Integer> {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "SQ_AUDITORIA")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "SQ_FUNCIONALIDADE", nullable = false)
    private Funcionalidade funcionalidade;

    @ManyToOne
    @JoinColumn(name = "SQ_TIPO_ACESSO", nullable = false)
    private TipoAcesso tipoAcesso;

    @ManyToOne
    @JoinColumn(name = "SQ_PESSOA", nullable = false)
    private Usuario usuario;

    @Column(name = "NM_PERFIL", nullable = false, length = 50)
    private String nomePerfil;

    @Column(name = "DH_OPERACAO", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataOperacao;

    @Column(name = "TX_XML_AUDITORIA", nullable = false, columnDefinition="xml")
    private String xmlAuditoria;

    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Funcionalidade getFuncionalidade() {
        return funcionalidade;
    }

    public void setFuncionalidade(Funcionalidade funcionalidade) {
        this.funcionalidade = funcionalidade;
    }

    public TipoAcesso getTipoAcesso() {
        return tipoAcesso;
    }

    public void setTipoAcesso(TipoAcesso tipoAcesso) {
        this.tipoAcesso = tipoAcesso;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public String getNomePerfil() {
        return nomePerfil;
    }

    public void setNomePerfil(String nomePerfil) {
        this.nomePerfil = nomePerfil;
    }

    public Date getDataOperacao() {
        return dataOperacao;
    }

    public void setDataOperacao(Date dataOperacao) {
        this.dataOperacao = dataOperacao;
    }

    public String getXmlAuditoria() {
        return xmlAuditoria;
    }

    public void setXmlAuditoria(String xmlAuditoria) {
        this.xmlAuditoria = xmlAuditoria;
    }

}
