
package br.com.erudio.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import br.com.erudio.modelo.base.EntidadeBase;

@Entity
@Table(name = "TIPO_RELACIONAMENTO")
public class TipoRelacionamento extends EntidadeBase<Integer> {

    private static final long serialVersionUID = 1L;

    public static final Integer ID_SERVIDOR_DO_ERUDIO = 4;

    @Id
    @Column(name = "ID_TIPO_RELACIONAMENTO", nullable = false)
    private Integer id;

    @Column(name = "NM_TIPO_RELACIONAMENTO", nullable = false, length = 100)
    private String nome;

    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

}
