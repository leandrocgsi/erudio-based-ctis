package br.com.erudio.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import br.com.erudio.modelo.base.EntidadeBase;

@Entity
@Table(name = "USUARIO_INTERNO")
public class UsuarioInterno extends EntidadeBase<Integer> {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "SQ_PESSOA", nullable = false)
    private Integer id;

    @MapsId
    @OneToOne
    @JoinColumn(name = "SQ_PESSOA", nullable = false)
    private Usuario usuario;
    
    @ManyToOne
    @JoinColumn(name = "SQ_UNIDADE", nullable = false)
    private Unidade unidade;
    
    @Column(name = "NM_LOGIN_REDE", nullable = false, length = 150)
    private String loginRede;
    
    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
    
    public Unidade getUnidade() {
        return unidade;
    }

    public void setUnidade(Unidade unidade) {
        this.unidade = unidade;
    }

    public String getLoginRede() {
        return loginRede;
    }

    public void setLoginRede(String loginRede) {
        this.loginRede = loginRede;
    }

}
