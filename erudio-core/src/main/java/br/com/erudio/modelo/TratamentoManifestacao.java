package br.com.erudio.modelo;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.com.erudio.modelo.base.EntidadeBase;
import br.com.erudio.modelo.chavecomposta.TratamentoManifestacaoId;

@Entity
@Table(name = "TRATAMENTO_MANIFESTACAO")
public class TratamentoManifestacao extends EntidadeBase<TratamentoManifestacaoId> {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private TratamentoManifestacaoId id;

    @MapsId(value = "idManifestacao")
    @ManyToOne
    @JoinColumn(name = "SQ_MANIFESTACAO", nullable = false)
    private Manifestacao manifestacao;

    @MapsId(value = "idUsuarioInterno")
    @ManyToOne
    @JoinColumn(name = "SQ_PESSOA", nullable = false)
    private UsuarioInterno usuarioInterno;
    
    @MapsId(value = "idTipoPapelUsuarioInterno")
    @ManyToOne
    @JoinColumn(name = "ID_TIPO_PAPEL_PESSOA", nullable = false)
    private TipoPapelPessoa tipoPapelPessoa;
    
    @Column(name="DH_INCLUSAO_TRATAMENTO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataInclusao;

    @Override
    public TratamentoManifestacaoId getId() {
        return id;
    }

    public void setId(TratamentoManifestacaoId id) {
        this.id = id;
    }

    public Manifestacao getManifestacao() {
        return manifestacao;
    }

    public void setManifestacao(Manifestacao manifestacao) {
        this.manifestacao = manifestacao;
    }

    public UsuarioInterno getUsuarioInterno() {
        return usuarioInterno;
    }

    public void setUsuarioInterno(UsuarioInterno usuarioInterno) {
        this.usuarioInterno = usuarioInterno;
    }

    public Date getDataInclusao() {
        return dataInclusao;
    }

    public void setDataInclusao(Date dataInclusao) {
        this.dataInclusao = dataInclusao;
    }

    public TipoPapelPessoa getTipoPapelPessoa() {
        return tipoPapelPessoa;
    }

    public void setTipoPapelPessoa(TipoPapelPessoa tipoPapelPessoa) {
        this.tipoPapelPessoa = tipoPapelPessoa;
    }    
}