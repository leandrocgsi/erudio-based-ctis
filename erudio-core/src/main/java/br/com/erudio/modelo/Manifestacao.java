package br.com.erudio.modelo;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.annotations.TypeDef;

import br.com.erudio.modelo.base.EntidadeBase;
import br.com.erudio.modelo.conversor.ConversorSimNao;

@Entity
@Table(name = "MANIFESTACAO")
@TypeDef(name = "sim_nao", typeClass = ConversorSimNao.class)
public class Manifestacao extends EntidadeBase<Integer> {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "SQ_MANIFESTACAO", nullable = false)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "SQ_TIPO_MANIFESTACAO", nullable = false)
    private TipoManifestacao tipoManifestacao;

    @ManyToOne
    @JoinColumn(name = "ID_TIPO_CANAL_ENTRADA", nullable = false)
    private TipoCanalEntrada tipoCanalEntrada;
    
    @ManyToOne
    @JoinColumn(name = "ID_TIPO_RESPOSTA_MANIFESTACAO", nullable = false)
    private TipoRespostaManifestacao tipoRespostaManifestacao;
    
    @ManyToOne
    @JoinColumn(name = "ID_TIPO_RELACIONAMENTO", nullable = false)
    private TipoRelacionamento tipoRelacionamento;
    
    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "SQ_PESSOA_MANIFESTANTE", nullable = false)
    private Pessoa pessoaManifestante;

    @ManyToOne
    @JoinColumn(name = "SQ_PAIS", nullable = false)
    private Pais pais;
    
    @ManyToOne
    @JoinColumn(name = "SG_UF")
    private UnidadeFederativa uf;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "SQ_MANIFESTACAO_PAI")
    private Manifestacao manifestacaoPai;
    
    @ManyToOne
    @JoinColumn(name = "ID_TIPO_SITUACAO_MANIFESTACAO", nullable = false)
    private TipoSituacaoManifestacao tipoSituacaoManifestacao; 

    @Column(name = "NR_PROCESSO_JUDICIAL_TRAMITE", length = 25)
    private String numeroProcesso;

    @Size(min=30,max=10000)
    @Column(name = "DS_MANIFESTACAO", nullable = false, columnDefinition="text")
    private String descricao;
    
    @Column(name = "DS_LOGRADOURO", length=150)
    private String logradouro;
    
    @Column(name = "NR_ENDERECAMENTO_POSTAL", length=15)
    private String numeroPostal;
    
    @Column(name = "DS_BAIRRO", length=72)
    private String bairro;
    
    @Column(name = "DS_LOCALIDADE", length=60)
    private String localidade;
    
    @Column(name = "NR_ENDERECO", length=6, columnDefinition="char")
    private String numeroEndereco;
    
    @Column(name = "DS_COMPLEMENTO", length=100)
    private String complementoEndereco;

    @Column(name = "NM_SISTEMA_ORIGEM", nullable = false, length = 50)
    private String sistemaOrigem;

    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TipoManifestacao getTipoManifestacao() {
        return tipoManifestacao;
    }

    public void setTipoManifestacao(TipoManifestacao tipoManifestacao) {
        this.tipoManifestacao = tipoManifestacao;
    }

    public TipoCanalEntrada getTipoCanalEntrada() {
        return tipoCanalEntrada;
    }

    public void setTipoCanalEntrada(TipoCanalEntrada tipoCanalEntrada) {
        this.tipoCanalEntrada = tipoCanalEntrada;
    }

    public TipoRespostaManifestacao getTipoRespostaManifestacao() {
        return tipoRespostaManifestacao;
    }

    public void setTipoRespostaManifestacao(TipoRespostaManifestacao tipoRespostaManifestacao) {
        this.tipoRespostaManifestacao = tipoRespostaManifestacao;
    }

    public TipoRelacionamento getTipoRelacionamento() {
        return tipoRelacionamento;
    }

    public void setTipoRelacionamento(TipoRelacionamento tipoRelacionamento) {
        this.tipoRelacionamento = tipoRelacionamento;
    }

    public Pessoa getPessoaManifestante() {
        return pessoaManifestante;
    }

    public void setPessoaManifestante(Pessoa pessoaManifestante) {
        this.pessoaManifestante = pessoaManifestante;
    }

    public Pais getPais() {
        return pais;
    }

    public void setPais(Pais pais) {
        this.pais = pais;
    }

    public UnidadeFederativa getUf() {
        return uf;
    }

    public void setUf(UnidadeFederativa uf) {
        this.uf = uf;
    }

    public Manifestacao getManifestacaoPai() {
        return manifestacaoPai;
    }

    public void setManifestacaoPai(Manifestacao manifestacaoPai) {
        this.manifestacaoPai = manifestacaoPai;
    }

    public TipoSituacaoManifestacao getTipoSituacaoManifestacao() {
        return tipoSituacaoManifestacao;
    }

    public void setTipoSituacaoManifestacao(TipoSituacaoManifestacao tipoSituacaoManifestacao) {
        this.tipoSituacaoManifestacao = tipoSituacaoManifestacao;
    }

    public String getNumeroProcesso() {
        return numeroProcesso;
    }

    public void setNumeroProcesso(String numeroProcesso) {
        this.numeroProcesso = numeroProcesso;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public String getNumeroPostal() {
        return numeroPostal;
    }

    public void setNumeroPostal(String numeroPostal) {
        this.numeroPostal = numeroPostal;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getLocalidade() {
        return localidade;
    }

    public void setLocalidade(String localidade) {
        this.localidade = localidade;
    }

    public String getNumeroEndereco() {
        return numeroEndereco;
    }

    public void setNumeroEndereco(String numeroEndereco) {
        this.numeroEndereco = numeroEndereco;
    }

    public String getComplementoEndereco() {
        return complementoEndereco;
    }

    public void setComplementoEndereco(String complementoEndereco) {
        this.complementoEndereco = complementoEndereco;
    }

    public String getSistemaOrigem() {
        return sistemaOrigem;
    }

    public void setSistemaOrigem(String sistemaOrigem) {
        this.sistemaOrigem = sistemaOrigem;
    }
    
}
