package br.com.erudio.modelo.chavecomposta;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class CargoUsuarioInternoId implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer idUsuarioInterno;

    private Integer idCargo;

    public Integer getIdUsuarioInterno() {
        return idUsuarioInterno;
    }

    public void setIdUsuarioInterno(Integer idUsuarioInterno) {
        this.idUsuarioInterno = idUsuarioInterno;
    }

    public Integer getIdCargo() {
        return idCargo;
    }

    public void setIdCargo(Integer idCargo) {
        this.idCargo = idCargo;
    }

}
