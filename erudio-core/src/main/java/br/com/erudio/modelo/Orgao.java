package br.com.erudio.modelo;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import br.com.erudio.modelo.base.EntidadeBase;
import br.com.erudio.modelo.conversor.ConversorSimNao;

@Entity
@Table(name = "ORGAO")
@TypeDef(name = "sim_nao", typeClass = ConversorSimNao.class)
public class Orgao extends EntidadeBase<Integer> {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "SQ_ORGAO")
    private Integer id;
    
    @Column(name = "NM_ORGAO", nullable = false, length = 100)
    private String nome;
    
    @Column(name = "SG_ORGAO", nullable = false, length = 6)
    private String sigla;
    
    @Column(name = "NM_EMAIL_ORGAO", nullable = false, length = 150)
    private String email;
    
    @Column(name = "NR_TELEFONE_ORGAO", length = 11)
    private String telefone;
    
    @Column(name = "DS_OBSERVACAO", length = 300)
    private String observacao;
    
    @Column(name = "DS_ENCAMINHAMENTO")
    private String descricaoEncaminhamento;
    
    @Column(name = "NR_CEP", length = 8, columnDefinition="char")
    private String cep;
    
    @Column(name = "DS_LOGRADOURO", length = 150)
    private String logradouro;
    
    @Column(name = "DS_COMPLEMENTO", length = 100)
    private String complemento;
    
    @Column(name = "DS_BAIRRO", length = 72)
    private String bairro;
    
    @Column(name = "DS_LOCALIDADE", length = 60)
    private String localidade;
    
    @Column(name = "NR_ENDERECO", length = 6, columnDefinition="char")
    private String numeroEndereco;
    
    @Column(name = "ST_ORGAO_ATIVO", length = 1)
    @Type(type = "sim_nao")
    private Boolean ativo;
    
    @ManyToOne
    @JoinColumn(name = "SG_UF", nullable=false)
    private UnidadeFederativa uf;
    
    @ManyToOne(cascade={CascadeType.ALL})
    @JoinColumn(name="SQ_ORGAO_PAI")
    private Orgao orgaoPai;
    
    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public String getDescricaoEncaminhamento() {
        return descricaoEncaminhamento;
    }

    public void setDescricaoEncaminhamento(String descricaoEncaminhamento) {
        this.descricaoEncaminhamento = descricaoEncaminhamento;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getLocalidade() {
        return localidade;
    }

    public void setLocalidade(String localidade) {
        this.localidade = localidade;
    }

    public String getNumeroEndereco() {
        return numeroEndereco;
    }

    public void setNumeroEndereco(String numeroEndereco) {
        this.numeroEndereco = numeroEndereco;
    }

    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }

    public UnidadeFederativa getUf() {
        return uf;
    }

    public void setUf(UnidadeFederativa uf) {
        this.uf = uf;
    }

    public Orgao getOrgaoPai() {
        return orgaoPai;
    }

    public void setOrgaoPai(Orgao orgaoPai) {
        this.orgaoPai = orgaoPai;
    }
    
}