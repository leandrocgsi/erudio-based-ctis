package br.com.erudio.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import br.com.erudio.modelo.base.EntidadeBase;
import br.com.erudio.modelo.conversor.ConversorSimNao;

@Entity
@Table(name = "PAIS")
@TypeDef(name = "sim_nao", typeClass = ConversorSimNao.class)
public class Pais extends EntidadeBase<Integer> implements ExclusaoLogica{

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "SQ_PAIS", nullable = false)
    private Integer id;

    @Column(name = "NM_PAIS", nullable = false, length = 50)
    private String nome;

    @Column(name = "ST_PAIS_ATIVO", nullable = false)
    @Type(type = "sim_nao")
    private Boolean ativo;

    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @Override
    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }

}