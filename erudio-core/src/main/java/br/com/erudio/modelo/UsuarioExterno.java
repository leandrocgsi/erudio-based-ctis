package br.com.erudio.modelo;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.com.erudio.modelo.base.EntidadeBase;

@Entity
@Table(name = "USUARIO_EXTERNO")
public class UsuarioExterno extends EntidadeBase<Integer> {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "SQ_PESSOA", nullable = false)
    private Integer id;

    @MapsId
    @OneToOne
    @JoinColumn(name = "SQ_PESSOA", nullable = false)
    private Usuario usuario;

    @Column(name = "DH_CADASTRO", nullable = false)
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date dataCadastro;

    @Column(name = "DH_CONFIRMACAO_CADASTRO")
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date dataConfirmacaoCadastro;

    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Date getDataCadastro() {
        return dataCadastro;
    }

    public void setDataCadastro(Date dataCadastro) {
        this.dataCadastro = dataCadastro;
    }

    public Date getDataConfirmacaoCadastro() {
        return dataConfirmacaoCadastro;
    }

    public void setDataConfirmacaoCadastro(Date dataConfirmacaoCadastro) {
        this.dataConfirmacaoCadastro = dataConfirmacaoCadastro;
    }
}