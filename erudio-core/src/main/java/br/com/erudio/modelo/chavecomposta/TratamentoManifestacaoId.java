package br.com.erudio.modelo.chavecomposta;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class TratamentoManifestacaoId implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer idManifestacao;

    private Integer idUsuarioInterno;
    
    private Integer idTipoPapelUsuarioInterno;

    public Integer getIdManifestacao() {
        return idManifestacao;
    }

    public void setIdManifestacao(Integer idManifestacao) {
        this.idManifestacao = idManifestacao;
    }

    public Integer getIdUsuarioInterno() {
        return idUsuarioInterno;
    }

    public void setIdUsuarioInterno(Integer idUsuarioInterno) {
        this.idUsuarioInterno = idUsuarioInterno;
    }

    public Integer getIdTipoPapelUsuarioInterno() {
        return idTipoPapelUsuarioInterno;
    }

    public void setIdTipoPapelUsuarioInterno(Integer idTipoPapelUsuarioInterno) {
        this.idTipoPapelUsuarioInterno = idTipoPapelUsuarioInterno;
    }

}
