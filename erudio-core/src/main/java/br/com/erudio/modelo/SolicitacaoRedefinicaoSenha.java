package br.com.erudio.modelo;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import br.com.erudio.modelo.base.EntidadeBase;
import br.com.erudio.modelo.conversor.ConversorSimNao;

@Entity
@Table(name = "SOLICITACAO_REDEFINICAO_SENHA")
@TypeDef(name = "sim_nao", typeClass = ConversorSimNao.class)
public class SolicitacaoRedefinicaoSenha extends EntidadeBase<Integer> {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "SQ_SOLIC_REDEFINICAO_SENHA", nullable = false)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "SQ_PESSOA", nullable = false)
    private UsuarioExterno usuarioExterno;

    @Column(name = "DH_SOLICITACAO", nullable = false)
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date dataSolicitacao;

    @Column(name = "ST_SOLICITACAO_UTILIZADA", nullable = false, length = 1)
    @Type(type = "sim_nao")
    private Boolean utilizada;

    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public UsuarioExterno getUsuarioExterno() {
        return usuarioExterno;
    }

    public void setUsuarioExterno(UsuarioExterno usuarioExterno) {
        this.usuarioExterno = usuarioExterno;
    }

    public Date getDataSolicitacao() {
        return dataSolicitacao;
    }

    public void setDataSolicitacao(Date dataSolicitacao) {
        this.dataSolicitacao = dataSolicitacao;
    }

    public Boolean getUtilizada() {
        return utilizada;
    }

    public void setUtilizada(Boolean utilizada) {
        this.utilizada = utilizada;
    }

}
