package br.com.erudio.modelo;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import br.com.erudio.modelo.base.EntidadeBase;
import br.com.erudio.modelo.conversor.ConversorSimNao;

@Entity
@Table(name = "PERFIL_ACESSO")
@TypeDef(name = "sim_nao", typeClass = ConversorSimNao.class)
public class PerfilAcesso extends EntidadeBase<Integer> {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "SQ_PERFIL_ACESSO", nullable = false)
    private Integer id;

    @Column(name = "NM_PERFIL_ACESSO", nullable = false, length = 50)
    private String nome;

    @Column(name = "SG_PERFIL_ACESSO", nullable = false, length = 6)
    private String sigla;

    @Column(name = "DS_PERFIL_ACESSO", length = 300)
    private String descricao;

    @Column(name = "ST_PERFIL_ACESSO_ATIVO", nullable = false, length = 1)
    @Type(type = "sim_nao")
    private Boolean ativo;

    @OneToMany(mappedBy="perfilAcesso", cascade=CascadeType.DETACH, fetch=FetchType.LAZY)
    private List<FuncionalidadeAcessoPerfil> permissoes;
    
    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }

    public List<FuncionalidadeAcessoPerfil> getPermissoes() {
        return permissoes;
    }

    public void setPermissoes(List<FuncionalidadeAcessoPerfil> permissoes) {
        this.permissoes = permissoes;
    }

}