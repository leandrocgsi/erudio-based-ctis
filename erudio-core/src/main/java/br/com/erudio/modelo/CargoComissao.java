package br.com.erudio.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import br.com.erudio.modelo.base.EntidadeBase;
import br.com.erudio.modelo.conversor.ConversorSimNao;

@Entity
@Table(name = "CARGO_COMISSAO")
@TypeDef(name = "sim_nao", typeClass = ConversorSimNao.class)
public class CargoComissao extends EntidadeBase<Integer> {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "SQ_CARGO")
    private Integer id;

    @MapsId
    @OneToOne
    @JoinColumn(name = "SQ_CARGO", nullable = false)
    private Cargo cargo;

    @ManyToOne
    @JoinColumn(name = "SQ_UNIDADE", nullable = false)
    private Unidade unidade;

    @Column(name = "ST_CARGO_COMISSAO_DIRECAO", nullable = false, length = 1)
    @Type(type="sim_nao")
    private Boolean direcao;

    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Cargo getCargo() {
        return cargo;
    }

    public void setCargo(Cargo cargo) {
        this.cargo = cargo;
    }

    public Unidade getUnidade() {
        return unidade;
    }

    public void setUnidade(Unidade unidade) {
        this.unidade = unidade;
    }

    public Boolean getDirecao() {
        return direcao;
    }

    public void setDirecao(Boolean direcao) {
        this.direcao = direcao;
    }
}
