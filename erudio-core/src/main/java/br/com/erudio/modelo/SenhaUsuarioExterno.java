package br.com.erudio.modelo;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.com.erudio.modelo.base.EntidadeBase;

@Entity
@Table(name = "SENHA_USUARIO_EXTERNO")
public class SenhaUsuarioExterno extends EntidadeBase<Integer> {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "SQ_SENHA_USUARIO_EXTERNO", nullable = false)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "SQ_PESSOA", nullable = false)
    private UsuarioExterno usuarioExterno;

    @Column(name = "NM_SENHA_USUARIO_EXTERNO", nullable = false, length = 32)
    private String senha;

    @Column(name = "DH_CRIACAO_SENHA_USUARIO", nullable = false)
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date dataCriacao;

    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public UsuarioExterno getUsuarioExterno() {
        return usuarioExterno;
    }

    public void setUsuarioExterno(UsuarioExterno usuarioExterno) {
        this.usuarioExterno = usuarioExterno;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public Date getDataCriacao() {
        return dataCriacao;
    }

    public void setDataCriacao(Date dataCriacao) {
        this.dataCriacao = dataCriacao;
    }

}
