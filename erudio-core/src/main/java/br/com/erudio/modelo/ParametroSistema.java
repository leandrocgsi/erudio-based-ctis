package br.com.erudio.modelo;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.erudio.modelo.base.EntidadeBase;

@Entity
@Table(name = "PARAMETRO_SISTEMA")
public class ParametroSistema extends EntidadeBase<Integer> {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "SQ_PARAMETRO_SISTEMA", nullable = false)
    private Integer id;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "SQ_PARAMETRO_SISTEMA_PAI")
    private ParametroSistema paramentroPai;

    @Column(name = "NM_PARAMETRO_SISTEMA", nullable = false, length = 50)
    private String nome;

    @Column(name = "DS_PARAMETRO_SISTEMA", nullable = false, length = 2000)
    private String descricao;

    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ParametroSistema getParamentroPai() {
        return paramentroPai;
    }

    public void setParamentroPai(ParametroSistema paramentroPai) {
        this.paramentroPai = paramentroPai;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

}
