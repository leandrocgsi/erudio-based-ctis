package br.com.erudio.modelo.conversor;

import org.hibernate.type.BooleanType;
import org.hibernate.type.descriptor.java.BooleanTypeDescriptor;
import org.hibernate.type.descriptor.sql.CharTypeDescriptor;

public class ConversorSimNao extends BooleanType {

    private static final long serialVersionUID = 1L;

    public static final String TRUE = "S";
    public static final String FALSE = "N";
    

    public ConversorSimNao() {
        super(CharTypeDescriptor.INSTANCE, new BooleanTypeDescriptor(TRUE.charAt(0), FALSE.charAt(0)));
    }
}
