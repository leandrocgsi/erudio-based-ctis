package br.com.erudio.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import br.com.erudio.modelo.base.EntidadeBase;
import br.com.erudio.modelo.conversor.ConversorSimNao;

@Entity
@Table(name = "PESSOA")
@TypeDef(name = "sim_nao", typeClass = ConversorSimNao.class)
public class Pessoa extends EntidadeBase<Integer> {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "SQ_PESSOA")
    private Integer id;

    @Column(name = "NM_PESSOA", nullable = false, length = 150)
    private String nome;
    
    @Column(name = "NM_EMAIL", nullable = false, length = 150)
    private String email;
    
    @Column(name = "NM_PESSOA_RECEITA", length = 150)
    private String nomePessoaReceita;
    
    @Column(name = "NR_CNPJ", length = 14)
    private String cnpj;
    
    @Column(name = "NR_CPF", length = 11)
    private String cpf;
    
    @Column(name = "ST_PESSOA_USUARIO", nullable = false, length = 1)
    @Type(type="sim_nao")
    private Boolean usuario;
    
    @ManyToOne
    @JoinColumn(name = "SQ_PAIS", nullable=false)
    private Pais pais; 
    
    @ManyToOne
    @JoinColumn(name = "ID_TIPO_PESSOA", nullable=false)
    private TipoPessoa tipoPessoa;
    
    @ManyToOne
    @JoinColumn(name = "SG_UF")
    private UnidadeFederativa uf;
    
    @ManyToOne
    @JoinColumn(name = "SQ_GRAU_INSTRUCAO")
    private GrauInstrucao grauInstrucao;
    
    @ManyToOne
    @JoinColumn(name = "ID_PRONOME_TRATAMENTO")
    private PronomeTratamento pronomeTratamento;

    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNomePessoaReceita() {
        return nomePessoaReceita;
    }

    public void setNomePessoaReceita(String nomePessoaReceita) {
        this.nomePessoaReceita = nomePessoaReceita;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Boolean getUsuario() {
        return usuario;
    }

    public void setUsuario(Boolean usuario) {
        this.usuario = usuario;
    }

    public Pais getPais() {
        return pais;
    }

    public void setPais(Pais pais) {
        this.pais = pais;
    }

    public TipoPessoa getTipoPessoa() {
        return tipoPessoa;
    }

    public void setTipoPessoa(TipoPessoa tipoPessoa) {
        this.tipoPessoa = tipoPessoa;
    }

    public UnidadeFederativa getuf() {
        return uf;
    }

    public void setuf(UnidadeFederativa uf) {
        this.uf = uf;
    }

    public GrauInstrucao getGrauInstrucao() {
        return grauInstrucao;
    }

    public void setGrauInstrucao(GrauInstrucao grauInstrucao) {
        this.grauInstrucao = grauInstrucao;
    }

    public PronomeTratamento getPronomeTratamento() {
        return pronomeTratamento;
    }

    public void setPronomeTratamento(PronomeTratamento pronomeTratamento) {
        this.pronomeTratamento = pronomeTratamento;
    }
}