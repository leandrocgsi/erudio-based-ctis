package br.com.erudio.seguranca.vo;

import br.com.erudio.negocio.vo.base.BaseVO;

public class PerfilAutenticacaoVO extends BaseVO {

    private static final long serialVersionUID = 1L;

    private Integer id;
    
    private String nomePerfil;

    public PerfilAutenticacaoVO(){
        super();
    }
    
    public PerfilAutenticacaoVO(Integer id, String nomePerfil){
        this.id = id;
        this.nomePerfil = nomePerfil;
    }
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNomePerfil() {
        return nomePerfil;
    }

    public void setNomePerfil(String nomePerfil) {
        this.nomePerfil = nomePerfil;
    }
    
}
