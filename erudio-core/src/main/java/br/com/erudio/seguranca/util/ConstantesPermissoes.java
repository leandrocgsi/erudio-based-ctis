package br.com.erudio.seguranca.util;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class ConstantesPermissoes {
    
    private static final Logger LOGGER = Logger.getLogger(ConstantesPermissoes.class.getName());
    
    public final class ManterPais {
        public static final String NOME_FUNCIONALIDADE = "MANTER_PAIS";
        public static final String CONSULTAR_PAIS = NOME_FUNCIONALIDADE + ">CONSULTAR_PAIS";
        public static final String INCLUIR_PAIS = NOME_FUNCIONALIDADE + ">INCLUIR_PAIS";
        public static final String ALTERAR_PAIS = NOME_FUNCIONALIDADE + ">ALTERAR_PAIS";
        public static final String EXCLUIR_PAIS = NOME_FUNCIONALIDADE + ">EXCLUIR_PAIS";
        public static final String VISUALIZAR_PAIS = NOME_FUNCIONALIDADE + ">VISUALIZAR_PAIS";

        private ManterPais() {
        }
    }

    public final class GerenciarPerfil {
        public static final String NOME_FUNCIONALIDADE = "GERENCIAR_PERFIL";
        public static final String LISTAR_PERFIL = NOME_FUNCIONALIDADE + ">LISTAR_PERFIL";
        public static final String INCLUIR_PERFIL = NOME_FUNCIONALIDADE + ">INCLUIR_PERFIL";
        public static final String ALTERAR_PERFIL = NOME_FUNCIONALIDADE + ">ALTERAR_PERFIL";
        public static final String EXCLUIR_PERFIL = NOME_FUNCIONALIDADE + ">EXCLUIR_PERFIL";
        public static final String VISUALIZAR_PERFIL = NOME_FUNCIONALIDADE + ">VISUALIZAR_PERFIL";

        private GerenciarPerfil() {
        }
    }

    public final class AssociarUsuarioPerfil {
        public static final String NOME_FUNCIONALIDADE = "ASSOCIAR_USUARIO_PERFIL";
        public static final String ADICIONAR_ASSOCIACAO = NOME_FUNCIONALIDADE + ">ADICIONAR_ASSOCIACAO";
        public static final String EXCLUIR_ASSOCIACAO = NOME_FUNCIONALIDADE + ">EXCLUIR_ASSOCIACAO";

        private AssociarUsuarioPerfil() {
        }
    }

    public final class GerenciarUnidade {
        public static final String NOME_FUNCIONALIDADE = "GERENCIAR_UNIDADE";
        public static final String SELECIONAR_UNIDADE = NOME_FUNCIONALIDADE + ">SELECIONAR_UNIDADE";
        public static final String VISUALIZAR_UNIDADE = NOME_FUNCIONALIDADE + ">VISUALIZAR_UNIDADE";
        public static final String ALTERAR_UNIDADE = NOME_FUNCIONALIDADE + ">ALTERAR_UNIDADE";

        private GerenciarUnidade() {
        }
    }

    public final class ManterOrgao {
        public static final String NOME_FUNCIONALIDADE = "MANTER_ORGAO";
        public static final String CONSULTAR_ORGAO = NOME_FUNCIONALIDADE + ">CONSULTAR_ORGAO";
        public static final String INCLUIR_ORGAO = NOME_FUNCIONALIDADE + ">INCLUIR_ORGAO";
        public static final String ALTERAR_ORGAO = NOME_FUNCIONALIDADE + ">ALTERAR_ORGAO";
        public static final String EXCLUIR_ORGAO = NOME_FUNCIONALIDADE + ">EXCLUIR_ORGAO";
        public static final String VISUALIZAR_ORGAO = NOME_FUNCIONALIDADE + ">VISUALIZAR_ORGAO";
        public static final String INCLUIR_AREA = NOME_FUNCIONALIDADE + ">INCLUIR_AREA";
        public static final String ALTERAR_AREA = NOME_FUNCIONALIDADE + ">ALTERAR_AREA";
        public static final String EXCLUIR_AREA = NOME_FUNCIONALIDADE + ">EXCLUIR_AREA";
        public static final String VISUALIZAR_AREA = NOME_FUNCIONALIDADE + ">VISUALIZAR_AREA";

        private ManterOrgao() {
        }
    }

    public final class AssociarFuncionalidadePerfil {
        public static final String NOME_FUNCIONALIDADE = "ASSOCIAR_FUNCIONALIDADE_PERFIL";
        public static final String ASSOCIAR_FUNCIONALIDADE = NOME_FUNCIONALIDADE + ">ASSOCIAR_FUNCIONALIDADE";
        public static final String DESASSOCIAR_FUNCIONALIDADE = NOME_FUNCIONALIDADE + ">DESASSOCIAR_FUNCIONALIDADE";

        private AssociarFuncionalidadePerfil() {
        }
    }

    public final class GerenciarParametros {
        public static final String NOME_FUNCIONALIDADE = "GERENCIAR_PARAMETROS";
        public static final String GERENCIAR_PARAMETROS = NOME_FUNCIONALIDADE + ">GERENCIAR_PARAMETROS";

        private GerenciarParametros() {
        }
    }

    public final class ManterAssunto {
        public static final String NOME_FUNCIONALIDADE = "MANTER_ASSUNTO";
        public static final String CONSULTAR_ASSUNTO = NOME_FUNCIONALIDADE + ">CONSULTAR_ASSUNTO";
        public static final String INCLUIR_ASSUNTO = NOME_FUNCIONALIDADE + ">INCLUIR_ASSUNTO";
        public static final String ALTERAR_ASSUNTO = NOME_FUNCIONALIDADE + ">ALTERAR_ASSUNTO";
        public static final String EXCLUIR_ASSUNTO = NOME_FUNCIONALIDADE + ">EXCLUIR_ASSUNTO";
        public static final String VISUALIZAR_ASSUNTO = NOME_FUNCIONALIDADE + ">VISUALIZAR_ASSUNTO";

        private ManterAssunto() {
        }
    }

    public final class ManterBancoResposta {
        public static final String NOME_FUNCIONALIDADE = "MANTER_BANCO_RESPOSTA";
        public static final String CONSULTAR_BANCO_RESPOSTAS = NOME_FUNCIONALIDADE + ">CONSULTAR_BANCO_RESPOSTAS";
        public static final String INCLUIR_RESPOSTA = NOME_FUNCIONALIDADE + ">INCLUIR_RESPOSTA";
        public static final String ALTERAR_RESPOSTA = NOME_FUNCIONALIDADE + ">ALTERAR_RESPOSTA";
        public static final String EXCLUIR_RESPOSTA = NOME_FUNCIONALIDADE + ">EXCLUIR_RESPOSTA";
        public static final String VISUALIZAR_RESPOSTA = NOME_FUNCIONALIDADE + ">VISUALIZAR_RESPOSTA";

        private ManterBancoResposta() {
        }
    }

    public final class ManterPalavraChave {
        public static final String NOME_FUNCIONALIDADE = "MANTER_PALAVRA_CHAVE";
        public static final String CONSULTAR_PALAVRA_CHAVE = NOME_FUNCIONALIDADE + ">CONSULTAR_PALAVRA_CHAVE";
        public static final String INCLUIR_PALAVRA_CHAVE = NOME_FUNCIONALIDADE + ">INCLUIR_PALAVRA_CHAVE";
        public static final String ALTERAR_PALAVRA_CHAVE = NOME_FUNCIONALIDADE + ">ALTERAR_PALAVRA_CHAVE";
        public static final String EXCLUIR_PALAVRA_CHAVE = NOME_FUNCIONALIDADE + ">EXCLUIR_PALAVRA_CHAVE";
        public static final String VISUALIZAR_PALAVRA_CHAVE = NOME_FUNCIONALIDADE + ">VISUALIZAR_PALAVRA_CHAVE";

        private ManterPalavraChave() {
        }
    }

    public final class ManterCategoriaManifestacao {
        public static final String NOME_FUNCIONALIDADE = "MANTER_CATEGORIA_MANIFESTACAO";
        public static final String CONSULTAR_CATEGORIAS = NOME_FUNCIONALIDADE + ">CONSULTAR_CATEGORIAS";
        public static final String INCLUIR_CATEGORIA = NOME_FUNCIONALIDADE + ">INCLUIR_CATEGORIA";
        public static final String ALTERAR_CATEGORIA = NOME_FUNCIONALIDADE + ">ALTERAR_CATEGORIA";
        public static final String EXCLUIR_CATEGORIA = NOME_FUNCIONALIDADE + ">EXCLUIR_CATEGORIA";
        public static final String VISUALIZAR_CATEGORIA = NOME_FUNCIONALIDADE + ">VISUALIZAR_CATEGORIA";

        private ManterCategoriaManifestacao() {
        }
    }

    public final class ManterTipoManifestacao {
        public static final String NOME_FUNCIONALIDADE = "MANTER_TIPO_MANIFESTACAO";
        public static final String CONSULTAR_TIPOS = NOME_FUNCIONALIDADE + ">CONSULTAR_TIPOS";
        public static final String INCLUIR_TIPO = NOME_FUNCIONALIDADE + ">INCLUIR_TIPO";
        public static final String ALTERAR_TIPO = NOME_FUNCIONALIDADE + ">ALTERAR_TIPO";
        public static final String EXCLUIR_TIPO = NOME_FUNCIONALIDADE + ">EXCLUIR_TIPO";
        public static final String VISUALIZAR_TIPO = NOME_FUNCIONALIDADE + ">VISUALIZAR_TIPO";

        private ManterTipoManifestacao() {
        }
    }

    public final class ManterManifestante {
        public static final String NOME_FUNCIONALIDADE = "MANTER_MANIFESTANTE";
        public static final String CONSULTAR_MANIFESTANTE = NOME_FUNCIONALIDADE + ">CONSULTAR_MANIFESTANTE";
        public static final String INCLUIR_MANIFESTANTE = NOME_FUNCIONALIDADE + ">INCLUIR_MANIFESTANTE";
        public static final String ALTERAR_MANIFESTANTE = NOME_FUNCIONALIDADE + ">ALTERAR_MANIFESTANTE";
        public static final String EXCLUIR_MANIFESTANTE = NOME_FUNCIONALIDADE + ">EXCLUIR_MANIFESTANTE";
        public static final String VISUALIZAR_MANIFESTANTE = NOME_FUNCIONALIDADE + ">VISUALIZAR_MANIFESTANTE";

        private ManterManifestante() {
        }
    }

    public final class ConsultarManifestacao {
        public static final String NOME_FUNCIONALIDADE = "CONSULTAR_MANIFESTACAO";
        public static final String DETALHAR_MANIFESTACAO = NOME_FUNCIONALIDADE + ">DETALHAR_MANIFESTACAO";
        public static final String APRESENTAR_RECURSO = NOME_FUNCIONALIDADE + ">APRESENTAR_RECURSO";

        private ConsultarManifestacao() {
        }
    }

    public final class CadastrarManifestacao {
        public static final String NOME_FUNCIONALIDADE = "CADASTRAR_MANIFESTACAO";
        public static final String CADASTRAR_MANIFESTACAO = NOME_FUNCIONALIDADE + ">CADASTRAR_MANIFESTACAO";

        private CadastrarManifestacao() {
        }
    }

    public final class AtualizarCadastro {
        public static final String NOME_FUNCIONALIDADE = "ATUALIZAR_CADASTRO";
        public static final String ATUALIZAR_CADASTRO = NOME_FUNCIONALIDADE + ">ATUALIZAR_CADASTRO";

        private AtualizarCadastro() {
        }
    }
    
    public static final Map<String, Object> TODAS_PERMISSOES = new HashMap<>();
    
    static {
        Class[] permissoesClasses = ConstantesPermissoes.class.getDeclaredClasses();
        for (Class pemissaoClass : permissoesClasses) {
            Map<String, String> parmissoesMap = new HashMap<>();
            Field[] permissoes = pemissaoClass.getDeclaredFields();
            for (Field permissao : permissoes) {
                if (Modifier.isPublic(permissao.getModifiers()) && 
                        Modifier.isStatic(permissao.getModifiers()) &&
                        Modifier.isFinal(permissao.getModifiers())) {
                    try {
                        String nome = permissao.getName();
                        if ("NOME_FUNCIONALIDADE".equals(nome)) {
                            nome = "NOME";
                        }
                        parmissoesMap.put(nome, permissao.get(null).toString());
                    } catch (IllegalArgumentException | IllegalAccessException e) {
                        LOGGER.log(Level.FINE, e.getMessage(), e);
                    }
                }
            }
            TODAS_PERMISSOES.put(parmissoesMap.get("NOME"), parmissoesMap);
        }
    }
}
