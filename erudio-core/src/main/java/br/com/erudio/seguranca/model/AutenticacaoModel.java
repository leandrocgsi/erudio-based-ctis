package br.com.erudio.seguranca.model;

import static br.com.erudio.seguranca.util.PermissoesUtil.montarNomeRole;
import static org.picketlink.idm.IDMMessages.MESSAGES;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.picketlink.idm.IdentityManagementException;
import org.picketlink.idm.IdentityManager;
import org.picketlink.idm.RelationshipManager;
import org.picketlink.idm.model.basic.BasicModel;
import org.picketlink.idm.model.basic.Grant;
import org.picketlink.idm.model.basic.GroupMembership;
import org.picketlink.idm.model.basic.Role;
import org.picketlink.idm.query.IdentityQuery;
import org.picketlink.idm.query.IdentityQueryBuilder;
import org.picketlink.idm.query.RelationshipQuery;

import br.com.erudio.modelo.FuncionalidadeAcessoPerfil;

public class AutenticacaoModel extends BasicModel {

    private static final String CARACTERE_BARRA = "/";
    
    private static String CAMINHO = null;
    
    public static UsuarioAutenticacao getUsuario(IdentityManager identityManager, String loginName) {
        if (identityManager == null) {
            throw MESSAGES.nullArgument("IdentityManager");
        }

        if (StringUtils.isEmpty(loginName)) {
            return null;
        }

        IdentityQueryBuilder queryBuilder = identityManager.getQueryBuilder();
        List<UsuarioAutenticacao> agents = queryBuilder.createIdentityQuery(UsuarioAutenticacao.class)
                .where(queryBuilder.equal(UsuarioAutenticacao.NOME_USUARIO, loginName)).getResultList();

        if (agents.isEmpty()) {
            return null;
        } else if (agents.size() == 1) {
            return agents.get(0);
        } else {
            throw new IdentityManagementException("Error - multiple Agent objects found with same login name");
        }
    }
    
    public static PerfilAutenticacao getPerfilAutenticacao(IdentityManager identityManager, String groupPath) {
        if (identityManager == null) {
            throw MESSAGES.nullArgument("IdentityManager");
        }
        verificaCaminho(groupPath);

        PerfilAutenticacao group = null;
        String[] paths = CAMINHO.split(CARACTERE_BARRA);

        if (paths.length > 0) {
            String name = paths[paths.length - 1];
            IdentityQueryBuilder queryBuilder = identityManager.getQueryBuilder();
            IdentityQuery<PerfilAutenticacao> query = queryBuilder.createIdentityQuery(PerfilAutenticacao.class)
                .where(queryBuilder.equal(PerfilAutenticacao.NAME, name));

            List<PerfilAutenticacao> result = query.getResultList();

            for (PerfilAutenticacao storedGroup : result) {
                if (storedGroup.getPath().equals(CAMINHO)) {
                    return storedGroup;
                }

                if (storedGroup.getPath().endsWith(CAMINHO)) {
                    group = storedGroup;
                }
            }
        }

        return group;
    }

    private static String verificaCaminho(String groupPath) {        
        if (StringUtils.isEmpty(groupPath)) {
            return null;
        }

        if (!groupPath.startsWith(CARACTERE_BARRA)) {
            CAMINHO = CARACTERE_BARRA + groupPath;
        }else{
            CAMINHO = groupPath;
        }
        return CAMINHO;
    }
    
    public static void vincularPerfilFuncionalidade(PerfilAutenticacao perfilAutenticacao,
            List<FuncionalidadeAcessoPerfil> permissoes, IdentityManager identityManager){
        
        if(permissoes == null || permissoes.isEmpty()){
            return;
        }
        
        for (FuncionalidadeAcessoPerfil fap : permissoes) {
            perfilAutenticacao.addRole(criarRole(fap, identityManager));
        }
        
        identityManager.update(perfilAutenticacao);
    }
    
    public static Role criarRole(String roleName, IdentityManager identityManager) {
        Role role = AutenticacaoModel.getRole(identityManager, roleName);
        if (role == null) {
            role = new Role(roleName);
            identityManager.add(role);
        }
        return role;
    }
    
    public static List<Grant> getGrantsUsuario(RelationshipManager relationshipManager, UsuarioAutenticacao usuario){
        RelationshipQuery<Grant> query = relationshipManager.createRelationshipQuery(Grant.class);
        query.setParameter(Grant.ASSIGNEE, usuario);
        
        return query.getResultList();
    }
    
    
    public static void revogarRolesDoUsuario(RelationshipManager relationshipManager, UsuarioAutenticacao usuario){
        List<Grant> result = getGrantsUsuario(relationshipManager, usuario);
        
        for (Grant grant : result) {
            AutenticacaoModel.revokeRole(relationshipManager, usuario, grant.getRole());
        }
    }
    
    public static void removerPerilsUsuario(RelationshipManager relationshipManager, UsuarioAutenticacao usuario){
        List<GroupMembership> relacoes = getPerfisUsuario(relationshipManager, usuario);
        if(relacoes != null){
            for (GroupMembership relacao : relacoes) {
                removeFromGroup(relationshipManager, usuario, relacao.getGroup());
            }
        }
    }

    public static List<GroupMembership> getPerfisUsuario(RelationshipManager relationshipManager, UsuarioAutenticacao usuario){
        RelationshipQuery<GroupMembership> query = relationshipManager.createRelationshipQuery(GroupMembership.class);
        query.setParameter(GroupMembership.MEMBER, usuario);
        return query.getResultList();
    }
    
    public static List<GroupMembership> getMembrosLogadosPerfil(PerfilAutenticacao perfilAutenticacao,
            RelationshipManager relationshipManager){
        RelationshipQuery<GroupMembership> query = relationshipManager.createRelationshipQuery(GroupMembership.class);
        query.setParameter(GroupMembership.GROUP, perfilAutenticacao);
        return query.getResultList();
    }
    
    public static PerfilAutenticacao criarPerfilAutenticacao(IdentityManager identityManager, String nomePerfil){
        PerfilAutenticacao perfil = new PerfilAutenticacao(nomePerfil);
        identityManager.add(perfil);
        return perfil;
    }
    
    private static Role criarRole(FuncionalidadeAcessoPerfil fap, IdentityManager identityManager){
        return criarRole(montarNomeRole(fap.getFuncionalidade().getNome(),
                fap.getTipoAcesso().getNome()),
                identityManager);
    }
    
}