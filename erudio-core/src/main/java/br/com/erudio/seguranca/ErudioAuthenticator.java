package br.com.erudio.seguranca;

import static org.picketlink.log.BaseLog.AUTHENTICATION_LOGGER;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import org.picketlink.annotations.PicketLink;
import org.picketlink.authentication.BaseAuthenticator;
import org.picketlink.authentication.CredentialExpiredException;
import org.picketlink.authentication.LockedAccountException;
import org.picketlink.authentication.UnexpectedCredentialException;
import org.picketlink.credential.DefaultLoginCredentials;
import org.picketlink.idm.IdentityManager;
import org.picketlink.idm.credential.Credentials;
import org.picketlink.idm.credential.Password;
import org.picketlink.idm.credential.UsernamePasswordCredentials;

import br.com.erudio.seguranca.model.UsuarioAutenticacao;
import br.com.erudio.util.Util;

@RequestScoped
@PicketLink
public class ErudioAuthenticator extends BaseAuthenticator {

    @Inject
    private DefaultLoginCredentials credentials;

    @Inject
    private AutenticacaoModelManager autenticacaoModelManager;

    @Inject
    private Instance<IdentityManager> identityManager;

    @Override
    public void authenticate() {

        if (credentials.getCredential() == null) {
            return;
        }

        Credentials creds;

        if (this.isUsernamePasswordCredential()) {

            inicializarContexoUsuario();

            String senhaInformada = new String(((Password) credentials.getCredential()).getValue());
            Password passwordInformado = new Password(Util.stringToMD5(senhaInformada));

            creds = new UsernamePasswordCredentials(credentials.getUserId(), passwordInformado);
        } else if (isCustomCredential()) {
            creds = (Credentials) credentials.getCredential();
        } else {
            throw new UnexpectedCredentialException("Unsupported credential type [" + credentials.getCredential() + "].");
        }

        if (AUTHENTICATION_LOGGER.isDebugEnabled()) {
            AUTHENTICATION_LOGGER.debugf("Validating credentials [%s] using PicketLink IDM.", creds);
        }

        identityManager.get().validateCredentials(creds);

        this.credentials.setStatus(creds.getStatus());
        this.credentials.setValidatedAccount(creds.getValidatedAccount());

        if (AUTHENTICATION_LOGGER.isDebugEnabled()) {
            AUTHENTICATION_LOGGER.debugf("Credential status is [%s] and validated account [%s]", this.credentials.getStatus(), this.credentials.getValidatedAccount());
        }

        validaCredencial(creds);

    }

    private void validaCredencial(Credentials creds) {
        if (Credentials.Status.VALID.equals(creds.getStatus())) {
            setStatus(AuthenticationStatus.SUCCESS);
            setAccount(creds.getValidatedAccount());
        } else if (Credentials.Status.ACCOUNT_DISABLED.equals(creds.getStatus())) {
            throw new LockedAccountException("Account [" + this.credentials.getUserId() + "] is disabled.");
        } else if (Credentials.Status.EXPIRED.equals(creds.getStatus())) {
            throw new CredentialExpiredException("Credential is expired for Account [" + this.credentials.getUserId() + "].");
        }
    }

    /**
     * Passa o nome de usuario ao ModelManager para que seja recuperado do banco
     * ou via LDAP e adicionado ao contexto do picketlink
     * 
     * @param loginName
     */
    private UsuarioAutenticacao inicializarContexoUsuario() {
        return autenticacaoModelManager.inicializarUsuario(credentials.getUserId(), credentials.getPassword());
    }

    private boolean isCustomCredential() {
        return Credentials.class.isInstance(credentials.getCredential());
    }

    private boolean isUsernamePasswordCredential() {
        return Password.class.equals(credentials.getCredential().getClass()) && credentials.getUserId() != null;
    }

}
