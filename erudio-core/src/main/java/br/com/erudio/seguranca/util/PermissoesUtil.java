package br.com.erudio.seguranca.util;

public class PermissoesUtil {
    
    private PermissoesUtil(){
        
    }
    public static final String montarNomeRole(String nomeFuncionalidade, String nomeTipoAcesso){
        return nomeFuncionalidade + ">" + nomeTipoAcesso;
    }
    
}
