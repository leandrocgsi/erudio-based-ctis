package br.com.erudio.seguranca;

import static br.com.erudio.seguranca.util.PermissoesUtil.montarNomeRole;

import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import org.picketlink.event.PartitionManagerCreateEvent;
import org.picketlink.idm.IdentityManager;
import org.picketlink.idm.PartitionManager;
import org.picketlink.idm.model.Attribute;
import org.picketlink.idm.model.basic.Realm;

import br.com.erudio.modelo.Funcionalidade;
import br.com.erudio.modelo.PerfilAcesso;
import br.com.erudio.modelo.TipoAcesso;
import br.com.erudio.negocio.leitura.FuncionalidadeNegocioLeitura;
import br.com.erudio.negocio.leitura.PerfilAcessoNegocioLeitura;
import br.com.erudio.seguranca.model.AutenticacaoModel;
import br.com.erudio.seguranca.model.PerfilAutenticacao;;

@Stateless
public class SecurityInitializer {

    public static final String KEYSTORE_FILE_PATH = "/keystore.jks";
    /**
    private static final String KEYSTORE_PASS = "KeyStorePass159753@erudio.com.br";
    */
    private static final Logger LOGGER = Logger.getLogger(SecurityInitializer.class.getName());

    @Inject
    private FuncionalidadeNegocioLeitura funcionalidadeNegocio;
    
    @Inject
    private PerfilAcessoNegocioLeitura perfilAcessoNegocio;
    
    public void configureDefaultPartition(@Observes PartitionManagerCreateEvent event) {
        PartitionManager partitionManager = event.getPartitionManager();
        criarParticaoPadrao(partitionManager);
        criarRolesPadrao(partitionManager);
        criarPerfis(partitionManager);
    }

    private void criarParticaoPadrao(PartitionManager partitionManager) {
        Realm partition = partitionManager.getPartition(Realm.class, Realm.DEFAULT_REALM);
        
        if (partition == null) {
            partition = new Realm(Realm.DEFAULT_REALM);
            configureKeys(partition);
            partitionManager.add(partition);
        }
    }

    private void criarRolesPadrao(PartitionManager partitionManager) {
        List<Funcionalidade> funcionalidades = funcionalidadeNegocio.listarFuncionalidadesAtivas();
        
        if(funcionalidades != null){
            for (Funcionalidade funcionalidade : funcionalidades) {
                criarRolesFuncionalidade(funcionalidade, partitionManager);
            }
        }
    }

    private void criarPerfis(PartitionManager partitionManager){
        List<PerfilAcesso> perfis = perfilAcessoNegocio.listarPerfisAtivosComFuncionalidades();
        if(perfis != null){
            for (PerfilAcesso perfilAcesso : perfis) {
                criarConfigurarPerfil(perfilAcesso, partitionManager);
            }
        }
    }
    
    private void criarConfigurarPerfil(PerfilAcesso perfil, PartitionManager partitionManager){
        IdentityManager identityManager = partitionManager.createIdentityManager();
        PerfilAutenticacao group = criarPerfil(perfil.getNome(), identityManager);
        AutenticacaoModel.vincularPerfilFuncionalidade(group, perfil.getPermissoes(), identityManager);
    }
    
    private PerfilAutenticacao criarPerfil(String groupPath, IdentityManager identityManager){
        PerfilAutenticacao perfilAutenticacao = AutenticacaoModel.getPerfilAutenticacao(identityManager, groupPath);
        if(perfilAutenticacao == null){
            perfilAutenticacao = AutenticacaoModel.criarPerfilAutenticacao(identityManager, groupPath);
        }
        return perfilAutenticacao;
    }
    
    private void criarRolesFuncionalidade(Funcionalidade funcionalidade, PartitionManager partitionManager){
        IdentityManager identityManager = partitionManager.createIdentityManager();
        if(funcionalidade.getTiposAcesso() != null){
            for (TipoAcesso tipoAcesso : funcionalidade.getTiposAcesso()) {
                AutenticacaoModel.criarRole(montarNomeRole(funcionalidade.getNome(),
                    tipoAcesso.getNome()),
                    identityManager);
            }
        }
    }
    
    private void configureKeys(Realm partition){
        KeyStore ks = getKeyStore();
        if(ks != null){
            try {
                partition.setAttribute(new Attribute<byte[]>("PublicKey", getPublicKey(ks)));
                partition.setAttribute(new Attribute<byte[]>("PrivateKey", getPrivateKey(ks)));
            } catch (UnrecoverableKeyException | KeyStoreException | NoSuchAlgorithmException e) {
                LOGGER.log(Level.FINE, e.getMessage(), e);
                LOGGER.log(Level.FINE, "FALHA AO CARREGAR INFORMACOES DE CHAVES DE SEGURANCA");
            }
        }
    }
    
    private byte[] getPrivateKey(KeyStore ks) throws KeyStoreException, NoSuchAlgorithmException, UnrecoverableKeyException {
        return ks.getKey("servercert", "test123".toCharArray()).getEncoded();
    }

    private byte[] getPublicKey(KeyStore ks) throws KeyStoreException {
        return ks.getCertificate("servercert").getPublicKey().getEncoded();
    }

    /**
     * Para_Implementar:Alterar senha do arquivo de chaves / utilizar KEYSTORE_PASS no metodo getKeyStore() linha 135
     * */
    private KeyStore getKeyStore() {
        KeyStore ks = null;
        try {
            ks = KeyStore.getInstance(KeyStore.getDefaultType());

            ks.load(getClass().getResourceAsStream(KEYSTORE_FILE_PATH), "store123".toCharArray());
        } catch (Exception e) {
            LOGGER.log(Level.FINE, e.getMessage(), e);
        }

        return ks;
    }
    
}