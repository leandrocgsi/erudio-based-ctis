package br.com.erudio.seguranca.model;

import org.picketlink.idm.model.AbstractIdentityType;
import org.picketlink.idm.model.Account;
import org.picketlink.idm.model.annotation.AttributeProperty;
import org.picketlink.idm.model.annotation.IdentityStereotype;
import org.picketlink.idm.model.annotation.IdentityStereotype.Stereotype;
import org.picketlink.idm.model.annotation.StereotypeProperty;
import org.picketlink.idm.model.annotation.StereotypeProperty.Property;
import org.picketlink.idm.model.annotation.Unique;
import org.picketlink.idm.query.QueryParameter;

import br.com.erudio.negocio.vo.UsuarioVO;

@IdentityStereotype(Stereotype.USER)
public class UsuarioAutenticacao extends AbstractIdentityType implements Account {

    private static final long serialVersionUID = 1L;
    
    public static final QueryParameter NOME_USUARIO = QUERY_ATTRIBUTE.byName("nomeUsuario");
    
    @StereotypeProperty(Property.IDENTITY_USER_NAME)
    @AttributeProperty
    @Unique
    private String nomeUsuario;
    
    @AttributeProperty
    private TipoUsuarioAutenticacao tipoUsuario;
    
    @AttributeProperty
    private UsuarioVO usuario;

    public String getNomeUsuario() {
        return nomeUsuario;
    }

    public void setNomeUsuario(String nomeUsuario) {
        this.nomeUsuario = nomeUsuario;
    }

    public TipoUsuarioAutenticacao getTipoUsuario() {
        return tipoUsuario;
    }

    public void setTipoUsuario(TipoUsuarioAutenticacao tipoUsuario) {
        this.tipoUsuario = tipoUsuario;
    }

    public UsuarioVO getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioVO usuario) {
        this.usuario = usuario;
    }
    
}
