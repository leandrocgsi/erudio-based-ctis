package br.com.erudio.seguranca.model;

import org.picketlink.idm.credential.AbstractToken;
import org.picketlink.json.jose.JWS;
import org.picketlink.json.jose.JWSBuilder;

public class AutenticacaoToken extends AbstractToken {

    private final JWS jwsToken;
    
    public AutenticacaoToken(String token) {
        super(token);
        jwsToken = new JWSBuilder().build(token);
    }

    @Override
    public String getSubject() {
        return jwsToken.getSubject();
    }

}
