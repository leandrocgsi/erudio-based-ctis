package br.com.erudio.seguranca.model;

import static org.picketlink.idm.model.annotation.IdentityStereotype.Stereotype.GROUP;

import java.util.ArrayList;
import java.util.List;

import org.picketlink.idm.model.annotation.AttributeProperty;
import org.picketlink.idm.model.annotation.IdentityStereotype;
import org.picketlink.idm.model.annotation.InheritsPrivileges;
import org.picketlink.idm.model.basic.Group;
import org.picketlink.idm.model.basic.Role;
import org.picketlink.idm.query.QueryParameter;

@IdentityStereotype(GROUP)
public class PerfilAutenticacao extends Group {

    private static final long serialVersionUID = 1L;

    public static final QueryParameter NOME = QUERY_ATTRIBUTE.byName("nomePerfil");
    
    private List<Role> roles;
    
    @SuppressWarnings("unused")
    private PerfilAutenticacao(){
        super();
    }
    
    public PerfilAutenticacao(String nomePerfil){
        super(nomePerfil);
    }

    @InheritsPrivileges
    @AttributeProperty
    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }
    
    /*------*/
    
    public void addRole(Role role){
        if(this.roles == null){
            this.roles = new ArrayList<Role>();
        }
        this.roles.add(role);
    }
    
}
