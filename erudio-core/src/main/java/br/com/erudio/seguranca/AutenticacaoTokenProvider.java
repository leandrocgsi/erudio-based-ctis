package br.com.erudio.seguranca;

import java.util.UUID;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.picketlink.idm.IdentityManager;
import org.picketlink.idm.PartitionManager;
import org.picketlink.idm.credential.Token;
import org.picketlink.idm.credential.storage.TokenCredentialStorage;
import org.picketlink.idm.model.Account;
import org.picketlink.idm.model.Attribute;
import org.picketlink.idm.model.basic.Realm;
import org.picketlink.json.jose.JWSBuilder;

import br.com.erudio.seguranca.model.AutenticacaoToken;

@Stateless
public class AutenticacaoTokenProvider implements Token.Provider<AutenticacaoToken> {

    private static final int MILISSEGUNDO = 1000;
    private static final int EXPIRACAO = 5 * 60;
    
    @Inject
    private PartitionManager partitionManager;

    @Override
    public AutenticacaoToken issue(Account account) {
        TokenCredentialStorage tokenCredentialStorage = getIdentityManager().retrieveCurrentCredential(account, TokenCredentialStorage.class);
        AutenticacaoToken token;
        
        if (tokenCredentialStorage == null) {
            JWSBuilder builder = new JWSBuilder();
                
            builder
                .id(UUID.randomUUID().toString())
                .rsa256(getPrivateKey())
                .issuer(account.getPartition().getName())
                .issuedAt(getCurrentTime())
                .subject(account.getId())
                .expiration(getCurrentTime() + (EXPIRACAO))
                .notBefore(getCurrentTime());

            token = new AutenticacaoToken(builder.build().encode());
            
            getIdentityManager().updateCredential(account, token);
        } else {
            token = new AutenticacaoToken(tokenCredentialStorage.getToken());
        }
        
        return token;
    }

    @Override
    public AutenticacaoToken renew(Account account, AutenticacaoToken renewToken) {
        TokenCredentialStorage tokenStorage = getCurrentToken(account, getIdentityManager());

        if (tokenStorage.getToken().equals(renewToken.getToken())) {
            invalidate(account);
            return issue(account);
        }

        return null;
    }

    @Override
    public void invalidate(Account account) {
        IdentityManager identityManager = getIdentityManager();
        identityManager.removeCredential(account, TokenCredentialStorage.class);
    }

    @Override
    public Class<AutenticacaoToken> getTokenType() {
        return AutenticacaoToken.class;
    }

    private TokenCredentialStorage getCurrentToken(Account account, IdentityManager identityManager) {
        return identityManager.retrieveCurrentCredential(account, TokenCredentialStorage.class);
    }
    
    private byte[] getPrivateKey() {
        Attribute<byte[]> privateKey = getPartition().<byte[]>getAttribute("PrivateKey");
        byte[] result = null;
        
        if(privateKey != null){
            result = privateKey.getValue();
        } 
        return result;
    }
    
    private int getCurrentTime() {
        return (int) (System.currentTimeMillis() / MILISSEGUNDO);
    }
    
    private Realm getPartition(){
        return this.partitionManager.getPartition(Realm.class, Realm.DEFAULT_REALM);
    }
    
    private IdentityManager getIdentityManager() {
        return this.partitionManager.createIdentityManager(getPartition());
    }

}
