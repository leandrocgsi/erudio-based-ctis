package br.com.erudio.seguranca;

import javax.enterprise.event.Observes;

import org.picketlink.config.SecurityConfigurationBuilder;
import org.picketlink.event.SecurityConfigurationEvent;

import br.com.erudio.seguranca.model.PerfilAutenticacao;
import br.com.erudio.seguranca.model.UsuarioAutenticacao;

public class IdentityManagerConfiguration {

    public static final String ERUDIO_IDM_DEFAULT_NAME = "erudio.idm";
    
    @SuppressWarnings("unchecked")
    public void configureIDM(@Observes SecurityConfigurationEvent event){
        SecurityConfigurationBuilder builder = event.getBuilder();
        builder
            .idmConfig()
                .named(ERUDIO_IDM_DEFAULT_NAME)
                    .stores()
                        .file()
                            .workingDirectory(System.getProperty("java.io.tmpdir") + "/erudio-auth-factor-store")
                        .supportType(
                                UsuarioAutenticacao.class,
                                PerfilAutenticacao.class)
                        .supportAllFeatures();
    }
    
}
