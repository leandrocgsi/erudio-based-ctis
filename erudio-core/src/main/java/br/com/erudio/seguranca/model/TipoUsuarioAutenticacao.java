package br.com.erudio.seguranca.model;

public enum TipoUsuarioAutenticacao {
    USUARIO_INTERNO, USUARIO_EXTERNO;
}
