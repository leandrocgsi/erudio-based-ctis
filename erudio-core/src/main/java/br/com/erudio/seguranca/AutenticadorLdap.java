package br.com.erudio.seguranca;

import java.io.IOException;
import java.util.Hashtable;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.naming.AuthenticationException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;

import br.com.erudio.exception.NegocioException;

public final class AutenticadorLdap {

    private static final String INITIAL_CONTEXT_FACTORY = "com.sun.jndi.ldap.LdapCtxFactory";
    private static final String SECURITY_AUTHENTICATION = "simple";
    private static final String REFERRAL = "follow";
    private static final String URL_SERVIDOR = "URL_SERVIDOR";
    private static final String DOMINIO = "DOMINIO";
    private static final String MSG_ERRO_CARGA_PROPERTIES = "Erro na carga do arquivo properties de configuração.";
    private static final Logger LOGGER = Logger.getLogger(AutenticadorLdap.class.getName());
    private static final String CONTRA_BARRAS = "\\";
    private static final int LOAD_FACTOR = 11;
    private static Properties prop = new Properties();

    static {
        try {
            prop.load(AutenticadorLdap.class.getResourceAsStream("/META-INF/autenticacao.properties"));
        } catch (IOException e) {
            LOGGER.log(Level.FINE, e.getMessage(), e);
            NegocioException.throwExceptionErro(MSG_ERRO_CARGA_PROPERTIES);
        }
    }

    private AutenticadorLdap() {
    }

    public static boolean autenticarUsuario(String nome, String senha) {
        try {
            String dominioUser = prop.getProperty(DOMINIO) + CONTRA_BARRAS + nome;
            Hashtable<String, String> env = preencherMapaParametros(senha, dominioUser);

            @SuppressWarnings("unused")
            DirContext ctx = new InitialDirContext(env);
            return Boolean.TRUE;
        } catch (AuthenticationException ae) {
            LOGGER.log(Level.FINE, ae.getMessage(), ae);
            return Boolean.FALSE;
        } catch (Exception ne) {
            LOGGER.log(Level.FINE, ne.getMessage(), ne);
            return Boolean.FALSE;
        }
    }

    private static Hashtable<String, String> preencherMapaParametros(String senha, String dominio_user) {
        Hashtable<String, String> env = new Hashtable<String, String>(LOAD_FACTOR);
        env.put(javax.naming.Context.INITIAL_CONTEXT_FACTORY, INITIAL_CONTEXT_FACTORY);
        env.put(javax.naming.Context.PROVIDER_URL, prop.getProperty(URL_SERVIDOR));

        env.put(javax.naming.Context.SECURITY_AUTHENTICATION, SECURITY_AUTHENTICATION);
        env.put(javax.naming.Context.SECURITY_PRINCIPAL, dominio_user);
        env.put(javax.naming.Context.SECURITY_CREDENTIALS, senha);
        env.put(javax.naming.Context.REFERRAL, REFERRAL);
        return env;
    }
}