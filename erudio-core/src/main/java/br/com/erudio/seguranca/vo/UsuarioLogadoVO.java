package br.com.erudio.seguranca.vo;

import java.util.List;

import br.com.erudio.negocio.vo.base.BaseVO;

public class UsuarioLogadoVO extends BaseVO {

    private static final long serialVersionUID = 1L;

    private String nome;
    
    private String nomeUsuario;
    
    private String tipo;
    
    private List<String> permissoes;
    
    private PerfilAutenticacaoVO perfil;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getNomeUsuario() {
        return nomeUsuario;
    }

    public void setNomeUsuario(String nomeUsuario) {
        this.nomeUsuario = nomeUsuario;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public List<String> getPermissoes() {
        return permissoes;
    }

    public void setPermissoes(List<String> permissoes) {
        this.permissoes = permissoes;
    }

    public PerfilAutenticacaoVO getPerfil() {
        return perfil;
    }

    public void setPerfil(PerfilAutenticacaoVO perfil) {
        this.perfil = perfil;
    }
    
}
