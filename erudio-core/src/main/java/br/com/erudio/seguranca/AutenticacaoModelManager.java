package br.com.erudio.seguranca;

import static br.com.erudio.modelo.TipoUsuario.ID_TIPO_USUARIO_INTERNO;

import java.util.ArrayList;
import java.util.List;

import javax.management.relation.Role;

import br.com.erudio.exception.NegocioException;
import br.com.erudio.modelo.PerfilAcesso;
import br.com.erudio.modelo.SenhaUsuarioExterno;
import br.com.erudio.modelo.Usuario;
import br.com.erudio.modelo.UsuarioExterno;
import br.com.erudio.modelo.UsuarioInterno;
import br.com.erudio.negocio.NegocioBaseErudio;
import br.com.erudio.negocio.leitura.PerfilAcessoNegocioLeitura;
import br.com.erudio.negocio.leitura.UsuarioNegocioLeitura;
import br.com.erudio.negocio.vo.UsuarioVO;
import br.com.erudio.seguranca.model.AutenticacaoModel;
import br.com.erudio.seguranca.model.PerfilAutenticacao;
import br.com.erudio.seguranca.model.TipoUsuarioAutenticacao;
import br.com.erudio.seguranca.model.UsuarioAutenticacao;
import br.com.erudio.util.Util;

@Stateless
public class AutenticacaoModelManager extends NegocioBaseErudio {

    @Inject
    private RelationshipManager relationshipManager;

    @Inject
    private IdentityManager identityManager;

    @Inject
    private UsuarioNegocioLeitura usuarioNegocioLeitura;
    
    @Inject
    private PerfilAcessoNegocioLeitura acessoNegocio;

    /**
     * PARA_FAZER: verificar possibilidade de lancar AutenticacaoException  metodo inicializarUsuario() linha 54;
     */
    public UsuarioAutenticacao inicializarUsuario(String nomeUsuario, String senha) {
        Usuario usuario = recuperarUsuarioExternoInterno(nomeUsuario);

        NegocioException.throwExceptionAlertaIfNull(usuario, "Usuário não encontrado");
        UsuarioAutenticacao usuarioAutenticacao = null;

        if (ID_TIPO_USUARIO_INTERNO.equals(usuario.getTipoUsuario().getId())) {
            usuarioAutenticacao = ativarUsuarioInterno(usuario, nomeUsuario, senha);
        } else {
            usuarioAutenticacao = ativarUsuarioExterno(usuario, nomeUsuario);
        }

        return usuarioAutenticacao;
    }
    
    public void atualizarPermissoesPerfil(String nomePerfil) {
        PerfilAcesso perfilAcesso = acessoNegocio.buscarPerfilAcessoPorNome(nomePerfil);
        PerfilAutenticacao perfilAutenticacao = AutenticacaoModel.getPerfilAutenticacao(identityManager, nomePerfil);
        
        if(perfilAcesso == null && perfilAutenticacao == null){
            return;
        }
        
        if(perfilAcesso == null && perfilAutenticacao != null){
            tratarPerfilExcluido(perfilAutenticacao);
        }

        if(perfilAutenticacao == null){
            perfilAutenticacao = AutenticacaoModel.criarPerfilAutenticacao(identityManager, nomePerfil);
        } else {
            perfilAutenticacao.setRoles(null);
        }
        
        AutenticacaoModel.vincularPerfilFuncionalidade(perfilAutenticacao, perfilAcesso.getPermissoes(), identityManager);
        atualizarUsuariosVinculadosAoPerfil(perfilAutenticacao);
    }
    
    public List<String> recuperarPermissoesUsuario(UsuarioAutenticacao usuario){
        List<Grant> grants = AutenticacaoModel.getGrantsUsuario(relationshipManager, usuario);
        List<String> permissoes = null;
        
        if(grants != null && !grants.isEmpty()){
            permissoes = new ArrayList<String>();
            for (Grant grant : grants) {
                permissoes.add(grant.getRole().getName());
            }
        }
        
        return permissoes;
    }
    
    public void atualizarPermissoesUsuario(String nomeUsuario){
        UsuarioAutenticacao ua = AutenticacaoModel.getUsuario(identityManager, nomeUsuario);
        if(ua == null){
            return;
        }
        
        Usuario usuario = recuperarUsuarioExternoInterno(nomeUsuario);
        
        if(usuario == null){
            this.identityManager.remove(ua);
        } else {
            removerVinculosUsuario(ua);
            vincularUsuarioPerfilPermicoes(ua);
        }
    }

    private void tratarPerfilExcluido(PerfilAutenticacao perfilAutenticacao){
        List<GroupMembership> membros = AutenticacaoModel.getMembrosLogadosPerfil(perfilAutenticacao, relationshipManager);
        revogarRolesUsuario(membros);
        this.identityManager.remove(perfilAutenticacao);
    }
    
    
    private UsuarioAutenticacao ativarUsuarioInterno(Usuario usuario, String nomeUsuario, String senha) {
        if(this.validarSenhaUsuarioLDAP(nomeUsuario, senha)){
            return adicionarUsuarioPicketLink(usuario, nomeUsuario, Util.stringToMD5(senha));
        }
        return null; 
    }

    private UsuarioAutenticacao ativarUsuarioExterno(Usuario usuario, String nomeUsuario) {
        SenhaUsuarioExterno senhaCadastrada = recuperarUltimaSenhaUsuarioExterno(usuario);
        return adicionarUsuarioPicketLink(usuario, nomeUsuario, senhaCadastrada.getSenha());
    }

    private Boolean validarSenhaUsuarioLDAP(String nomeUsuario, String senha) {
        return AutenticadorLdap.autenticarUsuario(nomeUsuario, senha);
    }

    private Usuario recuperarUsuarioExternoInterno(String nomeUsuario) {
        Usuario usuario = recuperarUsuarioExterno(nomeUsuario);
        if (usuario == null) {
            usuario = recuperarUsuarioInterno(nomeUsuario);
        }

        if(usuario == null && !isUsuarioAtivo(usuario)){
            return null;
        }
        
        return usuario;
    }

    private Boolean isUsuarioAtivo(Usuario usuario){
        return usuario.getAtivo() && usuario.getPerfil().getAtivo();
    }
    
    private Usuario recuperarUsuarioExterno(String email) {
        Usuario usuario = null;
        UsuarioExterno usuarioExterno = usuarioNegocioLeitura.buscarUsuarioExternoPorEmail(email);
        if (usuarioExterno != null) {
            usuario = usuarioExterno.getUsuario();
        }
        return usuario;
    }

    private Usuario recuperarUsuarioInterno(String nomeUsuario) {
        Usuario usuario = null;
        UsuarioInterno usuarioInterno = usuarioNegocioLeitura.buscarUsuarioInternoPorLoginRede(nomeUsuario);
        if (usuarioInterno != null) {
            usuario = usuarioInterno.getUsuario();
        }
        return usuario;
    }

    private SenhaUsuarioExterno recuperarUltimaSenhaUsuarioExterno(Usuario usuario) {
        return usuarioNegocioLeitura.buscarUltimaSenhaUsuarioExterno(usuario);
    }

    private UsuarioAutenticacao criarUsuarioAutenticacao(Usuario usuario, String nomeUsuario) {
        UsuarioAutenticacao ua = new UsuarioAutenticacao();
        ua.setUsuario(converter(usuario, UsuarioVO.class));
        ua.setNomeUsuario(nomeUsuario);
        ua.setTipoUsuario(ID_TIPO_USUARIO_INTERNO.equals(usuario.getTipoUsuario().getId()) ? 
                TipoUsuarioAutenticacao.USUARIO_INTERNO : TipoUsuarioAutenticacao.USUARIO_EXTERNO);
        
        return ua;
    }

    private UsuarioAutenticacao adicionarUsuarioPicketLink(Usuario usuario, String nomeUsuario, String senha) {
        UsuarioAutenticacao ua = AutenticacaoModel.getUsuario(identityManager, nomeUsuario);

        if (ua != null) {
            this.identityManager.remove(ua);
        }

        ua = criarUsuarioAutenticacao(usuario, nomeUsuario);
        this.identityManager.add(ua);
        this.identityManager.updateCredential(ua, new Password(senha));
        vincularUsuarioPerfilPermicoes(ua);
        
        return ua;
    }

    private void removerVinculosUsuario(UsuarioAutenticacao usuario){
        AutenticacaoModel.removerPerilsUsuario(relationshipManager, usuario);
        AutenticacaoModel.revogarRolesDoUsuario(relationshipManager, usuario);
    }
    
    private void vincularUsuarioPerfilPermicoes(UsuarioAutenticacao usuario) {
        PerfilAutenticacao perfil = AutenticacaoModel.getPerfilAutenticacao(identityManager, usuario.getUsuario().getPerfil().getNome());
        if(perfil != null){
            AutenticacaoModel.addToGroup(relationshipManager, usuario, perfil);
            vincularUsuarioPermicoes(usuario, perfil.getRoles());
        }
    }
    
    private void vincularUsuarioPermicoes(UsuarioAutenticacao usuario, List<Role> roles) {
        if (roles != null) {
            for (Role role : roles) {
                AutenticacaoModel.grantRole(relationshipManager, usuario, role);
            }
        }
    } 

    private void atualizarUsuariosVinculadosAoPerfil(PerfilAutenticacao perfilAutenticacao) {
        List<GroupMembership> membros = AutenticacaoModel.getMembrosLogadosPerfil(perfilAutenticacao, relationshipManager);
        revogarRolesUsuario(membros);
        vincularRolesUsuarios(membros, perfilAutenticacao);
    }

    private void vincularRolesUsuarios(List<GroupMembership> usuarios, PerfilAutenticacao perfil) {
        for (GroupMembership usuario : usuarios) {
            vincularUsuarioPermicoes((UsuarioAutenticacao) usuario.getMember(), perfil.getRoles());
        }
    }

    private void revogarRolesUsuario(List<GroupMembership> result) {
        for (GroupMembership groupMembership : result) {
            AutenticacaoModel.revogarRolesDoUsuario(relationshipManager, (UsuarioAutenticacao)groupMembership.getMember());
        }
    }
}
